//
//  BMLiveProcCaptureViewController.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 11/26/14.
//  Copyright (c) 2014 BIOMIO. All rights reserved.
//

// Sets up the video capture session for the specified camera, quality and grayscale mode
//
//
// camera: -1 for default, 0 for back camera, 1 for front camera
// qualityPreset: [AVCaptureSession sessionPreset] value
// grayscale: YES to capture grayscale frames, NO to capture RGBA frames
//


#import "BMLiveProcCaptureViewController.h"
// handlers
#import "BMSocketHandler.h"
// libs/frameworks
#import "UIImage+OpenCV.h"
#import <opencv2/highgui/cap_ios.h>
// models
#import "BMTryAuthMethod.h"

#define RECT_TO_FRAME_RATIO_FRONT_CAM           0.7
#define RECT_TO_FRAME_RATIO_BACK_CAM            0.6

#define JPEG_COMPRESSION                        1   // compression is 0(most) ... 1(least)

#define FRAMES_BEFORE_CAPTURE                   30
#define BLUR_CHECK_COEF                         150
#define BLUR_CHECK_FLASH_COEF                   150
#define DETECTION_CENTER_DISTANCE_COEF          0.18

// Options for cv::CascadeClassifier::detectMultiScale
const int kHaarOptions =  CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_DO_ROUGH_SEARCH;

@interface BMLiveProcCaptureViewController ()

@end

@implementation BMLiveProcCaptureViewController {
    int mGoodFrameCounter;
    int mWarningFrameCounter;
    
    BOOL delayFinished;
    BOOL frameCapture;
    BOOL captureGrayscale;
    cv::CascadeClassifier detectionCascade;
    
    AVCaptureSession *captureSession;
    AVCaptureDevice *captureDevice;
    AVCaptureVideoDataOutput *videoOutput;
    AVCaptureVideoPreviewLayer *videoPreviewLayer;
    AVCaptureStillImageOutput *stillImageOutput;
    CGRect insideBorderRect;
    UIView* insideBorderView;
    
    CGRect cropFactor;
    
    NSTimeInterval lastShot;
}

@synthesize camera;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        camera = FrontCamera;
        mGoodFrameCounter = 0;
        mWarningFrameCounter = 0;
        captureGrayscale = YES;
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createCaptureSessionForCamera:self.camera qualityPreset:self.qualityPreset grayscale:captureGrayscale];
//    [captureSession startRunning];
    
    // Load the face Haar cascade from resources
    NSString *cascadeFilePath = [[NSBundle mainBundle] pathForResource:self.detectionCascadeFilename ofType:@"xml"];
    if (!detectionCascade.load([cascadeFilePath UTF8String])) {
        NSLog(@"Could not load cascade file: %@", cascadeFilePath);
    }
    [self performSelector:@selector(setDelayStatus) withObject:nil afterDelay:3.0];
    
    //    insideBorderView = [[UIView alloc]initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, self.view.frame.size.height-20)];
    //    insideBorderView.layer.borderColor = [UIColor redColor].CGColor;
    //    insideBorderView.layer.borderWidth = 3.0f;
    //    [self.view addSubview:insideBorderView];
    //    CGFloat borderWidth = 3.0f;
    //    self.view.frame = CGRectInset(self.view.frame, -borderWidth, -borderWidth);
    //    self.view.layer.borderColor = [UIColor redColor].CGColor;
    //    self.view.layer.borderWidth = borderWidth;
}

- (void) setDelayStatus {
    delayFinished = YES;
}

- (void) viewWillAppear:(BOOL)animated {
    self.view.frame = self.frameRect;
    [captureSession startRunning];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma mark - Camera Methods

// Set torch on or off (if supported)
- (void)setTorchOn:(BOOL)torch {
    NSError *error = nil;
    if ([captureDevice hasTorch]) {
        BOOL locked = [captureDevice lockForConfiguration:&error];
        if (locked) {
            captureDevice.torchMode = (torch)? AVCaptureTorchModeOn : AVCaptureTorchModeOff;
            [captureDevice unlockForConfiguration];
        }
    }
}

/**
 *  @return Return YES if the torch is on
 */
- (BOOL)torchOn {
    return (captureDevice.torchMode == AVCaptureTorchModeOn);
}

/**
 *  @brief Switch camera 'on-the-fly'
 *
 *  @param cameraId Camera: 0 for back camera, 1 for front camera
 */
- (void)setCamera:(NSInteger)cameraId {
    if (cameraId != self.camera) {
        camera = cameraId;
        if (captureSession) {
            NSArray* devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
            
            [captureSession beginConfiguration];
            
            [captureSession removeInput:[[captureSession inputs] lastObject]];
            
            if (self.camera >= 0 && self.camera < [devices count]) {
                captureDevice = [devices objectAtIndex:self.camera];
            }
            else {
                captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
            }
            
            // Create device input
            NSError *error = nil;
            AVCaptureDeviceInput *input = [[AVCaptureDeviceInput alloc] initWithDevice:captureDevice error:&error];
            [captureSession addInput:input];
            
            [captureSession commitConfiguration];
        }
    }
}

#pragma mark CaptureSession Methods

- (BOOL)createCaptureSessionForCamera:(NSInteger)cameraId qualityPreset:(NSString *)qualityPreset grayscale:(BOOL)grayscale {
    // Set up AV capture
    NSArray* devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    
    if ([devices count] == 0) {
        NSLog(@"No video capture devices found");
        return NO;
    }
    if (cameraId == -1) {
        self.camera = -1;
        captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] ;
    }
    else if (cameraId >= 0 && self.camera < [devices count]) {
        self.camera = cameraId;
        captureDevice = [devices objectAtIndex:cameraId];
    }
    else {
        self.camera = -1;
        captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        NSLog(@"Camera number out of range. Using default camera");
    }
    
    // Create the capture session
    captureSession = [[AVCaptureSession alloc] init];
    
    if ([captureDevice supportsAVCaptureSessionPreset:self.qualityPreset])
        captureSession.sessionPreset = self.qualityPreset;
    else {
        NSLog(@"captureDevice NOT supportsAVCaptureSessionPreset:%@\nUSING AVCaptureSessionPresetHigh", self.qualityPreset);
        self.qualityPreset = AVCaptureSessionPresetHigh;
        captureSession.sessionPreset = self.qualityPreset;
    }
    // Create device input
    NSError *error = nil;
    AVCaptureDeviceInput *input = [[AVCaptureDeviceInput alloc] initWithDevice:captureDevice error:&error];
    
    // Create and configure device output
    stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    if (stillImageOutput.stillImageStabilizationSupported)
        stillImageOutput.automaticallyEnablesStillImageStabilizationWhenAvailable = YES;
    
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
    [stillImageOutput setOutputSettings:outputSettings];
    
    [captureSession addOutput:stillImageOutput];
    
    videoOutput = [[AVCaptureVideoDataOutput alloc] init];
    dispatch_queue_t queue = dispatch_queue_create("cameraQueue", NULL);
    [videoOutput setSampleBufferDelegate:self queue:queue];
    //dispatch_release(queue);
    
    videoOutput.alwaysDiscardsLateVideoFrames = YES;
    
    // For grayscale mode, the luminance channel from the YUV fromat is used
    // For color mode, BGRA format is used
    OSType format = kCVPixelFormatType_32BGRA;
    
    // Check YUV format is available before selecting it (iPhone 3 does not support it)
    if (grayscale && [videoOutput.availableVideoCVPixelFormatTypes containsObject:
                      [NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange]]) {
        format = kCVPixelFormatType_420YpCbCr8BiPlanarFullRange;
    }
    videoOutput.videoSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA]
                                                            forKey:(id)kCVPixelBufferPixelFormatTypeKey];
    
    //    videoOutput.videoSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithUnsignedInt:format]
    //                                                             forKey:(id)kCVPixelBufferPixelFormatTypeKey];
    
    // Connect up inputs and outputs
    if ([captureSession canAddInput:input]) {
        [captureSession addInput:input];
    }
    
    if ([captureSession canAddOutput:videoOutput]) {
        [captureSession addOutput:videoOutput];
    }
    
    // Create the preview layer
    videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:captureSession];
    [videoPreviewLayer setFrame: self.frameRect];
    videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer insertSublayer:videoPreviewLayer atIndex:0];
    
    return YES;
}

- (void)applyFocusMode:(AVCaptureFocusMode)focusMode withPointOfInterest:(CGPoint)point {
    if ([captureDevice isFocusPointOfInterestSupported] && [captureDevice isFocusModeSupported:focusMode]) {
        CGPoint currentPointOfInterest = captureDevice.focusPointOfInterest;
        AVCaptureFocusMode currentFocusMode = captureDevice.focusMode;
        
        NSError *error;
        if (!CGPointEqualToPoint(point, currentPointOfInterest) || currentFocusMode != focusMode) {
            if ([captureDevice lockForConfiguration:&error]) {
                [captureDevice setFocusPointOfInterest:point];
                [captureDevice setFocusMode:focusMode];
                [captureDevice unlockForConfiguration];
            }
        }
    }
}

/**
 *  AVCaptureVideoDataOutputSampleBufferDelegate delegate method called when a video frame is available. This method is called on the video capture GCD queue. A cv::Mat is created from the frame data and passed on for processing with OpenCV.
 *
 */
- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection {
    CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    OSType format = CVPixelBufferGetPixelFormatType(pixelBuffer);
    CGRect videoRect = CGRectMake(0.0f, 0.0f, CVPixelBufferGetWidth(pixelBuffer), CVPixelBufferGetHeight(pixelBuffer));
    AVCaptureVideoOrientation videoOrientation = [[[videoOutput connections] objectAtIndex:0] videoOrientation];
    if (format == kCVPixelFormatType_420YpCbCr8BiPlanarFullRange) {
        // For grayscale mode, the luminance channel of the YUV data is used
        CVPixelBufferLockBaseAddress(pixelBuffer, 0);
        void *baseaddress = CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 0);
        cv::Mat mat(videoRect.size.height, videoRect.size.width, CV_8UC1, baseaddress, 0);
        [self processFrame:mat videoRect:videoRect videoOrientation:videoOrientation];
        
        CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
    }
    else if (format == kCVPixelFormatType_32BGRA) {
        // For color mode a 4-channel cv::Mat is created from the BGRA data
        CVPixelBufferLockBaseAddress(pixelBuffer, 0);
        void *baseaddress = CVPixelBufferGetBaseAddress(pixelBuffer);
        
        cv::Mat mat(videoRect.size.height, videoRect.size.width, CV_8UC4, baseaddress, 0);
        
        [self processFrame:mat videoRect:videoRect videoOrientation:videoOrientation];
        if (frameCapture) {
            frameCapture = NO;
            [self processImage:[self imageFromSampleBuffer:sampleBuffer]];
        }
        CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
    }
    else {
        NSLog(@"Unsupported video format");
    }
}

#pragma mark Frame Geometry methods

/**
 *  @discussion Create an affine transform for converting CGPoints and CGRects from the video frame coordinate space to the preview layer coordinate space. Usage:
 – CGPoint viewPoint = CGPointApplyAffineTransform(videoPoint, transform);
 - CGRect viewRect = CGRectApplyAffineTransform(videoRect, transform);
 
 Use CGAffineTransformInvert to create an inverse transform for converting from the view cooridinate space to
 the video frame coordinate space.
 *
 *  @param videoFrame       A rect describing the dimensions of the video frame
 *  @param videoOrientation The videoframe orientation
 *
 *  @return Returns an affine transform
 */
- (CGAffineTransform)affineTransformForVideoFrame:(CGRect)videoFrame
                                      orientation:(AVCaptureVideoOrientation)videoOrientation {
    //NSLog(@"w:%.f h:%.f", videoFrame.size.width, videoFrame.size.height);
    CGSize viewSize = self.view.bounds.size;
    NSString * const videoGravity = videoPreviewLayer.videoGravity;
    CGFloat widthScale = 1.0f;
    CGFloat heightScale = 1.0f;
    
    // Move origin to center so rotation and scale are applied correctly
    CGAffineTransform t = CGAffineTransformMakeTranslation(-videoFrame.size.width / 2.0f, -videoFrame.size.height / 2.0f);
    
    switch (videoOrientation) {
        case AVCaptureVideoOrientationPortrait:
            widthScale = viewSize.width / videoFrame.size.width;
            heightScale = viewSize.height / videoFrame.size.height;
            break;
            
        case AVCaptureVideoOrientationPortraitUpsideDown:
            t = CGAffineTransformConcat(t, CGAffineTransformMakeRotation(M_PI));
            widthScale = viewSize.width / videoFrame.size.width;
            heightScale = viewSize.height / videoFrame.size.height;
            break;
            
        case AVCaptureVideoOrientationLandscapeRight:
            t = CGAffineTransformConcat(t, CGAffineTransformMakeRotation(M_PI_2));
            widthScale = viewSize.width / videoFrame.size.height;
            heightScale = viewSize.height / videoFrame.size.width;
            break;
            
        case AVCaptureVideoOrientationLandscapeLeft:
            t = CGAffineTransformConcat(t, CGAffineTransformMakeRotation(-M_PI_2));
            widthScale = viewSize.width / videoFrame.size.height;
            heightScale = viewSize.height / videoFrame.size.width;
            break;
    }
    
    // Adjust scaling to match video gravity mode of video preview
    if (videoGravity == AVLayerVideoGravityResizeAspect) {
        heightScale = MIN(heightScale, widthScale);
        widthScale = heightScale;
    }
    else if (videoGravity == AVLayerVideoGravityResizeAspectFill) {
        heightScale = MAX(heightScale, widthScale);
        widthScale = heightScale;
    }
    
    // Apply the scaling
    t = CGAffineTransformConcat(t, CGAffineTransformMakeScale(widthScale, heightScale));
    
    // Move origin back from center
    t = CGAffineTransformConcat(t, CGAffineTransformMakeTranslation(viewSize.width / 2.0f, viewSize.height / 2.0f));
    
    return t;
}

#pragma mark - Button methods

/**
 *  Turn torch on/off
 *
 */
- (IBAction)toggleTorch:(id)sender {
    self.torchOn = !self.torchOn;
}

/**
 *  Switch between front/back cameras
 *
 */
- (IBAction)toggleCamera:(id)sender {
    if (self.camera == FrontCamera)
        self.camera = BackCamera;
    else
        self.camera = FrontCamera;
}

#pragma mark - Image Processing

- (void) captureStillImage { //method to capture image with AVCaptureStillImageOutput
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in stillImageOutput.connections) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] ) {
                videoConnection = connection;
                videoConnection.preferredVideoStabilizationMode = AVCaptureVideoStabilizationModeAuto;
                break;
            }
        }
        if (videoConnection)
            break;
    }
    
    NSLog(@"Capturing: %@", stillImageOutput);
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
        if (imageSampleBuffer != NULL) {
            NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                [self processImage:imageData];
            });
        }
    }];
}

- (void) processImage: (id) pImage {
    UIImage* lResultImage;
    if ([pImage isKindOfClass:[NSData class]])
        lResultImage =  [UIImage imageWithData:pImage];
    else if ([pImage isKindOfClass:[UIImage class]])
        lResultImage = pImage;
    else
        return;
    
    if (![self checkForBlurryImage:lResultImage]) {
        self.samplesRequested--;
        lResultImage = [self cropImage:lResultImage withCropFactor:cropFactor];
        // save photo to photoroll
        //        UIImageWriteToSavedPhotosAlbum(lResultImage, nil, nil, nil);
        
        if ([self.cameraDelegate respondsToSelector:@selector(BMLiveCapturedPhoto:encodedToBase64:)]) {
            BOOL completed = [self.cameraDelegate BMLiveCapturedPhoto:lResultImage encodedToBase64:[self encodeToBase64String:lResultImage]];
            if (completed)
                [captureSession stopRunning];
        }
    }
}

/**
 *  @description
 *  @remark Note that this method is called on the video capture GCD queue. Use dispatch_sync or dispatch_async to update UI from the main queue.
 *
 *  @param mat                   The frame as an OpenCV::Mat object. The matrix will have 1 channel for grayscale frames and 4 channels for BGRA frames. (Use -[VideoCaptureViewController setGrayscale:])
 *  @param rect            A CGRect describing the video frame dimensions
 *  @param videOrientation Video frame orientation. Will generally be AVCaptureVideoOrientationLandscapeRight for the back camera and AVCaptureVideoOrientationLandscapeRight for the front camera
 */
- (void)processFrame:(cv::Mat &)mat
           videoRect:(CGRect)rect
    videoOrientation:(AVCaptureVideoOrientation)videOrientation {
    // Shrink video frame to 320X240
    
    if (rect.size.width > 500) {
        CGFloat kW = rect.size.width / 320;
        rect.size.width = 320;
        rect.size.height /= kW;
        CGFloat sizeMult = 1.0/kW;
        cv::resize(mat, mat, cv::Size(), sizeMult, sizeMult, CV_INTER_LINEAR);
    }
    
    // Rotate video frame by 90deg to portrait by combining a transpose and a flip
    // Note that AVCaptureVideoDataOutput connection does NOT support hardware-accelerated
    // rotation and mirroring via videoOrientation and setVideoMirrored properties so we
    // need to do the rotation in software here.
    cv::transpose(mat, mat);
    CGFloat temp = rect.size.width;
    rect.size.width = rect.size.height;
    rect.size.height = temp;
    
    if (videOrientation == AVCaptureVideoOrientationLandscapeRight) {
        // flip around y axis for back camera
        cv::flip(mat, mat, 1);
    }
    else {
        // Front camera output needs to be mirrored to match preview layer so no flip is required here
    }
    
    videOrientation = AVCaptureVideoOrientationPortrait;
    
    // Detect objects
    std::vector<cv::Rect> objectsVect;
    
    detectionCascade.detectMultiScale(mat, objectsVect, 1.1, 7, kHaarOptions, cv::Size(60, 60));
    if (objectsVect.size() > 0){
        cv::Rect crop = objectsVect[0];
        CGRect roiRect = CGRectMake(crop.x, crop.y, crop.width, crop.height);
        CGRect imageRect = rect;
        
        CGFloat roiToImageWidth = imageRect.size.width/roiRect.size.width;
        
        CGFloat cH = roiRect.size.width * roiToImageWidth;
        CGFloat cY = roiRect.origin.y - (cH-roiRect.size.width)/2;
        
        CGFloat x = 0; // cX/imageRect.size.width;
        CGFloat y = cY/imageRect.size.height;
        CGFloat w = 1;
        CGFloat h = cH/imageRect.size.height;
        
        cropFactor = CGRectMake(x, y, w, h);
    }
    // Dispatch updating of face markers to main queue
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self displayObjects:objectsVect
                forVideoRect:rect
            videoOrientation:videOrientation];
    });
}

/**
 *  Update object (ROI) markers from given vector of object rectangles
 *
 *  @param objectsVect      Vector of rectangles of detected ROI objects
 *  @param rect             VideoRect of the videoPreview
 *  @param videoOrientation AVCaptureVideoOrientation of the videoPreview
 */
- (void)displayObjects:(const std::vector<cv::Rect> &)objectsVect
          forVideoRect:(CGRect)rect
      videoOrientation:(AVCaptureVideoOrientation)videoOrientation {
    NSArray *sublayers = [NSArray arrayWithArray:[videoPreviewLayer sublayers]];
    NSInteger sublayersCount = [sublayers count];
    NSInteger currentSublayer = 0;
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    
    // hide all the objects layers
    for (CALayer *layer in sublayers) {
        NSString *layerName = [layer name];
        if ([layerName isEqualToString:@"ObjectRoiLayer"])
            [layer setHidden:YES];
    }
    // Create transform to convert from vide frame coordinate space to view coordinate space
    CGAffineTransform t = [self affineTransformForVideoFrame:rect orientation:videoOrientation];
    insideBorderView.layer.borderColor = [UIColor redColor].CGColor;
    if (objectsVect.size() == 0) {
        self.detectionCenterImageview.highlighted = NO;
        mWarningFrameCounter = 0;
    }
    for (NSInteger i = 0; i < objectsVect.size(); i++) {
        insideBorderView.layer.borderColor = [UIColor clearColor].CGColor;
        CGRect lRoiRect;
        lRoiRect.origin.x = objectsVect[i].x;
        lRoiRect.origin.y = objectsVect[i].y;
        lRoiRect.size.width = objectsVect[i].width;
        lRoiRect.size.height = objectsVect[i].height;
        lRoiRect = CGRectApplyAffineTransform(lRoiRect, t);
        //        NSLog(@"objectsVect x %.f y %.f w %.f h %.f", objectsVect[i].x, objectsVect[i].y, objectsVect[i].width, objectsVect[i].height);
        CALayer *featureLayer = nil;
        
        while (!featureLayer && (currentSublayer < sublayersCount)) {
            CALayer *currentLayer = [sublayers objectAtIndex:currentSublayer++];
            if ([[currentLayer name] isEqualToString:@"ObjectRoiLayer"]) {
                featureLayer = currentLayer;
                [currentLayer setHidden:NO];
            }
        }
        
        if (!featureLayer) {
            // Create a new feature marker layer
            featureLayer = [[CALayer alloc] init];
            featureLayer.name = @"ObjectRoiLayer";
            featureLayer.borderColor = [[UIColor redColor] CGColor];
            featureLayer.borderWidth = 3.0f;
            //                        [videoPreviewLayer addSublayer:featureLayer];
            //                        videoPreviewLayer.transform = CATransform3DMakeScale(.60, .60, 1);
        }
        if (self.samplesRequested > 0)
            self.detectionCenterImageview.highlighted = [self analyzeROI:lRoiRect];
        featureLayer.frame = lRoiRect;
    }
    [CATransaction commit];
}

- (BOOL) analyzeROI:(CGRect) pRoiRect  {
    CGFloat ROI_TO_FRAME_ACCEPTED_RATIO = (self.camera == BackCamera) ? RECT_TO_FRAME_RATIO_BACK_CAM : RECT_TO_FRAME_RATIO_FRONT_CAM;
    
    CGFloat     FRAME_DIAGONAL  = sqrt(pow(videoPreviewLayer.frame.size.width,2)+pow(videoPreviewLayer.frame.size.height,2));
    CGPoint     FRAME_CENTER    = CGPointMake(videoPreviewLayer.frame.size.width/2, videoPreviewLayer.frame.size.height/2);
    
    CGPoint     ROI_CENTER      = CGPointMake(pRoiRect.origin.x + pRoiRect.size.width/2, pRoiRect.origin.y + pRoiRect.size.height/2);
    NSUInteger  ROI_WIDTH       = pRoiRect.size.width;
#define ROI_TO_FRAME_MAX_RATIO 0.9
    // if (ROI is BIG enough && ROI_CENTER is CLOSE to FRAME_CENTER
    //    [self showUserDistanceWarningIconInDirection:0 ForTimeInterval:1];
    if (ROI_WIDTH/videoPreviewLayer.frame.size.width >= ROI_TO_FRAME_ACCEPTED_RATIO &&
        ROI_WIDTH/videoPreviewLayer.frame.size.width < ROI_TO_FRAME_MAX_RATIO &&
        hypotf(ROI_CENTER.x - FRAME_CENTER.x, ROI_CENTER.y - FRAME_CENTER.y) < FRAME_DIAGONAL*DETECTION_CENTER_DISTANCE_COEF) {
        self.userDistanceWarningImageView.hidden = YES;
        
        mGoodFrameCounter++;
        mWarningFrameCounter = 0;
        NSInteger checkDelay = (delayFinished) ? 30 : 150;
        
        if (mGoodFrameCounter >= checkDelay){
            // captures image using AVCaptureStillImageOutput
            //             [self captureStillImage];
            // extracts current image from SampleBuffer
            frameCapture = YES;
            mGoodFrameCounter = 0;
            
            if  ([self.cameraDelegate respondsToSelector:@selector(BMLiveCaptureFaceDetectedInFrame)])
                [self.cameraDelegate BMLiveCaptureFaceDetectedInFrame];
        }
        return YES;
    }
    else {
        mGoodFrameCounter = 0;
        mWarningFrameCounter++;
        if (ROI_WIDTH/videoPreviewLayer.frame.size.width < ROI_TO_FRAME_ACCEPTED_RATIO && mWarningFrameCounter >= 30) {
            mWarningFrameCounter = 0;
            [self showUserDistanceWarningIconInDirection:0 ForTimeInterval:1.5];
        } else if (ROI_WIDTH/videoPreviewLayer.frame.size.width >= ROI_TO_FRAME_MAX_RATIO) {
            [self showUserDistanceWarningIconInDirection:1 ForTimeInterval:1.5];
        }
        return NO;
    }
}
/**
 *  Show a warning for the user to adjust the distance to the device.
 *
 *  @param direction 0 = outside/closer; 1 = inside/away;
 *  @param interval  time interval to be shown
 */
- (void) showUserDistanceWarningIconInDirection:(NSInteger) direction ForTimeInterval: (NSTimeInterval) interval {
    if (self.userDistanceWarningImageView.hidden) {
        self.userDistanceWarningImageView.hidden = NO;
        self.userDistanceWarningImageView.highlighted = direction;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(interval * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.userDistanceWarningImageView.hidden = YES;
            mWarningFrameCounter = -30;
        });
    }
}

- (UIImage*) cropImage:(UIImage*) pImage withCropFactor:(CGRect) pCropFactor {
    // TODO: fix image orientation to handle natural crop rect
    //  (0,0) is the top-right corner of the photo, so cropWidth <=> cRopHeight
    
    CGRect cropRect = CGRectMake(pImage.size.height * pCropFactor.origin.y * pImage.scale,
                                 0,
                                 pImage.size.height * pCropFactor.size.height * pImage.scale,
                                 pImage.size.width * pImage.scale);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([pImage CGImage], cropRect);
    UIImage *result = [UIImage imageWithCGImage:imageRef
                                          scale:pImage.scale
                                    orientation:pImage.imageOrientation];
    
    CGImageRelease(imageRef);
    return result;
}

#pragma mark Image Convert

/**
 *  Create an UIImage from sample buffer data
 *
 *  @param sampleBuffer CMSampleBufferRef
 *
 *  @return UIImage object
 */
- (UIImage *) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer {
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // Get the number of bytes per row for the pixel buffer
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    // Get the pixel buffer width and height
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    
    // Get the number of bytes per row for the pixel buffer
    u_int8_t *baseAddress = (u_int8_t *)malloc(bytesPerRow*height);
    memcpy( baseAddress, CVPixelBufferGetBaseAddress(imageBuffer), bytesPerRow * height);
    
    // size_t bufferSize = CVPixelBufferGetDataSize(imageBuffer);
    
    // Create a device-dependent RGB color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Create a bitmap graphics context with the sample buffer data
    
    //The context draws into a bitmap which is `width'
    //  pixels wide and `height' pixels high. The number of components for each
    //      pixel is specified by `space'
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8,
                                                 bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipFirst);
    
    // Create a Quartz image from the pixel data in the bitmap graphics context
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    
    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    
    // Free up the context and color space
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    // Create an image object from the Quartz image
    UIImage *image = [UIImage imageWithCGImage:quartzImage scale:1.0 orientation:UIImageOrientationRight];
    
    free(baseAddress);
    // Release the Quartz image
    CGImageRelease(quartzImage);
    
    
    return (image);
}

- (UIImage *)UIImageFromCVMat:(cv::Mat)cvMat {
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
    CGColorSpaceRef colorSpace;
    
    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    
    // Creating CGImage from cv::Mat
    CGImageRef imageRef = CGImageCreate(cvMat.cols,                                 //width
                                        cvMat.rows,                                 //height
                                        8,                                          //bits per component
                                        8 * cvMat.elemSize(),                       //bits per pixel
                                        cvMat.step[0],                            //bytesPerRow
                                        colorSpace,                                 //colorspace
                                        kCGImageAlphaNone|kCGBitmapByteOrderDefault,// bitmap info
                                        provider,                                   //CGDataProviderRef
                                        NULL,                                       //decode
                                        false,                                      //should interpolate
                                        kCGRenderingIntentDefault                   //intent
                                        );
    
    
    // Getting UIImage from CGImage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    
    return finalImage;
}

- (BOOL) checkForBlurryImage:(UIImage *) image {
    cv::Mat matImage = [self convertUIImageToCVMat:image];
    cv::Mat matImageGrey;
    cv::cvtColor(matImage, matImageGrey, CV_BGR2GRAY);
    
    cv::Mat dst2 = [self convertUIImageToCVMat:image];
    cv::Mat laplacianImage;
    
    
    dst2.convertTo(laplacianImage, CV_8UC1);
    
    cv::Laplacian(matImageGrey, laplacianImage, CV_8U);
    cv::Mat laplacianImage8bit;
    laplacianImage.convertTo(laplacianImage8bit, CV_8UC1);
    
    
    //-------------------------------------------------------------
    //-------------------------------------------------------------
    unsigned char *pixels = laplacianImage8bit.data;
    //-------------------------------------------------------------
    //-------------------------------------------------------------
    //    unsigned char *pixels = laplacianImage8bit.data;
    int maxLap = -16777216;
    NSLog(@"%s",pixels);
    for (int i = 0; i < ( laplacianImage8bit.elemSize()*laplacianImage8bit.total()); i++) {
        //NSLog(@"pixel[%i] = %c", i, pixels[i]);
        if (pixels[i] > maxLap){
            maxLap = pixels[i];
            //NSLog(@"maxLap=%i",maxLap);
        }
    }
    
    int soglia = 100;//BLUR_CHECK_FLASH_COEF; // smaller number = less sensitive; default = 180
    
    printf("\n maxLap : %i",maxLap);
    
    
    if (maxLap < soglia || maxLap == soglia) {
        printf("\nBLUR image\n");
        //[_fingerQntyLabel setText:@"BLUR"];
        return YES;
    }else {
        printf("\nNOT BLUR image\n");
        //  [_fingerQntyLabel setText:@"NOT BLUR"];
        return NO;
    }
    return NO;
}

- (cv::Mat)convertUIImageToCVMat:(UIImage *)image {
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels (color channels + alpha)
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    return cvMat;
}

/**
 *  Encode UIImage to Base64.
 *
 *  @param image UIImage to encode
 *
 *  @return NSString Base64
 */
- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImageJPEGRepresentation(image, JPEG_COMPRESSION) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    //    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

/**
 *  Decode Base64 to UIImage.
 *
 *  @param strEncodeData Base64 string
 *
 *  @return UIImage decoded object
 */
- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

@end
