//
//  BMRegistrationViewController.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 3/25/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

// view controllers
#import "BMRegistrationViewController.h"
#import "BMHelpViewController.h"
#import "BMTabViewController.h"
// views
#import "RSProgressBar.h"
// libs/frameworks
#import <AVFoundation/AVFoundation.h>
// handlers
#import "BMStateMachine.h"

@interface BMRegistrationViewController ()
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic) BOOL isReading;

-(BOOL)startReading;
-(void)stopReading;

@end

@implementation BMRegistrationViewController {
    NSString* mSecretString;
}

@synthesize viewPreview, captureSession, videoPreviewLayer;

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!APP_LAUNCHED_BEFORE) {
        BMHelpViewController* helpVC = [[BMHelpViewController alloc]init];
        helpVC.firstLaunchType = YES;
        [self presentViewController:helpVC animated:NO completion:nil];
    }
    
    // status bar background color
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, 20)];
    view.backgroundColor=[UIColor colorWithRed:0.396 green:0.416 blue:0.498 alpha:1.000];
    [self.view addSubview:view];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"BIOMIO_registration_secret"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self setStateMachineStates];
    self.QRCodeView.hidden = YES;
    self.textInputView.hidden = NO;
    
    // cancel buttons
    //    self.cancelTextfield.hidden = YES;
    self.cancelScannerPreview.hidden = YES;
    //    self.cancelTextfield.layer.borderWidth = 1.5;
    //    self.cancelTextfield.layer.cornerRadius = 4;
    //    self.cancelTextfield.layer.borderColor = [[UIColor colorWithRed:255.0f/255.0f green:46.0f/255.0f blue:40.0f/255.0f alpha:1.0f]CGColor];
    self.cancelScannerPreview.layer.borderWidth = 1.5;
    self.cancelScannerPreview.layer.cornerRadius = 4;
    self.cancelScannerPreview.layer.borderColor = [[UIColor colorWithRed:255.0f/255.0f green:46.0f/255.0f blue:40.0f/255.0f alpha:1.0f]CGColor];
    
    self.infoButton.layer.borderWidth = 1.5;
    self.infoButton.layer.cornerRadius = 20;
    self.infoButton.layer.borderColor = [[UIColor colorWithRed:0.255 green:0.529 blue:1.000 alpha:1.000]CGColor];
    
    [self.secretSegmentedCtrl addTarget:self action:@selector(secretInputSegmentedCtrlDidChangeValue) forControlEvents:UIControlEventValueChanged];
    self.captureSession = nil;
    _isReading = NO;
    
    CGFloat lSocketBarPosY = self.navigationController.navigationBar.bounds.size.height + [UIApplication sharedApplication].statusBarFrame.size.height;
    self.registrationProgressBar = [[RSProgressBar alloc] initWithFrame:CGRectMake(0, lSocketBarPosY, [UIScreen mainScreen].bounds.size.width, 5)];
    [self.view addSubview:self.registrationProgressBar];
    [self.registrationProgressBar setBarColor:COLOR_ORANGE];
    
    self.okCodeButton.hidden = YES;
}

- (void) viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;    
    if ([STATE_MACHINE.currentState isEqual:STATE_MACHINE.disconnectedState] || [STATE_MACHINE.currentState isEqual:STATE_MACHINE.connectionFailureState]) {
        [self.registrationProgressBar setBarColor:COLOR_RED];
    }
    else if ([STATE_MACHINE.currentState isEqual:STATE_MACHINE.connectionOpenState]) {
        [self.registrationProgressBar setBarColor:COLOR_ORANGE];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setStateMachineStates {
    [[BMStateMachine sharedHandler].registrationServerHelloDidReceive setDidFireEventBlock:^(TKEvent *event, TKTransition *transition) {
        BMTabViewController* tabbar = (BMTabViewController*) self.tabBarController;
        [tabbar refreshTabBar];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"registrationSuccess"];
        [self.navigationController popToRootViewControllerAnimated:NO];
        
    }];
    
    [[BMStateMachine sharedHandler].registrationDidFail setDidFireEventBlock:^(TKEvent *event, TKTransition *transition) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.registrationProgressBar setProgressBarState:RSProgressBarStateStatic];
            self.secretSegmentedCtrl.hidden = NO;
            self.secretTextfield.text = @"";
            if (transition.userInfo) {
                if ([transition.userInfo [@"status"] isEqualToString:@"wrong code"] || [transition.userInfo [@"status"] isEqualToString:@"Not Found"]) {
                    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Oops!"
                                                                                   message:@"Wrong verification code"
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK"
                                                                       style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                                         [self startReading];
                                                                     }];
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }
            }
        });
    }];
}

#pragma mark - Button/Controls methods

- (void) secretInputSegmentedCtrlDidChangeValue {
    switch (self.secretSegmentedCtrl.selectedSegmentIndex) {
        case 0: { // qr code
            [self.secretTextfield resignFirstResponder];
            [self performSelector:@selector(startStopScanningCode:) withObject:nil];
            self.QRCodeView.hidden = NO;
            self.textInputView.hidden = YES;
            //            self.cancelTextfield.hidden = YES;
            self.secretTextfield.text = @"";
            
            break;
        }
        case 1: { // manual text
            if (self.isReading)
                [self stopReading];
            
            self.QRCodeView.hidden = YES;
            self.textInputView.hidden = NO;
            [self.secretTextfield becomeFirstResponder];
            
        }
        default:
            break;
    }
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!self.isReading) {
        UITouch* lTouch = [touches anyObject];
        if (lTouch.view == self.textInputView && [self.secretTextfield isFirstResponder]) {
            [self.view endEditing:YES];
            self.okCodeButton.hidden = YES;
        }
    }
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    self.okCodeButton.hidden = NO;
    return YES;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    
    if (![self.secretTextfield.text isEqualToString:@""]) {
        [self startConnectionWithSecret:self.secretTextfield.text];
    }
    return YES;
}

- (IBAction)okCodeButtonPressed:(id)sender {
    self.okCodeButton.hidden = YES;
    [self.view endEditing:YES];
    if (![self.secretTextfield.text isEqualToString:@""]) {
        [self startConnectionWithSecret:self.secretTextfield.text];
    }
}

#pragma mark - QR code Scanning

- (BOOL)startReading {
    NSError *error;
    self.viewPreview.hidden = NO;
    self.cancelScannerPreview.hidden = NO;
    // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
    // as the media type parameter.
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Get an instance of the AVCaptureDeviceInput class using the previous device object.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        // If any error occurs, simply log the description of it and don't continue any more.
        NSLog(@"%@", [error localizedDescription]);
        UIAlertController* lAlertCtrl = [UIAlertController alertControllerWithTitle:@"Error"
                                                                            message:[NSString stringWithFormat:@"%@\n%@",error.localizedDescription, @"Please check camera permission in Settings"]
                                                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* lOkAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                            style:UIAlertActionStyleCancel
                                                          handler:nil];
        UIAlertAction* lSettingsAction = [UIAlertAction actionWithTitle:@"Settings"
                                                                  style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction *action) {
                                                                    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                    [[UIApplication sharedApplication] openURL:url];
                                                                }];
        
        [lAlertCtrl addAction:lOkAction];
        [lAlertCtrl addAction:lSettingsAction];
        [self presentViewController:lAlertCtrl animated:YES completion:nil];
        
        return NO;
    }
    
    // Initialize the captureSession object.
    self.captureSession = [[AVCaptureSession alloc] init];
    // Set the input device on the capture session.
    [self.captureSession addInput:input];
    
    
    // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [self.captureSession addOutput:captureMetadataOutput];
    
    // Create a new serial dispatch queue.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    self.videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    [self.videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [self.videoPreviewLayer setFrame:viewPreview.layer.bounds];
    [viewPreview.layer addSublayer:self.videoPreviewLayer];
    
    // Start video capture.
    [self.captureSession startRunning];
    
    return YES;
}

-(void)stopReading{
    // Stop video capture and make the capture session object nil.
    [self.captureSession stopRunning];
    self.captureSession = nil;
    self.viewPreview.hidden = YES;
    self.cancelScannerPreview.hidden = YES;
    _isReading = NO;
    // Remove the video preview layer from the viewPreview view's layer.
    [self.videoPreviewLayer removeFromSuperlayer];
}

- (void) startConnectionWithSecret: (NSString*) pSecret {
    [[NSUserDefaults standardUserDefaults]setObject:pSecret forKey:@"BIOMIO_registration_secret"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.registrationProgressBar setProgressBarState:RSProgressBarStateRefreshing];
        
    });
    
    NSError* error = nil;
    if ([[BMStateMachine sharedHandler].currentState isEqual:[BMStateMachine sharedHandler].connectionOpenState]) {
        [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidOpenWithoutUser userInfo:nil error:&error];
    }
    else {
        [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidStart userInfo:nil error:&error];
    }
}

- (IBAction)startStopScanningCode:(id)sender {
    if (!_isReading) {
        if ([self startReading]) {
            NSLog(@"startQRReading");
        }
    }
    else {
        [self stopReading];
        NSLog(@"stopQRReading");
    }
    _isReading = !_isReading;
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        // Get the metadata object.
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            NSLog(@"QR CODE DECODED: %@", [metadataObj stringValue]);
            
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
            _isReading = NO;
            [self startConnectionWithSecret:[metadataObj stringValue]];
        }
    }
}

@end
