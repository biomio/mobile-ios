//
//  BMPinCodeView.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 12/16/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

#import "BMPinCodeView.h"

@implementation BMPinCodeView {
    NSTimer* countdownTimer;
}

+ (instancetype) pinCodeViewWithFrame:(CGRect) frame
                                 code:(NSString*) code
                     expirationPeriod:(NSTimeInterval) expire
                             delegate:(id) delegateObject {
    BMPinCodeView* pinCodeView = [[BMPinCodeView alloc]initWithFrame:frame];
    pinCodeView.pincodeString = code;
    pinCodeView.timeoutPeriod = expire;
    pinCodeView.delegate = delegateObject;
    return pinCodeView;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSBundle mainBundle]loadNibNamed:@"BMPinCodeView" owner:self options:nil];
        [self addSubview:self.mainView];
        [self commonInit];
    }
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [[NSBundle mainBundle]loadNibNamed:@"BMPinCodeView" owner:self options:nil];
        [self addSubview:self.mainView];
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
    self.mainView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    self.confirmButton.layer.borderColor = COLOR_GREEN.CGColor;
    self.confirmButton.layer.borderWidth = 3;
    self.confirmButton.layer.cornerRadius = 5;
    self.pinCodeLabel.text = @"******";
}

- (void)setPincodeString:(NSString *)pincodeString {
    if (pincodeString) {
        _pincodeString = pincodeString;
        self.pinCodeLabel.text = [pincodeString isEqualToString:@""] ? @"******" : pincodeString;
    }
}

#pragma mark - Timeout

- (void)setTimeoutPeriod:(NSTimeInterval)timeoutPeriod {
    // set initial timeout text
    NSDate* expirationDate = [[NSDate date] initWithTimeIntervalSinceNow:timeoutPeriod+0.5]; // +0.5 because only in this case (plus >0) the countdownLabel will show first the full timeout period and then start decrementing
    NSTimeInterval expirationTimestamp = [expirationDate timeIntervalSinceNow];
    NSInteger mins = (expirationTimestamp)/60;
    NSInteger secs = (expirationTimestamp) - mins*60;
    
    self.countdownLabel.text =  [NSString stringWithFormat:@"%ld:%02ld", (long)mins, (long)secs];
    
    countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                     target:self
                                   selector:@selector(updateCountdownLabel:)
                                   userInfo:expirationDate
                                    repeats:YES];
}

- (void) updateCountdownLabel:(NSTimer*) timer {
    NSDate* expirationDate = timer.userInfo;
    NSTimeInterval expirationTimestamp = [expirationDate timeIntervalSinceNow];
    
    if (expirationTimestamp > 1) {
        NSInteger mins = expirationTimestamp/60;
        NSInteger secs = expirationTimestamp - mins*60;
        self.countdownLabel.text =  [NSString stringWithFormat:@"%ld:%02ld", (long)mins, (long)secs];
    } else {
        [timer invalidate];
        self.countdownLabel.text =  @"0:00";
        if ([self.delegate respondsToSelector:@selector(pinCodeDidExpire:)])
            [self.delegate pinCodeDidExpire:self];
    }
}

#pragma mark - Buttons

- (IBAction)confirmButtonPressed:(id)sender {
    [countdownTimer invalidate];
    if ([self.delegate respondsToSelector:@selector(pinCodeConfirmed:)])
        [self.delegate pinCodeConfirmed:self];
}

@end
