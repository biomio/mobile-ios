//
//  BMTryAuthMethod.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 5/18/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, AuthMethodStatus) {
    AuthMethodStatusClean           = 0,
    AuthMethodStatusActive          = 1,
    AuthMethodStatusSent            = 2,
    AuthMethodStatusCompleted       = 3,
    AuthMethodStatusSkipped         = 4,
    AuthMethodStatusExpired         = 5,
    AuthMethodStatusFailed          = 6
};

@interface BMTryAuthMethod : NSObject
@property (nonatomic, strong, readonly) NSString* title;
@property (nonatomic, readonly) BOOL userInteractive;
@property (nonatomic, readonly) NSInteger index;
@property (nonatomic, strong, readonly) NSString* parrentTryID;

@property (nonatomic, strong, readonly) NSString* deviceTitle;
@property (nonatomic, strong, readonly) NSString* deviceSettings;

@property (nonatomic, readonly) NSUInteger samplesRequested;

@property (nonatomic, strong) NSMutableArray* authSamples;
@property (nonatomic, assign) AuthMethodStatus status;

- (void) addAuthSample: (id) pResult;
- (void) didFailWithError:(NSError*) error;
- (BOOL) setCompleted;

+ (instancetype) authMethodWithTitle:(NSString*) title
                             samples:(NSUInteger) samplesRequested
                     userInteractive:(BOOL) userInteractive
                             atIndex:(NSInteger) idx
                              device:(NSString*) deviceTitle
                      deviceSettings:(NSString*) deviceSettings
                   childOfTryRequest:(NSString*) parrentID;

- (NSUInteger) samplesCompleted;
@end
