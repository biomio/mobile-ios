//
//  Global.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 1/20/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#ifndef BIOMIO_iOS_Global_h
#define BIOMIO_iOS_Global_h\

#pragma mark Server URL
#define WEBSOCKET_URL                   @"wss://gate.biom.io:443/websocket"

#pragma mark Device

#define DEVICE_ID                       [UIDevice currentDevice].identifierForVendor.UUIDString
#define IOS_VERSION                     [[UIDevice currentDevice] systemVersion]

#define APP_LAUNCHED_BEFORE             [[NSUserDefaults standardUserDefaults] boolForKey:@"APP_LAUNCHED_BEFORE"]

#define WARNING_CODES                   @"CAPABILITIES_WARNINGS_CODES"
#define WARNING_REVIEWED                @"CAPABILITIES_WARNINGS_REVIEWED"

#pragma mark - NSUserDefaults Keys

#pragma mark - BMUserModel
#define BIOMIO_USER                     @"BIOMIO_currentUser"
#define BIOMIO_DEVICE_ID                @"BIOMIO_currentDeviceID"
#define BIOMIO_APP_ID                   @"BIOMIO_currentAppID"
#define BIOMIO_PRIVKEY                  @"BIOMIO_privateKey"

#pragma mark User Backup
#define BIOMIO_USER_BACKUP              @"BIOMIO_currentUser_backup"
#define BIOMIO_DEVICE_ID_BACKUP         @"BIOMIO_currentDeviceID_backup"
#define BIOMIO_APP_ID_BACKUP            @"BIOMIO_currentAppID_backup"
#define BIOMIO_PRIVKEY_BACKUP           @"BIOMIO_privateKey_backup"


#define BIOMIO_KEYCHAIN                 @"com.biomio.keychain"

#define APNS_TOKEN_NSDATA               @"BIOMIO_apnsToken_NSData"
#define APNS_TOKEN_NSSTRING             @"BIOMIO_apnsToken_NSString"

#pragma mark - Colors

#define COLOR_GREEN                     [UIColor colorWithRed:20.0f/255.0f green:187.0f/255.0f blue:165.0f/255.0f alpha:1.000]
#define COLOR_ORANGE                    [UIColor colorWithRed:255.0f/255.0f green:162.0f/255.0f blue:93.0f/255.0f alpha:1.000]
#define COLOR_RED                       [UIColor colorWithRed:255.0f/255.0f green:73.0f/255.0f blue:73.0f/255.0f alpha:1.000]

#define ERROR_DOMAIN                    @"com.biomio.app.errorDomain"

#pragma mark - Analytics

#define INTERCOM_API_KEY                @"ios_sdk-ef3e605505054dbec28fcedcdaccc6ebdc6cade7"
#define INTERCOM_APP_ID                 @"xuwmsgst"

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#endif
