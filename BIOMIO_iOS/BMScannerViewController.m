//
//  BMScannerViewController.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 7/7/16.
//  Copyright © 2016 BIOMIO. All rights reserved.
//

#import "BMScannerViewController.h"
// view controllers
#import "BMTabViewController.h"

@interface BMScannerViewController ()
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic) BOOL isReading;

-(BOOL)startReading;
-(void)stopReading;

@end

@implementation BMScannerViewController {
    NSString* mSecretString;
    NSDate* lastCapturedTime;
}
@synthesize viewPreview, captureSession, videoPreviewLayer;

#pragma mark - Life cycle
// TODO: check camera permission
- (void)viewDidLoad {
    [super viewDidLoad];
    self.captureSession = nil;
    _isReading = NO;
    [self startReading];
}


- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (!_isReading)
        [self startReading];
    lastCapturedTime = [NSDate dateWithTimeIntervalSinceNow:-600];
}

- (void) viewDidDisappear:(BOOL)animated {
    if (_isReading)
        [self stopReading];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - QR code Scanning

- (BOOL)startReading {
    NSError *error;
    self.viewPreview.hidden = NO;
    // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
    // as the media type parameter.
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Get an instance of the AVCaptureDeviceInput class using the previous device object.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        // If any error occurs, simply log the description of it and don't continue any more.
        NSLog(@"%@", [error localizedDescription]);
        UIAlertController* lAlertCtrl = [UIAlertController alertControllerWithTitle:@"Error"
                                                                            message:[NSString stringWithFormat:@"%@\n%@",error.localizedDescription, @"Please check camera permission in Settings"]
                                                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* lOkAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                            style:UIAlertActionStyleCancel
                                                          handler:nil];
        UIAlertAction* lSettingsAction = [UIAlertAction actionWithTitle:@"Settings"
                                                                  style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction *action) {
                                                                    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                    [[UIApplication sharedApplication] openURL:url];
                                                                }];
        
        [lAlertCtrl addAction:lOkAction];
        [lAlertCtrl addAction:lSettingsAction];
        [self presentViewController:lAlertCtrl animated:YES completion:nil];

        return NO;
    }
    
    // Initialize the captureSession object.
    self.captureSession = [[AVCaptureSession alloc] init];
    // Set the input device on the capture session.
    [self.captureSession addInput:input];
    
    
    // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [self.captureSession addOutput:captureMetadataOutput];
    
    // Create a new serial dispatch queue.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    self.videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    [self.videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [self.videoPreviewLayer setFrame:viewPreview.layer.bounds];
    [viewPreview.layer addSublayer:self.videoPreviewLayer];
    
    // Start video capture.
    [self.captureSession startRunning];
    
    return YES;
}


-(void)stopReading{
    // Stop video capture and make the capture session object nil.
    [self.captureSession stopRunning];
    self.captureSession = nil;
    self.viewPreview.hidden = YES;
    _isReading = NO;
    // Remove the video preview layer from the viewPreview view's layer.
    [self.videoPreviewLayer removeFromSuperlayer];
}

-(void)dismissAlert:(UIAlertView *) alertView {
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
    
    BMTabViewController* tabbar = (BMTabViewController*) self.tabBarController;
    [tabbar.customTabBar manuallySelectTabBarItemAtIndex:0];
}

- (IBAction)startStopScanningCode:(id)sender {
    if (!_isReading) {
        if ([self startReading]) {
            NSLog(@"startQRReading");
        }
    }
    else {
        [self stopReading];
        NSLog(@"stopQRReading");
    }
    _isReading = !_isReading;
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        // Get the metadata object.
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            NSLog(@"QR CODE DECODED: %@", [metadataObj stringValue]);
            if (fabs([lastCapturedTime timeIntervalSinceNow]) > 4.0) {
                NSLog(@"QR CODE SCANNED: %@", [metadataObj stringValue]);
                lastCapturedTime = [NSDate date];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //                    [self stopReading];
                    
                    NSURL* requestUrl = [NSURL URLWithString:[metadataObj stringValue]];
                    [SOCKET_HANDLER sendIdentificationRequestToURL:requestUrl
                                                   completionBlock:^(NSHTTPURLResponse *response, NSError *error) {
                                                       
                                                   }];
                    BMTabViewController* tabbar = (BMTabViewController*) self.tabBarController;
                    [tabbar.customTabBar manuallySelectTabBarItemAtIndex:0];
                });
            }
        }
    }
}

#pragma mark - Socket Delegates Methods

- (void)socketDidReceiveTryRequest {
    BMTabViewController* tabbar = (BMTabViewController*) self.tabBarController;
    [tabbar.customTabBar manuallySelectTabBarItemAtIndex:0];
    
    if  ([SOCKET_HANDLER.delegate respondsToSelector:@selector(socketDidReceiveTryRequest)])
        [SOCKET_HANDLER.delegate socketDidReceiveTryRequest];
}

@end
