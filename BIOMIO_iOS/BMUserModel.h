//
//  BMUserModel.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 1/20/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BMUserModel : NSObject

@property (nonatomic, copy) NSString *appID;
@property (nonatomic, copy) NSString *deviceID;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *refreshToken;
@property (nonatomic, copy) NSString *privateKey;
@property (nonatomic, copy) NSString *telephoneNumber;

+ (BMUserModel*) createUser;
+ (BMUserModel*) currentUser;

/**
 *  Creates a user backup and removes the current user account.
 *
 *  @return empty user account
 */
+ (BMUserModel*) logOut;
+ (BMUserModel*) updateCurrentUserWithValue: (NSString*) pValue ForKey: (NSString*) pKey;
+ (BOOL) isRegistrationRequired;

#pragma mark - Backup

+ (void) createUserBackup;
+ (BOOL) isUserBackupAvailable;
+ (void) restoreUserBackup;
+ (void) removeUserBackup;
@end