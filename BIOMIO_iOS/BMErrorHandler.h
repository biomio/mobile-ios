//
//  BMErrorHandler.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 7/22/16.
//  Copyright © 2016 BIOMIO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BMErrorHandler : NSObject

+ (NSError*) errorWithCode: (NSInteger) errorCode;
+ (NSDictionary*) errorDictionary;

@end
