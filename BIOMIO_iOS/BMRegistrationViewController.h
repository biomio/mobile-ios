//
//  BMRegistrationViewController.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 3/25/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@class RSProgressBar;
@interface BMRegistrationViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewPreview;
@property (strong, nonatomic) IBOutlet UITextField *secretTextfield;
@property (strong, nonatomic) IBOutlet UIButton *cancelScannerPreview;
@property (strong, nonatomic) IBOutlet UIButton *scanButton;
@property (strong, nonatomic) IBOutlet UIButton *infoButton;
@property (strong, nonatomic) IBOutlet UIView *QRCodeView;
@property (strong, nonatomic) IBOutlet UIView *textInputView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *secretSegmentedCtrl;
@property (nonatomic, strong) RSProgressBar* registrationProgressBar;
@property (strong, nonatomic) IBOutlet UIButton *okCodeButton;

- (IBAction)startStopScanningCode:(id)sender;

@end
