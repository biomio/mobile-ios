//
//  BMMenuView.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 29.08.15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol BMMenuViewDelegate <NSObject>

- (void) cancelButtonPressed;
@optional
- (void) menuButtonPressedAtIndex: (NSInteger) pIndex;

@end

@interface BMMenuView : UIView <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (weak) id <BMMenuViewDelegate> delegate;
@property (strong, nonatomic) IBOutlet UICollectionView *menuCollectionView;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelBarButton;
@property (nonatomic, assign) BOOL visible;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *blurView;

- (void) setMenuCollectionViewWithTitle: (NSArray*) pTitles images: (NSArray*) pImages;

@end
