//
//  BMUserModel.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 1/20/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BMUserModel.h"
// libs/frameworks
#import <SAMKeychain/SAMKeychain.h>

@implementation BMUserModel

#pragma mark - Create/Get/Update
+ (BMUserModel*) createUser {
    BMUserModel* user = [[BMUserModel alloc] init];
    user.deviceID = DEVICE_ID;
    [BMUserModel saveCustomObject:user key:BIOMIO_USER];
    
    return user;
}

+ (BMUserModel*) currentUser {
    return [BMUserModel loadCustomObjectWithKey:BIOMIO_USER];
}

+ (BOOL) isRegistrationRequired {
    BMUserModel* currentUser = [BMUserModel currentUser];
    if (currentUser) {
        NSError* error;
        NSString* privateKey = [SAMKeychain passwordForService:BIOMIO_PRIVKEY account:BIOMIO_KEYCHAIN error:&error];
        if (!privateKey)
            privateKey = [BMUserModel currentUser].privateKey;
        BOOL registrationRequired = (currentUser.appID && privateKey) ? NO : YES;
        return registrationRequired;
    }
    return YES;
}

+ (BMUserModel*) updateCurrentUserWithValue: (NSString*) pValue ForKey: (NSString*) pKey {
    BMUserModel* currentUser = [BMUserModel currentUser];
    if ([pKey isEqualToString:BIOMIO_PRIVKEY])
        currentUser.privateKey = pValue;
    else if ([pKey isEqualToString:BIOMIO_APP_ID])
        currentUser.appID = pValue;
    else if ([pKey isEqualToString:BIOMIO_DEVICE_ID])
        currentUser.deviceID = pValue;
    else
        return nil;
    
    [BMUserModel saveCustomObject:currentUser key:BIOMIO_USER];
    return currentUser;
}

+ (BMUserModel*) logOut {
    [self createUserBackup];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:BIOMIO_USER];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"registrationSuccess"];
    NSError* error;
    [SAMKeychain deletePasswordForService:BIOMIO_PRIVKEY account:BIOMIO_KEYCHAIN error:&error];
    return [BMUserModel currentUser];
}

#pragma mark - Storing user info in NSUserDefaults

+ (void)saveCustomObject:(BMUserModel *)object key:(NSString *)key {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    [[NSUserDefaults standardUserDefaults] setObject:encodedObject forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.deviceID forKey:BIOMIO_DEVICE_ID];
    [encoder encodeObject:self.appID forKey:BIOMIO_APP_ID];
    [encoder encodeObject:self.privateKey forKey:BIOMIO_PRIVKEY];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.deviceID = [decoder decodeObjectForKey:BIOMIO_DEVICE_ID];
        self.appID  = [decoder decodeObjectForKey:BIOMIO_APP_ID];
        self.privateKey = [decoder decodeObjectForKey:BIOMIO_PRIVKEY];
    }
    return self;
}

+ (BMUserModel *)loadCustomObjectWithKey:(NSString *)key {
    NSData *encodedObject = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    BMUserModel *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}

#pragma mark - Misc

+ (NSString*) generateRandomStringWithPrefix: (NSString*) pPrefix {
    int len = 25;
    NSMutableString *lUserID = [NSMutableString stringWithCapacity: len+6];
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    lUserID = [NSMutableString stringWithFormat:@"%@", pPrefix];
    for (int i=0; i<len; i++) {
        [lUserID appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((u_int32_t)[letters length])]];
    }
    return lUserID;
}

#pragma mark - Backup

+ (void) createUserBackup {
    BMUserModel* currentUser = [BMUserModel currentUser];
    [[NSUserDefaults standardUserDefaults]setValue: currentUser.deviceID forKey:BIOMIO_DEVICE_ID_BACKUP];
    [[NSUserDefaults standardUserDefaults]setValue: currentUser.appID forKey:BIOMIO_APP_ID_BACKUP];
    
    // save privatekey to keychain
    NSError* error;
    NSString* lPrivateKey = [SAMKeychain passwordForService:BIOMIO_PRIVKEY account:BIOMIO_KEYCHAIN error:&error];
    [SAMKeychain setPassword:lPrivateKey forService:BIOMIO_PRIVKEY_BACKUP account:BIOMIO_KEYCHAIN];
}

+ (BOOL) isUserBackupAvailable {
    if ([[NSUserDefaults standardUserDefaults] valueForKey:BIOMIO_DEVICE_ID_BACKUP] && [[NSUserDefaults standardUserDefaults] valueForKey:BIOMIO_APP_ID_BACKUP] && [SAMKeychain passwordForService:BIOMIO_PRIVKEY_BACKUP account:BIOMIO_KEYCHAIN]) {
        return YES;
    }
    return NO;
}

+ (void) restoreUserBackup {
    if ([BMUserModel isUserBackupAvailable]) {
        BMUserModel* currentUser = [BMUserModel currentUser];
        currentUser.deviceID = [[NSUserDefaults standardUserDefaults] valueForKey:BIOMIO_DEVICE_ID_BACKUP];
        currentUser.appID = [[NSUserDefaults standardUserDefaults] valueForKey:BIOMIO_APP_ID_BACKUP];
        [BMUserModel saveCustomObject:currentUser key:BIOMIO_USER];
        
        // private key
        NSError* error;
        NSString* lPrivateKey = [SAMKeychain passwordForService:BIOMIO_PRIVKEY_BACKUP account:BIOMIO_KEYCHAIN error:&error];
        if (lPrivateKey) {
            NSError* writeError;
            [SAMKeychain setPassword:lPrivateKey forService:BIOMIO_PRIVKEY account:BIOMIO_KEYCHAIN error:&writeError];
        }
        NSLog(@"RESTORING USER BACKUP");
    }
}

+ (void) removeUserBackup {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:BIOMIO_DEVICE_ID_BACKUP];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:BIOMIO_APP_ID_BACKUP];
}

@end
