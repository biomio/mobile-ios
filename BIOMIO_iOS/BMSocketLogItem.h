//
//  BMSocketLogItem.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 10/2/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM (NSInteger, logItemType) {
    logItemTypeMessageReceived      = 0,
    logItemTypeMessageSent          = 1,
    logItemTypeError                = 2,
};

@interface BMSocketLogItem : NSObject
/**
 *  Timestamp of the time the log was added
 */
@property (nonatomic, assign) NSTimeInterval timestamp;
/**
 *  Log body
 */
@property (nonatomic, copy) NSDictionary* message;
/**
 *  Message type
 */
@property (nonatomic, copy) NSString* oid;
/**
 *  Log type: ENUM logItemType
 */
@property (nonatomic) logItemType type;

+ (instancetype) logItemWithDictionary:(NSDictionary*) pDict type:(logItemType) pType;
@end
