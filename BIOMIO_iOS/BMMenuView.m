//
//  BMMenuView.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 29.08.15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import "BMMenuView.h"
#import "MenuCollectionViewCell.h"

@implementation BMMenuView {
    NSArray* menuTitleArray;
    NSArray* menuImageArray;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSBundle mainBundle]loadNibNamed:@"BMMenuView" owner:self options:nil];
        [self addSubview:self.mainView];
        [self commonInit];
    }
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:@"BMMenuView" owner:self options:nil];
        [self addSubview:self.mainView];
        [self commonInit];
    }
    return self;
}
- (IBAction)cancelBarButtonPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(cancelButtonPressed)])
        [self.delegate cancelButtonPressed];
}

- (void)commonInit {
    self.mainView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [self.menuCollectionView registerNib:[UINib nibWithNibName:@"MenuCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];

    self.versionLabel.text = [NSString stringWithFormat:@"v%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    self.blurView.layer.cornerRadius = 10;
    self.blurView.clipsToBounds = YES;
    self.mainView.layer.cornerRadius = 10;
    
    menuTitleArray = @[@"Diagnostics", @"Log out", @"Help", @"URL"];
    menuImageArray = @[[UIImage imageNamed:@"repair_instruments"], [UIImage imageNamed:@"logOut"], [UIImage imageNamed:@"Help_icon"], [UIImage imageNamed:@"repair_instruments"]];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [menuTitleArray count];
}

- (MenuCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Cell";
    MenuCollectionViewCell* cell = [self.menuCollectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.menuItemTitleLabel.text = [menuTitleArray objectAtIndex:indexPath.item];
    cell.menuItemImageView.image = [menuImageArray objectAtIndex:indexPath.item];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(menuButtonPressedAtIndex:)])
        [self.delegate menuButtonPressedAtIndex:indexPath.item];

    switch (indexPath.item) {
        case 0: {
            
            break;
        }
        default:
            break;
    }
}

- (void) setMenuCollectionViewWithTitle: (NSArray*) pTitles images: (NSArray*) pImages {
    menuTitleArray = [NSArray arrayWithArray:pTitles];
    menuImageArray = [NSArray arrayWithArray:pImages];
    [self.menuCollectionView reloadData];
}

@end
