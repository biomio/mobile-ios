//
//  BMErrorHandler.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 7/22/16.
//  Copyright © 2016 BIOMIO. All rights reserved.
//

#import "BMErrorHandler.h"

#define ERROR_DESCRIPTION_FILENAME      @"ErrorDescription"
@implementation BMErrorHandler

+ (NSError*) errorWithCode: (NSInteger) errorCode {
    NSDictionary* errorsDict = [BMErrorHandler errorDictionary];
    NSDictionary* errorDictionary = [errorsDict objectForKey:[NSString stringWithFormat:@"%lu", (long)errorCode]];
    if (errorDictionary) {
        NSError* error = [BMErrorHandler errorWithDictionary:errorDictionary];
        return error;
    }
    return nil;
}

+ (NSDictionary*) errorDictionary {
    NSString* filePath = [[NSBundle mainBundle] pathForResource:ERROR_DESCRIPTION_FILENAME ofType:@"plist"];
    NSDictionary* errors = [NSDictionary dictionaryWithContentsOfFile:filePath];
    return errors;
}

+ (NSError*) errorWithDictionary:(NSDictionary*) errorDictionary {
    NSMutableDictionary* localizedDict = [NSMutableDictionary dictionaryWithDictionary:errorDictionary[@"userInfo"]];
    [localizedDict addEntriesFromDictionary:@{NSLocalizedDescriptionKey:localizedDict[@"localizedDescription"]}];
    [localizedDict removeObjectForKey:@"localizedDescription"];
    NSError* error = [NSError errorWithDomain:ERROR_DOMAIN code:[errorDictionary[@"code"]integerValue] userInfo: localizedDict];
    return error;
}

@end
