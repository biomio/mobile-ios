//
//  BMStateMachine.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 5/15/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import "TKStateMachine.h"
// models
#import "TKState.h"
#import "TKEvent.h"
#import "TKTransition.h"

#define STATE_MACHINE           [BMStateMachine sharedHandler]

@interface BMStateMachine : TKStateMachine

/**
 *  Returns the default singleton state machine instance.
 */
+ (instancetype) sharedHandler;

#pragma mark - STATES
#pragma mark Connection
@property (strong, nonatomic) TKState* disconnectedState;
@property (strong, nonatomic) TKState* connectionOpenState;
@property (strong, nonatomic) TKState* connectionState;
@property (strong, nonatomic) TKState* connectionFailureState;
#pragma mark Handshake
@property (strong, nonatomic) TKState* regularHelloState;
@property (strong, nonatomic) TKState* registrationHelloState;

@property (strong, nonatomic) TKState* regularHandshakeState;
@property (strong, nonatomic) TKState* registrationHandshakeState;
#pragma mark Standbye
@property (strong, nonatomic) TKState* standbyeState;
#pragma mark Authentication
@property (strong, nonatomic) TKState* biometricAuthState;

#pragma mark - EVENTS

#pragma mark Connection
@property (strong, nonatomic) TKEvent* connectionDidStart;
@property (strong, nonatomic) TKEvent* connectionDidFail;
@property (strong, nonatomic) TKEvent* connectionNotAvailable;
@property (strong, nonatomic) TKEvent* connectionDidOpen;
@property (strong, nonatomic) TKEvent* socketWillClose;
@property (strong, nonatomic) TKEvent* registrationDidFail;

#pragma mark Regular handshake
@property (strong, nonatomic) TKEvent* connectionDidOpenWithUser;
@property (strong, nonatomic) TKEvent* regularServerHelloDidReceive;
#pragma mark Registration handshake
@property (strong, nonatomic) TKEvent* connectionDidOpenWithoutUser;
@property (strong, nonatomic) TKEvent* registrationServerHelloDidReceive;

@property (strong, nonatomic) TKEvent* handshakeMessageDidSend;
#pragma mark Authentication. Try request
@property (strong, nonatomic) TKEvent* tryMessageDidReceive;
@property (strong, nonatomic) TKEvent* biometricAuthDidSucceed;
@property (strong, nonatomic) TKEvent* biometricAuthDidExpire;
@property (strong, nonatomic) TKEvent* biometricAuthDidFail;
@property (strong, nonatomic) TKEvent* biometricAuthCanceled;
@property (strong, nonatomic) TKEvent* biometricAuthCanceledByServer;
#pragma mark Disconnection
@property (strong, nonatomic) TKEvent* byeMessageDidReceive;
@property (strong, nonatomic) TKEvent* serverDidReceiveProbe;

@end
