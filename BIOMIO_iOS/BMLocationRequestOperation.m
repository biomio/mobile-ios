//
//  BMLocationRequestOperation.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 11/20/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

#import "BMLocationRequestOperation.h"
// handlers
#import "BMSocketHandler.h"
#import "BMErrorHandler.h"
// models
#import "BMTryRequestRoot.h"
#import "BMTryAuthMethod.h"
@implementation BMLocationRequestOperation

#pragma mark - Init

- (id) initWithAuthMethod: (BMTryAuthMethod*) pAuthMethod timeout:(NSTimeInterval) pTimeout delegate:(id<LocationRequestOperationDelegate>) locationDelegate {
    if (self = [super init]) {
        self.authMethod = pAuthMethod;
        self.accuracyMeters = [pAuthMethod.deviceSettings integerValue];
        self.timeout = pTimeout;
        self.locationAccuracy = [self locationAccuracyWithAccuracyInMeters:self.accuracyMeters];
        self.delegate = locationDelegate;
        [self addObserver:self
               forKeyPath:@"finished"
                  options:NSKeyValueObservingOptionNew
                  context:nil];
    }
    return self;
}

-(void)dealloc {
    [self removeObserver:self forKeyPath:@"finished"];
}

-(void)observeValueForKeyPath:(NSString *)keyPath
                     ofObject:(id)object
                       change:(NSDictionary *)change
                      context:(void *)context {
    
    if([keyPath isEqualToString:@"finished"] == YES){
        
        if([self isCancelled] == NO){
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(locationOperationCompleted:)]){
                [self.delegate locationOperationCompleted:self];
            }
        }else {
            if(self.delegate != nil && [self.delegate respondsToSelector:@selector(locationOperationCanceled:)]){
                [self.delegate locationOperationCanceled:self];
            }
        }
    }
}
/**
 *  Converts location accuracy from meters to INTULocationAccuracy tier value
 *
 *  @param pMeters Accuracy in meters
 *
 *  @return INTULocationAccuracy tier value
 */
- (INTULocationAccuracy) locationAccuracyWithAccuracyInMeters:(NSInteger) pMeters {
    INTULocationAccuracy defaultAccuracy = INTULocationAccuracyBlock;
    if (pMeters <=0)
        return defaultAccuracy;
    else if (pMeters <= 5)
        return INTULocationAccuracyRoom;
    else if (pMeters <= 15)
        return INTULocationAccuracyHouse;
    else if (pMeters <= 100)
        return INTULocationAccuracyBlock;
    else if (pMeters <= 1000)
        return INTULocationAccuracyNeighborhood;
    else if (pMeters >= 1000)
        return INTULocationAccuracyCity;
    else
        return defaultAccuracy;
    /*
     INTULocationAccuracyCity          // 5000 meters or better, received within the last 10 minutes  -- lowest accuracy
     INTULocationAccuracyNeighborhood  // 1000 meters or better, received within the last 5 minutes
     INTULocationAccuracyBlock         // 100 meters or better, received within the last 1 minute
     INTULocationAccuracyHouse         // 15 meters or better, received within the last 15 seconds
     INTULocationAccuracyRoom          // 5 meters or better, received within the last 5 seconds      -- highest accuracy
     */
}

- (void)start {
    @autoreleasepool {
        NSLog(@"LOCATION OPERATION STARTED");
        if (self.isCancelled)
            return;
        if (self.locationAccuracy) {
            self.authMethod.status = AuthMethodStatusActive;
            dispatch_async(dispatch_get_main_queue(), ^{
                self.requestID = [[INTULocationManager sharedInstance] requestLocationWithDesiredAccuracy:self.locationAccuracy
                                                                                                  timeout:self.timeout
                                                                                                    block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                                                                                        if (status == INTULocationStatusSuccess ||
                                                                                                            (status == INTULocationStatusError && currentLocation) ||
                                                                                                            status == INTULocationStatusTimedOut) {
                                                                                                            [self saveAuthSamplesForLocation:currentLocation];
                                                                                                            [SOCKET_HANDLER sendProbeMessageWithAuthMethod:self.authMethod
                                                                                                                                                authResult:nil
                                                                                                                                                    status:BMProbeStatusSuccess
                                                                                                                                                     error:nil];
                                                                                                            [(NSObject*)self.delegate performSelectorOnMainThread:@selector(locationOperationCompleted:) withObject:self waitUntilDone:NO];
                                                                                                        }
                                                                                                        else if (status == INTULocationStatusError && !currentLocation) {
                                                                                                            [(NSObject*)self.delegate performSelectorOnMainThread:@selector(locationOperationFailed:) withObject:self waitUntilDone:NO];
                                                                                                            NSError* error = [BMErrorHandler errorWithCode:44005];
                                                                                                            [SOCKET_HANDLER sendProbeMessageWithAuthMethod:self.authMethod
                                                                                                                                                authResult:nil
                                                                                                                                                    status:BMProbeStatusFailed
                                                                                                                                                     error:error];
                                                                                                        }
                                                                                                        else {
                                                                                                            [(NSObject*)self.delegate performSelectorOnMainThread:@selector(locationOperationCompleted:) withObject:self waitUntilDone:NO];
                                                                                                        }
                                                                                                    }];
            });
        }
        if (self.isCancelled)
            return;
        [self main];
    }
}

- (void) saveAuthSamplesForLocation:(CLLocation*) location {
    if (location) {
        NSString* latString = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
        NSString* longString = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
        NSString* accString = [NSString stringWithFormat:@"%f",location.horizontalAccuracy];
        NSString* resultString = [NSString stringWithFormat:@"%@,%@,%@", latString, longString,accString];
        [self.authMethod addAuthSample:resultString];
        //    [self.authMethod addAuthSample:[NSString stringWithFormat:@"%f",location.coordinate.latitude]];
        //    [self.authMethod addAuthSample:[NSString stringWithFormat:@"%f",location.coordinate.longitude]];
        //    [self.authMethod addAuthSample:[NSString stringWithFormat:@"%f",location.horizontalAccuracy]];
    }
}

@end
