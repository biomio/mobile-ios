//
//  BMTryRequestHandler.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 11/19/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

// handlers
#import "BMTryRequestHandler.h"
#import "BMTryAuthMethod.h"
#import "BMTryRequestRoot.h"
#import "BMLocationRequestOperation.h"
#import "BMSocketHandler.h"

@interface BMTryRequestHandler () <TryRequestDelegate>
@end

@implementation BMTryRequestHandler {
    NSOperationQueue *nonInteractiveQueue;
}

#pragma mark - Init

+ (instancetype) sharedTryHandler {
    static BMTryRequestHandler *sharedTryHandler = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^ {
        sharedTryHandler = [self new];
    });
    return sharedTryHandler;
}

- (id)init {
    if (self = [super init]) {
        self.tryRequests = [NSMutableArray new];
        
        nonInteractiveQueue = [NSOperationQueue new];
        nonInteractiveQueue.maxConcurrentOperationCount = 3;
        nonInteractiveQueue.qualityOfService = NSQualityOfServiceUserInteractive;
    }
    return self;
}

#pragma mark - Run/Status
// TODO: update
- (BOOL) readyToRun {
    if ([self nextTryRequest])
        return YES;
    return NO;
}

- (void) startTryRequest:(BMTryRequestRoot*) tryRequest {
    if ([tryRequest.nonInteractiveAuthMethods count])
        [self addNonInteractiveAuthMethodsToOperationQueue:tryRequest.nonInteractiveAuthMethods];
}

- (BOOL) isUploadingProbeData {
    for (BMTryRequestRoot* tryRequest in self.tryRequests) {
        for (BMTryAuthMethod* authMethod in tryRequest.authMethods) {
            if (authMethod.status == AuthMethodStatusSent ||
                (authMethod.status == AuthMethodStatusActive &&
                 authMethod.authSamples.count>0)) {
                    return YES;
                }
        }
    }
    return NO;
}

#pragma mark - Authmethods
#pragma mark Interactive Authmethods

- (BMTryAuthMethod*) nextInteractiveAuthMethod {
    BMTryRequestRoot* nextTryRequest = [self nextTryRequest];
    for (BMTryAuthMethod* authM in nextTryRequest.selectedAuthMethod) {
        if (authM.status == AuthMethodStatusClean || authM.status == AuthMethodStatusActive)
            return authM;
    }
    return nil;
}

- (void)setActiveInteractiveAuthMethod:(BMTryAuthMethod *)activeInteractiveAuthMethod {
    if (activeInteractiveAuthMethod) {
        activeInteractiveAuthMethod.status = AuthMethodStatusActive;
        _activeInteractiveAuthMethod = activeInteractiveAuthMethod;
    } else {
        _activeInteractiveAuthMethod = nil;
    }
}

#pragma mark NonInteractive Authmethods

- (void) addNonInteractiveAuthMethodsToOperationQueue:(NSArray*) pAuthMethods {
    for (BMTryAuthMethod* authMethod in pAuthMethods) {
        if ([authMethod.title isEqualToString:@"location"]) {
            BMLocationRequestOperation* locationOper = [[BMLocationRequestOperation alloc]initWithAuthMethod:authMethod
                                                                                                     timeout:[self tryRequestWithID: authMethod.parrentTryID].timeout-15
                                                                                                    delegate:self];
            [nonInteractiveQueue addOperation:locationOper];
        }
    }
}

- (NSArray*) nonInteractiveOperationQueue {
    return nonInteractiveQueue.operations;
}

#pragma mark AuthSamples

- (void) addAuthResultToActiveInteractiveAuthMethod:(id)pResult {
    [self.activeTryRequest addAuthResultToActiveInteractiveAuthMethod:pResult];
}


#pragma mark - TryRequest

- (void) addTryRequest: (BMTryRequestRoot*) tryRequest {
    tryRequest.delegate = self;
    [self.tryRequests addObject:tryRequest];
    [self startTryRequest:tryRequest];
}

#pragma mark TryRequest - Getter

- (BMTryRequestRoot *)nextTryRequest {
    for (BMTryRequestRoot* tryReq in self.tryRequests) {
        if (![tryReq completed])
            return tryReq;
    }
    return nil;
}

- (BMTryRequestRoot*) activeTryRequest {
    for (BMTryRequestRoot* tryRequest in self.tryRequests) {
        if (tryRequest.status == TryRequestStatusActive)
            return tryRequest;
    }
    return nil;
}

- (BMTryRequestRoot*) tryRequestWithID: (NSString*) pID {
    if (pID){
        for (BMTryRequestRoot* tryReq in self.tryRequests) {
            if ([tryReq.identifier isEqualToString:pID])
                return tryReq;
        }
    }
    return nil;
}

- (BMTryAuthMethod*) authMethodWithTitle:(NSString*) title
                            parrentTryID:(NSString*) tryID {
    BMTryRequestRoot* tryRequest = [self tryRequestWithID:tryID];
    if (tryRequest) {
        return [tryRequest authMethodWithTitle:title];
    }
    return nil;
}

#pragma mark - Complete

- (void) completeAuthMethod: (BMTryAuthMethod*) authMethod {
    authMethod.status = AuthMethodStatusCompleted;
    [[self tryRequestWithID:authMethod.parrentTryID] status];
    [SOCKET_HANDLER forceUpdateStateVisualization];
}

#pragma mark - Cancel

- (void) cancelTryRequest: (BMTryRequestRoot*) tryRequest {
    [tryRequest cancel];
    [self.nonInteractiveOperationQueue enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BMLocationRequestOperation* operation = (BMLocationRequestOperation*) obj;
        if ([operation.authMethod.parrentTryID isEqualToString:tryRequest.identifier])
            [operation cancel];
    }];
}

- (void)cancelTryRequestWithID:(NSString *)tryID {
    BMTryRequestRoot* tryRequest = [self tryRequestWithID:tryID];
    if (tryRequest)
        [self cancelTryRequest:tryRequest];
}

#pragma mark - LocationRequestOperationDelegate

- (void) locationOperationCompleted:(BMLocationRequestOperation *)operation {
    if ([self.delegate respondsToSelector:@selector(nonInteractiveAuthMethodsDidUpdate)])
        [self.delegate nonInteractiveAuthMethodsDidUpdate];
}

- (void)locationOperationFailed:(BMLocationRequestOperation *)operation {
    if ([self.delegate respondsToSelector:@selector(nonInteractiveAuthMethodsDidUpdate)])
        [self.delegate nonInteractiveAuthMethodsDidUpdate];
}

#pragma mark - TryRequestDelegate

- (void)tryRequestDidExpire:(BMTryRequestRoot *)request {
    if ([self.delegate respondsToSelector:@selector(tryRequestDidExpire:)])
        [self.delegate tryRequestDidExpire:request];
}

@end
