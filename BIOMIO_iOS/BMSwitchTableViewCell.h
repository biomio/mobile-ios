//
//  BMSwitchTableViewCell.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 10/4/16.
//  Copyright © 2016 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BMSwitchTableViewCell;
@protocol SwitchCellDelegate <NSObject>
- (void) switchCell:(BMSwitchTableViewCell*) cell didChangeSwitchState:(UISwitch*) switchCtrl;
@end


@interface BMSwitchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UISwitch *switchControl;

@property id <SwitchCellDelegate> delegate;

@end
