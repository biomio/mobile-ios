//
//  BMSocketHandler.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 1/22/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
// handlers
#import "BMTryRequestHandler.h"

#define SOCKET_HANDLER               [BMSocketHandler sharedSocket]
#define TRY_REQUEST_HANDLER          [BMSocketHandler sharedSocket].tryRequestHandler

#pragma mark - BMProbeStatusEnum

@interface BMProbeStatusEnum : NSObject

typedef NS_ENUM(NSInteger, BMProbeStatus) {
    BMProbeStatusSuccess              = 0,
    BMProbeStatusFailed               = 1,
    BMProbeStatusCanceled             = 2
};

typedef NS_ENUM(NSInteger, BMSocketState) {
    BMSocketStateStandbye           = 1,
    BMSocketStateConnection         = 2,
    BMSocketStateDisconnected       = 3
};

+ (NSString*)probeStatusToString:(BMProbeStatus)status;

@end

#pragma mark - BMSocketDelegate

@class BMTryRequestRoot, BMTryAuthMethod;

@protocol BMSocketDelegate <NSObject>
- (void)socketDidReceiveTryRequest;
- (void)socketRequiresRegistration;
- (void)socketDidOpenUrlRegistrationCode;
- (void)socketDidChangeState:(BMSocketState)state
                   uploading:(BOOL)busy;
@optional
- (void)socketProbeDidTimeOut;
- (void)socketDidFailWithError:(NSError*)error;
@end

@interface NSData (NSData_Conversion)
- (NSString *)hexadecimalString;
@end

#pragma mark - BMSocketHandler

@interface BMSocketHandler : NSObject

/**
 *  Default singleton socket handler.
 */
+ (BMSocketHandler*)sharedSocket;

@property (weak) id <BMSocketDelegate> delegate;
@property (nonatomic, strong) BMTryRequestHandler* tryRequestHandler;

- (BOOL)isBusyUploadingProbe;
- (void)forceUpdateStateVisualization;

- (void)restartWithUserReset:(BOOL)reset;

/**
 *  @brief Send probe message with authentication result of the authmethod
 *
 *  @param authMethod  BMTryAuthMethod object
 *  @param result      Authentication result (Base64 string for photos; @"true"/@"false" for TouchID etc). If nil - authMethod.authSamples is used;
 *  @param status      Authentication status: BMProbeStatus enum value (success/failed/canceled)
 *  @param pError      Failure error if any. Used together with status=BMProbeStatusFailed
 */
- (void)sendProbeMessageWithAuthMethod:(BMTryAuthMethod*)authMethod
                            authResult:(NSString*)result
                                status:(BMProbeStatus)probeStatus
                                 error:(NSError*)pError;

- (void)sendIdentificationRequestToURL:(NSURL*)requestUrl
                       completionBlock:(void(^)(NSHTTPURLResponse* response, NSError* error))completion;

#pragma mark - Server URL

/**
 Currently active server URL
 
 @return Currently active server absolute URL string
 */
+ (NSString*)serverUrl;

/**
 Restore app default server URL
 
 @return returns NO if current URL equals the default URL. Otherwise returns YES
 */
+ (BOOL)restoreDefaultServerUrl;

/**
 Sets a new server URL
 
 @param newServerUrl new server URL string
 @return returns YES unless the new URL equals the currently active URL
 */
+ (BOOL)setServerUrl:(NSString*)newServerUrl;

@end
