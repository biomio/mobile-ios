//
//  UIImage+Additions.h
//  BIOMIO_iOS
//
//  Created by Roman Andruseiko on 10/8/14.
//  Copyright (c) 2014 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Additions)

- (UIImage *)fixOrientation;

@end
