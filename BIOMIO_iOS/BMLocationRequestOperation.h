//
//  BMLocationRequestOperation.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 11/20/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

#import <Foundation/Foundation.h>
// handlers
#import <INTULocationManager/INTULocationManager.h>

@class BMLocationRequestOperation, BMTryAuthMethod;

@protocol LocationRequestOperationDelegate <NSObject>
/**
 *  @brief Called after the location operation finishes
 *  @discussion In your delegate method, pass the whole class as an object back to the caller so that the caller can access it. Because you need to cast the operation to NSObject and return it on the main thread, the delegate method can't have more than one argument.
 *  @param operation BMLocationRequestOperation object
 */
- (void) locationOperationCompleted:(BMLocationRequestOperation*)operation;
@optional
- (void) locationOperationFailed:(BMLocationRequestOperation*)operation;
- (void) locationRequestDidCompleteWithLocation:(CLLocation*) pCurrentLocation achievedAccuracy:(INTULocationAccuracy) pAchievedAccuracy status: (INTULocationStatus) pStatus;
- (void) locationOperationCanceled:(BMLocationRequestOperation*)operation;
@end

@interface BMLocationRequestOperation : NSOperation
@property (nonatomic, strong) BMTryAuthMethod* authMethod;
@property id <LocationRequestOperationDelegate> delegate;
@property (nonatomic, assign) NSInteger accuracyMeters;
@property (nonatomic, assign) INTULocationAccuracy locationAccuracy;
@property (nonatomic, assign) NSInteger timeout;
@property (nonatomic, assign) INTULocationRequestID requestID;

- (id) initWithAuthMethod: (BMTryAuthMethod*) pAuthMethod timeout:(NSTimeInterval) pTimeout delegate:(id<LocationRequestOperationDelegate>) locationDelegate;

@end
