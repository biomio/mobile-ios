//
//  BMSocketLogger.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 10/1/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

#import "BMSocketLogger.h"
// handlers
#import "BMSocketHandler.h"
// libs/frameworks
#import "NSData+GZIP.h"
// models
#import "BMSocketLogItem.h"
#import "BMUserModel.h"

#define LOG_FILENAME                        @"logs.plist"
#define LOG_FILE_SIZE_LIMIT                 512      // KB
#define LOG_BUFFER_SIZE                     10
#define LOG_REMOVE_OLD_OUT_OF_ALL           0.3
#define LOG_MESSAGES_TO_CONSOLE             1

@implementation BMSocketLogger {
    NSMutableArray *logArray;
    NSMutableArray* logBuffer;
    NSMutableData* receivedData;
}

+ (instancetype)defaultLogger {
    static BMSocketLogger *defaultLogger = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^ {
        defaultLogger = [self new];
    });
    return defaultLogger;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.writeToStorage = YES;
        NSArray* logsInStorage =  [self readLogsFromStorage];
        logArray = logsInStorage ? [logsInStorage mutableCopy] : [NSMutableArray new];
        logBuffer = [NSMutableArray new];
    }
    return self;
}

#pragma mark - Add

- (void) logMessage: (id) pLog {
    if ([pLog isKindOfClass:[NSString class]]) {
        NSData *objectData = [pLog dataUsingEncoding:NSUTF8StringEncoding];
        NSError* jsonError = nil;
        NSDictionary *messageDict = [NSJSONSerialization JSONObjectWithData:objectData
                                                                    options:NSJSONReadingMutableContainers
                                                                      error:&jsonError];
        
        NSInteger seq = [messageDict[@"header"][@"seq"] integerValue];
        if (seq % 2)
            [self addLog:[BMSocketLogItem logItemWithDictionary:messageDict type:logItemTypeMessageReceived]];
        else
            [self addLog:[BMSocketLogItem logItemWithDictionary:messageDict type:logItemTypeMessageSent]];
    }
    else if ([pLog isKindOfClass:[NSMutableDictionary class]] || [pLog isKindOfClass:[NSDictionary class]]) {
        NSInteger seq = [pLog[@"header"][@"seq"] integerValue];
        if (seq % 2)
            [self addLog:[BMSocketLogItem logItemWithDictionary:pLog type:logItemTypeMessageReceived]];
        else
            [self addLog:[BMSocketLogItem logItemWithDictionary:pLog type:logItemTypeMessageSent]];
    }
    else
        NSAssert(false, @"Message of incompartible type. Supported types: NSString and NSDictionary/NSMutableDictionary");
}

- (void) logError: (NSError *)pError {
    if (pError) {
        NSDictionary* errorDict = @{
                                    @"code" : [NSNumber numberWithInteger: pError.code],
                                    @"description" : pError.localizedDescription ? pError.localizedDescription : @""
                                    };
        BMSocketLogItem* errorLogItem = [BMSocketLogItem logItemWithDictionary:errorDict
                                                                          type:logItemTypeError];
        [self addLog:errorLogItem];
    }
}

- (void) addLog:(BMSocketLogItem*) logItem {
    if (LOG_MESSAGES_TO_CONSOLE)
        NSLog(@"%@: %@", logItem.type == logItemTypeMessageSent ? @"SENT" : logItem.type == logItemTypeMessageReceived ? @"RECEIVED" : @"ERROR", logItem.message);
    [logArray addObject:logItem];
    [logBuffer addObject:logItem];
    if ([self.delegate respondsToSelector:@selector(socketLoggerDidUpdateLogs)])
        [self.delegate socketLoggerDidUpdateLogs];
    if (self.writeToStorage) {
        if ([logBuffer count] == LOG_BUFFER_SIZE)
            if ([self writeLogsToStorage:logBuffer]){
                [self cleanBuffer];
                if ([self logsFileSize] > LOG_FILE_SIZE_LIMIT)
                    [self removeOldestLogsInStorage];
            }
    }
}

#pragma mark - Write

- (BOOL) persistLogsToStorage {
    if ([logBuffer count])
        if ([self writeLogsToStorage:logBuffer]) {
            [self cleanBuffer];
            return YES;
        }
    return NO;
}

- (BOOL) writeLogsToStorage:(NSArray*) newLogs {
    NSMutableArray* logs = [NSMutableArray new];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:LOG_FILENAME];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath: path]) {
        logs = [[NSMutableArray alloc] init];
        if ([logs initWithContentsOfFile: path]) {
            logs = [logs initWithContentsOfFile: path];
        }
    }
    else {
        [fileManager createFileAtPath:path contents:nil attributes:nil];
        logs = [[NSMutableArray alloc] initWithCapacity:0];
        
    }
    if (!newLogs)
        newLogs = logBuffer;
    [newLogs enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BMSocketLogItem* logItem = obj;
        NSData* logData = [NSKeyedArchiver archivedDataWithRootObject:logItem];
        [logs addObject:logData];
    }];
    BOOL wrote = [logs writeToFile:path atomically:YES];
    if (wrote && [self.delegate respondsToSelector:@selector(socketLoggerDidWriteToStorage)])
        [self.delegate socketLoggerDidWriteToStorage];
    return wrote;
}

#pragma mark - Remove

- (BOOL) removeOldestLogsInStorage {
    double logsSize = [self logsFileSize];
    NSRange oldestRange;
    oldestRange.location = 0;
    oldestRange.length = (NSUInteger)ceil([logArray count]*LOG_REMOVE_OLD_OUT_OF_ALL);
    [logArray removeObjectsInRange:oldestRange];
    
    NSMutableArray* logs = [NSMutableArray new];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:LOG_FILENAME];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath: path]) {
        logs = [[NSMutableArray alloc] init];
        if ([logs initWithContentsOfFile: path]) {
            logs = [logs initWithContentsOfFile: path];
        }
        [logs removeObjectsInRange:oldestRange];
    }
    else {
    }
    
    BOOL wrote = [logs writeToFile:path atomically:YES];
    logsSize = [self logsFileSize];
    if (wrote && [self.delegate respondsToSelector:@selector(socketLoggerDidRemoveOldLogs)])
        [self.delegate socketLoggerDidRemoveOldLogs];
    if (wrote && [self.delegate respondsToSelector:@selector(socketLoggerDidUpdateLogs)])
        [self.delegate socketLoggerDidUpdateLogs];
    
    return wrote;
}

- (BOOL) cleanLogs {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:LOG_FILENAME];
    NSError* error = nil;
    BOOL success = [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
    [logArray removeAllObjects];
    if ([self.delegate respondsToSelector:@selector(socketLoggerDidCleanLogs)])
        [self.delegate socketLoggerDidCleanLogs];
    if ([self.delegate respondsToSelector:@selector(socketLoggerDidUpdateLogs)])
        [self.delegate socketLoggerDidUpdateLogs];
    
    [self cleanBuffer];
    return (success && [logArray count] == 0);
}

- (void) cleanBuffer {
    [logBuffer removeAllObjects];
    if ([self.delegate respondsToSelector:@selector(socketLoggerDidCleanBuffer)])
        [self.delegate socketLoggerDidCleanBuffer];
}

#pragma mark - Read

- (NSArray*) readLogsFromStorage {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:LOG_FILENAME];
    NSArray* logsData = [NSArray arrayWithContentsOfFile:path];
    NSMutableArray* logs = [NSMutableArray new];
    if ([logsData count]) {
        [logsData enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            BMSocketLogItem* logItem = [NSKeyedUnarchiver unarchiveObjectWithData:obj];
            [logs addObject:logItem];
        }];
        return logs;
    }
    else
        return nil;
}

- (NSDictionary*) getLogDictionary {
    NSMutableDictionary* logs = [NSMutableDictionary new];
    [logArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BMSocketLogItem* logItem = obj;
        [logs setValue:logItem.message forKey:[NSString stringWithFormat:@"%f", logItem.timestamp]];
    }];
    return logs;
}

- (double) logsFileSize {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:LOG_FILENAME];
    NSError* error = nil;
    NSDictionary* logsAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:&error];
    double logSize = [[logsAttributes valueForKey:NSFileSize] doubleValue]/1000; // size in KB
    return logSize;
}

- (NSArray*) allLogs {
    return logArray;
}

#pragma mark - Upload

- (void) uploadLogsWithCompletionHandler: (void(^)(NSHTTPURLResponse* response, NSError* error)) completion {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"https://biom.io:4433/php/commands.php/save_log/%@",[BMUserModel currentUser].appID]];
    NSData* zippedLogs = [self zipLogs];
    // Create the Request
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"POST"];
    
    // Configure the NSURL Session
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration]; //backgroundSessionConfigurationWithIdentifier:@"com.upload"];
    config.HTTPMaximumConnectionsPerHost = 1;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self  delegateQueue:nil];
    
    // Define the Upload task
    NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request
                                                               fromData:zippedLogs
                                                      completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                          NSHTTPURLResponse* httpRespose = (NSHTTPURLResponse*) response;
                                                          if ([self.delegate respondsToSelector:@selector(socketLoggerDidUploadLogsWithResponse:error:)])
                                                              [self.delegate socketLoggerDidUploadLogsWithResponse:httpRespose error:&error];
                                                          if (completion)
                                                              completion(httpRespose, error);
                                                      }];
    // TODO: add progressbar activity
    [uploadTask resume];
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend {
    NSLog(@"DIDSENT %lli TOTAL %lli EXPECTED %lli", bytesSent, totalBytesSent, totalBytesExpectedToSend);
}

- (NSData*) zipLogs {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[self getLogDictionary]
                                                       options:0
                                                         error:&error];
    
    if (!jsonData) {
        NSLog(@"LOGS JSON error: %@", [error localizedDescription]);
    } else {
        //        NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
        
    }
    
    NSData* zippedlogs = [jsonData gzippedDataWithCompressionLevel:.7];
    return zippedlogs;
}

@end
