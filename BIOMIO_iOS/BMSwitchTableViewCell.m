//
//  BMSwitchTableViewCell.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 10/4/16.
//  Copyright © 2016 BIOMIO. All rights reserved.
//

#import "BMSwitchTableViewCell.h"

@implementation BMSwitchTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)switchValueDidChange:(id)sender {
    if ([self.delegate respondsToSelector:@selector(switchCell:didChangeSwitchState:)])
        [self.delegate switchCell:self didChangeSwitchState:sender];    
}

@end
