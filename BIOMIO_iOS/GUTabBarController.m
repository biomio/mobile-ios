//
//  GUTabBarController.m
//
//
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import "GUTabBarController.h"

@interface GUTabBarController ()

@end

@implementation GUTabBarController

- (void)viewDidLoad{
    [super viewDidLoad];
	//hide native tabbar
	for(UIView *view in self.view.subviews){
        if([view isKindOfClass:[UITabBar class]]){
            [view setFrame:CGRectMake(view.frame.origin.x, self.view.frame.size.height, view.frame.size.width, view.frame.size.height)];
        }else{
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, self.view.frame.size.height)];
        }
    }
    
	//add custom tabbar
    GUTabBarView *lCustomTabBar = [[GUTabBarView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - self.tabBar.frame.size.height, self.view.frame.size.width, self.tabBar.frame.size.height)];
    lCustomTabBar.delegate = self;
    [self.view addSubview:lCustomTabBar];
    [self tabBarItemPressed:0];
}

- (void) tabBarItemPressed:(NSInteger)pTag {
    [self.delegate tabBarController:self shouldSelectViewController:[[self viewControllers] objectAtIndex:pTag]];
    [self setSelectedIndex:pTag];
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    [self tabBarItemPressed:item.tag];
}

@end
