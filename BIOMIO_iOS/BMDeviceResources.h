//
//  BMDeviceResources.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 8/12/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM (NSInteger, dataNetworkType) {
    dataNetworkTypeOffline      = 0,
    dataNetworkType2G           = 1,
    dataNetworkType3G           = 2,
    dataNetworkType4G           = 3,
    dataNetworkTypeLTE          = 4,
    dataNetworkTypeWiFi         = 5
};

@interface BMDeviceResources : NSObject

+ (void) logDeviceResources;
+ (NSString*) deviceModel;
+ (NSArray*) deviceCapabilitiesWarnings;

+ (BOOL) isTouchIDAvailableWithError:(NSError**) error;
+ (BOOL) isMicrophoneAvailableWithError:(NSError**) error;
+ (BOOL) isLocationServicesAvailableWithError:(NSError**) error;

+ (BOOL) isCameraAvailableWithError: (NSError **) error;
+ (BOOL) isCameraFlashAvailable;
+ (BOOL) isCameraTorchAvailable;

+ (NSDictionary*) cameraResolutions;
+ (NSString*) captureSessionPresetByResolution: (NSString*) pResolution;

+ (dataNetworkType) dataNetworkType;
+ (NSString*) dataNetworkTypeTitle;
+ (NSString*) apnsToken;

@end
