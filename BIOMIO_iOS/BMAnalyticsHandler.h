//
//  BMAnalyticsHandler.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 1/10/17.
//  Copyright © 2017 BIOMIO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BMAnalyticsHandler : NSObject

+ (void)setup;

+ (void)logEvent:(NSString*) eventName properties:(NSDictionary*) customProperties;

@end
