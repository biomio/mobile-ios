//
//  BMSocketViewController.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 12/11/14.
//  Copyright (c) 2014 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
// handlers
#import "BMSocketHandler.h"
// view controllers
#import "BMHomeViewController.h"

@interface BMSocketViewController : BMHomeViewController

@property (weak, nonatomic) IBOutlet UITableView *messageTableView;
@property (strong, nonatomic) IBOutlet UIView *messageDetailsView;
@property (strong, nonatomic) IBOutlet UITextView *messageDetailsTextview;

@property (weak, nonatomic) IBOutlet UIButton *uploadLogsButton;

@end