//
//  GUTabBarView.m
//
//
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import "GUTabBarView.h"
#import "BMUserModel.h"
// handlers
#import "BMSocketHandler.h"
#import "BMDeviceResources.h"

@implementation GUTabBarView {
    UILabel *warningCountBadge;
}
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setTabBarView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.tabBarItems = [NSMutableArray new];
        [self setTabBarView];
    }
    return self;
}

- (void) updateTabBarView {
    [self setTabBarView];
}

- (void) setTabBarView {
    // first remove all existing views from the view
    for (UIButton* button in self.tabBarItems)
        [button removeFromSuperview];
    if (warningCountBadge)
        [warningCountBadge removeFromSuperview];
    [self.tabBarItems removeAllObjects];
    
    CGSize itemSize = CGSizeMake(self.frame.size.height*1.3539, self.frame.size.height-5);
    if ([BMUserModel isRegistrationRequired]) {
        self.titles = @[@"Auth", @"Settings", @"Help"];
        self.images = @[[self imageWithImage:[UIImage imageNamed: self.titles[0]] scaledToSize:itemSize],
                        [self imageWithImage:[UIImage imageNamed: self.titles[1]] scaledToSize:itemSize],
                        [self imageWithImage:[UIImage imageNamed: self.titles[2]] scaledToSize:itemSize]];
        
        self.imagesSelected = @[[self imageWithImage:[UIImage imageNamed: [NSString stringWithFormat:@"%@_selected", self.titles[0]]] scaledToSize:itemSize],
                                [self imageWithImage:[UIImage imageNamed: [NSString stringWithFormat:@"%@_selected", self.titles[1]]] scaledToSize:itemSize],
                                [self imageWithImage:[UIImage imageNamed: [NSString stringWithFormat:@"%@_selected", self.titles[2]]] scaledToSize:itemSize]];
        
    }
    else {
        self.titles = @[@"Auth", @"BiomioID", @"Settings", @"Help"];
        self.images = @[[self imageWithImage:[UIImage imageNamed: self.titles[0]] scaledToSize:itemSize],
                        [self imageWithImage:[UIImage imageNamed: self.titles[1]] scaledToSize:itemSize],
                        [self imageWithImage:[UIImage imageNamed: self.titles[2]] scaledToSize:itemSize],
                        [self imageWithImage:[UIImage imageNamed: self.titles[3]] scaledToSize:itemSize]];
        
        self.imagesSelected = @[[self imageWithImage:[UIImage imageNamed: [NSString stringWithFormat:@"%@_selected", self.titles[0]]] scaledToSize:itemSize],
                                [self imageWithImage:[UIImage imageNamed: [NSString stringWithFormat:@"%@_selected", self.titles[1]]] scaledToSize:itemSize],
                                [self imageWithImage:[UIImage imageNamed: [NSString stringWithFormat:@"%@_selected", self.titles[2]]] scaledToSize:itemSize],
                                [self imageWithImage:[UIImage imageNamed: [NSString stringWithFormat:@"%@_selected", self.titles[3]]] scaledToSize:itemSize]];
    }
    
    self.tabBarItems = [NSMutableArray new];
    //    self.selectedTabIndex = 0;
    self.backgroundColor = [UIColor colorWithRed:0.396 green:0.416 blue:0.498 alpha:1.00];
    
    for (NSInteger i = 0; i < [self.titles count]; i++) {
        UIButton *lTabBarItem = [UIButton buttonWithType:UIButtonTypeCustom];
        [lTabBarItem setFrame:CGRectMake(i * self.frame.size.width / [self.titles count], 0, self.frame.size.width / [self.titles count], self.frame.size.height)];
        [lTabBarItem addTarget:self action:@selector(tabBarItemPressed:) forControlEvents:UIControlEventTouchUpInside];
        [lTabBarItem setTag:i];
        [lTabBarItem setImage: i == self.selectedTabIndex ? self.imagesSelected[i] : self.images[i] forState:UIControlStateNormal];
        [lTabBarItem setTitle: self.titles[i] forState:UIControlStateNormal];
        lTabBarItem.titleLabel.font = [UIFont systemFontOfSize:12.0];
        
        [lTabBarItem setBackgroundColor:[UIColor clearColor]];
        [self centerButton:lTabBarItem];
        //        [self button:lTabBarItem centerVerticallyWithPadding:-15.0];
        [self addSubview:lTabBarItem];
        
        // add a badge with a number of warnings to the Settings icon
        NSUInteger warningCount = [self capabilityWarningsCount];
        if ([self.titles[i] isEqualToString:@"Settings"] && warningCount>0) {
            NSInteger sideLength = 18;
            warningCountBadge = [[UILabel alloc]initWithFrame:CGRectMake((lTabBarItem.frame.size.width/2),4, sideLength, sideLength)];
            warningCountBadge.textColor = [UIColor whiteColor];
            warningCountBadge.textAlignment = NSTextAlignmentCenter;
            warningCountBadge.text = @"!"; //[NSString stringWithFormat:@"%lu", (unsigned long)warningCount];
            warningCountBadge.layer.cornerRadius = sideLength/2;
            warningCountBadge.layer.masksToBounds = YES;
            warningCountBadge.layer.borderColor =[[UIColor clearColor] CGColor];
            warningCountBadge.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:63.0/255.0 blue:53.0/255.0 alpha:1.0];
            warningCountBadge.font = [UIFont systemFontOfSize:13];
            [lTabBarItem addSubview:warningCountBadge];
        }
        [self.tabBarItems addObject:lTabBarItem];
    }
}

- (NSUInteger) capabilityWarningsCount {
    NSUInteger warningCount = [[BMDeviceResources deviceCapabilitiesWarnings] count];
    BOOL settingsReviewed = [[NSUserDefaults standardUserDefaults] boolForKey:WARNING_REVIEWED];
    return settingsReviewed ? 0 : warningCount;
}

- (void) checkCapabilities {
    NSUInteger warningCount = [[BMDeviceResources deviceCapabilitiesWarnings] count];
    NSArray* warningCodes = [[NSUserDefaults standardUserDefaults] objectForKey:WARNING_CODES];
    if (warningCount != [warningCodes count])
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:WARNING_REVIEWED];
}

#pragma mark - Tab selection

- (void) manuallySelectTabBarItemAtIndex:(NSUInteger) pIndex {
    [self tabBarItemPressed:self.tabBarItems[pIndex]];
}

- (void) tabBarItemPressed: (id) sender {
    UIButton* buttonPressed = (UIButton*) sender;
    if (buttonPressed.tag != self.selectedTabIndex) {
        self.selectedTabIndex = buttonPressed.tag;
        for (UIButton* buttons in self.tabBarItems) {
            if (buttons.tag == buttonPressed.tag) {
                [self.tabBarItems[buttons.tag] setImage:self.imagesSelected[buttons.tag] forState:UIControlStateNormal];
            }
            else {
                [self.tabBarItems[buttons.tag] setImage:self.images[buttons.tag] forState:UIControlStateNormal];
            }
        }
        if ([self.delegate respondsToSelector:@selector(tabBarItemPressedAtIndex:)])
            [self.delegate tabBarItemPressedAtIndex:buttonPressed.tag];
    }
}

#pragma mark Centering button image/title

- (void) centerButton: (UIButton*) button {
    // the space between the image and text
    CGFloat spacing = -15.0;
    
    // lower the text and push it left so it appears centered
    //  below the image
    CGSize imageSize = button.imageView.image.size;
    button.titleEdgeInsets = UIEdgeInsetsMake(
                                              0.0, - imageSize.width, - (imageSize.height + spacing), 0.0);
    
    // raise the image and push it right so it appears centered
    //  above the text
    CGSize titleSize = [button.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: button.titleLabel.font}];
    button.imageEdgeInsets = UIEdgeInsetsMake(
                                              - (titleSize.height + spacing), 0.0, 0.0, - titleSize.width);
}

// issue on zoomed interfaces and <= 4" screens: center tab (settings) has decentered image
- (void)button: (UIButton*) button centerVerticallyWithPadding:(float)padding {
    
    CGSize imageSize = button.imageView.frame.size;
    CGSize titleSize = button.titleLabel.frame.size;
    
    CGFloat totalHeight = (imageSize.height + titleSize.height + padding);
    
    button.imageEdgeInsets = UIEdgeInsetsMake(- (totalHeight - imageSize.height),
                                              0.0f,
                                              0.0f,
                                              - titleSize.width);
    
    button.titleEdgeInsets = UIEdgeInsetsMake(0.0f,
                                              - imageSize.width,
                                              - (totalHeight - titleSize.height),
                                              0.0f);
}

#pragma mark Misc

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
