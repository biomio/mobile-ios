//
//  BMSettingsViewController.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 6/15/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
// view controllers
#import "BMHomeViewController.h"

@interface BMSettingsViewController : BMHomeViewController <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end
