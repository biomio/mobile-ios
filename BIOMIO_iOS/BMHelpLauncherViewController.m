//
//  BMHelpLauncherViewController.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 10/28/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

#import "BMHelpLauncherViewController.h"
#import "BMHelpViewController.h"
#import "BMTabViewController.h"
@interface BMHelpLauncherViewController ()

@end

@implementation BMHelpLauncherViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    if (!self.helpClose) {
        BMHelpViewController* helpVC = [[BMHelpViewController alloc]init];
        helpVC.delegate = self;
        [self presentViewController:helpVC animated:YES completion:nil];
    }
    else {
        self.helpClose = NO;
        BMTabViewController* tabbar = (BMTabViewController*) self.tabBarController;
        [tabbar.customTabBar manuallySelectTabBarItemAtIndex:0];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Popover

- (void)helpPopoverDidClose {
    
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    NSLog(@"DISMISSED");
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    NSLog(@"DISMISSED2");
    return YES;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
