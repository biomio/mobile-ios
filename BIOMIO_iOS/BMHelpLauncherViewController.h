//
//  BMHelpLauncherViewController.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 10/28/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BMHelpViewController.h"

@interface BMHelpLauncherViewController : UIViewController <UIPopoverPresentationControllerDelegate, HelpPopoverDelegate>
@property (nonatomic, assign) BOOL helpClose;
@end
