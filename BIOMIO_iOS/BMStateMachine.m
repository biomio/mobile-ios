//
//  BMStateMachine.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 5/15/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//


#import "BMStateMachine.h"
#import <TKState.h>
#import <TKEvent.h>

@implementation BMStateMachine

+ (instancetype) sharedHandler {
    static BMStateMachine *sharedHandler = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^
                  {
                      sharedHandler = [self new];
                  });
    return sharedHandler;
}

- (id)init {
    if (self = [super init]) {
        //////////////////// STATES ////////////////////
        
        self.disconnectedState              = [TKState stateWithName:@"disconnectedState"];
        self.connectionOpenState            = [TKState stateWithName:@"connectionOpenState"];
        self.connectionState                = [TKState stateWithName:@"connectionState"];
        self.connectionFailureState         = [TKState stateWithName:@"connectionFailureState"];
        
        self.regularHelloState              = [TKState stateWithName:@"regularHelloState"];
        self.registrationHelloState         = [TKState stateWithName:@"registrationHelloState"];
        
        self.regularHandshakeState          = [TKState stateWithName:@"regularHandshakeState"];
        self.registrationHandshakeState     = [TKState stateWithName:@"registrationHandshakeState"];
        
        self.standbyeState                  = [TKState stateWithName:@"standbyeState"];
        self.biometricAuthState             = [TKState stateWithName:@"biometricAuthState"];
        
        [self setInitialState:self.disconnectedState];
        
        NSArray* allStates = @[self.disconnectedState,
                               self.connectionOpenState,
                               self.connectionState,
                               self.connectionFailureState,
                               
                               self.regularHelloState,
                               self.registrationHelloState,
                               
                               self.regularHandshakeState,
                               self.registrationHandshakeState,
                               
                               self.standbyeState,
                               self.biometricAuthState];
        
        //////////////////// EVENTS ////////////////////
        
        self.connectionDidStart             = [TKEvent eventWithName:@"connectionDidStart" transitioningFromStates:@[self.disconnectedState] toState:self.connectionState];
        self.connectionDidFail              = [TKEvent eventWithName:@"connectionDidFail" transitioningFromStates:@[self.connectionState] toState:self.disconnectedState];
        self.connectionDidOpen              = [TKEvent eventWithName:@"connectionDidOpen" transitioningFromStates:@[self.connectionState] toState:self.connectionOpenState];
        self.registrationDidFail            = [TKEvent eventWithName:@"registrationDidFail" transitioningFromStates:@[self.disconnectedState] toState:self.connectionState];
        self.connectionNotAvailable         = [TKEvent eventWithName:@"connectionNotAvailable" transitioningFromStates:@[self.connectionState] toState:self.disconnectedState];
        
        ///////// HANDSHAKE
        // REGULAR
        self.connectionDidOpenWithUser      = [TKEvent eventWithName:@"connectionDidOpenWithUser" transitioningFromStates:@[self.connectionOpenState] toState:self.regularHelloState];
        self.regularServerHelloDidReceive   = [TKEvent eventWithName:@"regularServerHelloDidReceive" transitioningFromStates:@[self.regularHelloState] toState:self.regularHandshakeState];
        
        // REGISTRATION
        self.connectionDidOpenWithoutUser   = [TKEvent eventWithName:@"connectionDidOpenWithoutUser" transitioningFromStates:@[self.connectionOpenState] toState:self.registrationHelloState];
        self.registrationServerHelloDidReceive = [TKEvent eventWithName:@"registrationServerHelloDidReceive" transitioningFromStates:@[self.registrationHelloState] toState:self.registrationHandshakeState];
        
        self.handshakeMessageDidSend        = [TKEvent eventWithName:@"handshakeMessageDidSend" transitioningFromStates:@[self.registrationHandshakeState, self.regularHandshakeState] toState:self.standbyeState];
        
        self.tryMessageDidReceive           = [TKEvent eventWithName:@"tryMessageDidReceive" transitioningFromStates:@[self.standbyeState] toState:self.biometricAuthState];
        
        ///////// AUTHENTICATION
        self.biometricAuthDidSucceed        = [TKEvent eventWithName:@"biometricAuthDidSucceed" transitioningFromStates:@[self.biometricAuthState] toState:self.standbyeState];
        self.biometricAuthDidFail           = [TKEvent eventWithName:@"biometricAuthDidFail" transitioningFromStates:@[self.biometricAuthState] toState:self.standbyeState];
        self.biometricAuthCanceled          = [TKEvent eventWithName:@"biometricAuthCanceled" transitioningFromStates:@[self.biometricAuthState] toState:self.standbyeState];
        self.biometricAuthCanceledByServer  = [TKEvent eventWithName:@"biometricAuthCanceledByServer" transitioningFromStates:@[self.biometricAuthState] toState:self.standbyeState];
        self.biometricAuthDidExpire         = [TKEvent eventWithName:@"biometricAuthDidExpire" transitioningFromStates:@[self.biometricAuthState] toState:self.standbyeState];
        self.serverDidReceiveProbe          = [TKEvent eventWithName:@"serverDidReceiveProbe" transitioningFromStates:@[self.biometricAuthState] toState:self.standbyeState];
        
        ///////// CLOSING CONNECTION
        self.byeMessageDidReceive           = [TKEvent eventWithName:@"byeMessageDidReceive" transitioningFromStates:allStates toState:self.disconnectedState];
        self.socketWillClose                = [TKEvent eventWithName:@"socketWillClose" transitioningFromStates:allStates toState:self.disconnectedState];
        [self activate];
    }
    return self;
}

@end
