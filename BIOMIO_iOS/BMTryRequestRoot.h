//
//  BMTryRequestRoot.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 5/18/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import <Foundation/Foundation.h>
@class BMTryRequestRoot, BMTryAuthMethod;

typedef NS_ENUM (NSInteger, TryCondition) {
    TryConditionAll           = 0,
    TryConditionAny           = 1
};

typedef NS_ENUM(NSInteger, TryRequestStatus) {
    TryRequestStatusClean           = 0,
    TryRequestStatusActive          = 1,
    TryRequestStatusSent            = 2,
    TryRequestStatusCompleted       = 3,
    TryRequestStatusCanceled        = 4,
    TryRequestStatusExpired         = 5
};

@protocol TryRequestDelegate <NSObject>
- (void) tryRequestDidExpire:(BMTryRequestRoot*) request;
@end

@interface BMTryRequestRoot : NSObject

@property (weak) id <TryRequestDelegate> delegate;
#pragma mark - Major

/**
 Type (Authentication/Training)
 */
@property (nonatomic, strong) NSArray* messages;
/**
 Try execution scheme: all/any
 */
@property (nonatomic, readonly) TryCondition condition;

/**
 Unique alphanumerical ID of the Try request
 */
@property (nonatomic, strong, readonly) NSString* identifier;
@property (nonatomic, assign) TryRequestStatus status;

// provider info
#pragma mark - Provider
@property (nonatomic, strong, readonly) NSString* providerTitle;
@property (nonatomic, strong, readonly) NSString* providerLogoURL;

#pragma mark - Timeout
@property (nonatomic, readonly) NSInteger timeout;
@property (nonatomic, readonly) NSTimer* authTimeoutTimer;      // Expiration timer for the try request. Calls delegate tryRequestDidExpire: when fired

#pragma mark - AuthMethods
@property (nonatomic, strong) NSArray* authMethods;
@property (nonatomic, strong, readonly) NSArray* interactiveAuthMethods;
@property (nonatomic, strong, readonly) NSArray* nonInteractiveAuthMethods;

@property (nonatomic, strong) NSArray* selectedAuthMethod;

#pragma mark - Methods
+ (instancetype) tryRequestWithID:(NSString*) identifier
                        condition:(TryCondition) condition
                         messages:(NSArray*) messages
                          timeout:(NSInteger) timeout
                    providerTitle:(NSString*) providerTitle
                  providerLogoURL:(NSString*) providerLogoUrl
                         delegate:(id<TryRequestDelegate>) delegateObj;

- (void) addAuthMethods:(NSArray*) authMethods;

#pragma mark Messages
/**
 Auth/Training
 */
- (NSString*) tryRequestType;

/**
 Try request description from the provider
 @return Introduction sentence
 */
- (NSString*) tryDescription;
- (NSString*) startButtonTitle;

#pragma mark Status
- (BOOL) completed;
- (BOOL) recoverToClean;
- (void) cancel;

#pragma mark AuthSamples
- (NSInteger) authMethodsRequested;
- (NSInteger) authMethodsCompleted;

- (void) addAuthResultToActiveInteractiveAuthMethod: (id) pResult;
- (void) addAuthResult:(NSString*) pResult toAuthMethod:(BMTryAuthMethod*) pAuthMethod;

- (BMTryAuthMethod*) authMethodWithTitle:(NSString*) title;
@end
