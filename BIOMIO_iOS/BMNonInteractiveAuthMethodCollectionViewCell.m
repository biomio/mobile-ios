//
//  BMNonInteractiveAuthMethodCollectionViewCell.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 12/9/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

#import "BMNonInteractiveAuthMethodCollectionViewCell.h"
#import "BMHomeViewController.h"

@implementation BMNonInteractiveAuthMethodCollectionViewCell {
    BOOL animating;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void) setCompleted:(BOOL)completed {
    _completed = completed;
    if (completed) {
        [self stopSpin];
        self.authMethodImageView.tintColor = COLOR_GREEN;
        self.authMethodImageView.image = [UIImage imageNamed:self.authMethodTitle];
    }
    else {
        self.authMethodImageView.image = [UIImage imageNamed:@"noninteractive_in_progress"];
        [self startSpin];
        
        self.backgroundColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
    }
}

- (void) spinWithOptions: (UIViewAnimationOptions) options {
    // this spin completes 360 degrees every 1 second
    [UIView animateWithDuration: 0.25f
                          delay: 0.0f
                        options: options
                     animations: ^{
                         self.authMethodImageView.transform = CGAffineTransformRotate(self.authMethodImageView.transform, -M_PI_2);
                     }
                     completion: ^(BOOL finished) {
                         if (finished) {
                             if (animating) {
                                 // if flag still set, keep spinning with constant speed
                                 [self spinWithOptions: UIViewAnimationOptionCurveLinear];
                             } else if (options != UIViewAnimationOptionCurveEaseOut) {
                                 // one last spin to original; set default image
                                 self.authMethodImageView.transform = CGAffineTransformIdentity;
                                 self.authMethodImageView.image = [UIImage imageNamed:self.authMethodTitle];
                             }
                         }
                     }];
}

- (void) startSpin {
    if (!animating) {
        animating = YES;
        [self spinWithOptions: UIViewAnimationOptionCurveEaseIn];
    }
}

- (void) stopSpin {
    // set the flag to stop spinning after one last 90 degree increment
    animating = NO;
    self.authMethodImageView.transform = CGAffineTransformIdentity;
}

@end
