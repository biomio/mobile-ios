//
//  RSProgressBar.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 8/28/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, RSProgressBarState) {
    RSProgressBarStateStatic        = 0,
    RSProgressBarStateRefreshing    = 1,
    RSProgressBarStateLoading       = 2
};

@interface RSProgressBar : UIView

@property (nonatomic, strong) UIColor *barColor;
@property (nonatomic, strong) UIColor *refreshIndicatorColor;
@property (nonatomic, strong) UIColor *incompleteBarColor;
@property (nonatomic, assign) RSProgressBarState progressBarState;

- (void)setProgressBarProgress:(CGFloat)progress animated: (BOOL) pAnimated;
- (void)setProgressBarProgress:(CGFloat)progress animated: (BOOL) pAnimated completed:(void(^)(BOOL completed)) completion;
@end
