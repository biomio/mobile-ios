//
//  BMProbeResourcesCollectionViewCell.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 03.09.15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import "BMProbeResourcesCollectionViewCell.h"

#define BORDER_INACTIVE_COLOR       [UIColor colorWithWhite:0.592 alpha:1.000]
#define BORDER_ACTIVE_COLOR         [UIColor colorWithRed:0.071 green:0.557 blue:0.255 alpha:1.000]

#define BORDER_INACTIVE_WIDTH       1;
#define BORDER_ACTIVE_WIDTH         2;


@implementation BMProbeResourcesCollectionViewCell

@synthesize resourceImageView, checkmarkImageView;
- (void)awakeFromNib {
    [super awakeFromNib];
    self.status = AuthMethodStatusClean;
    self.layer.cornerRadius = 50;
    self.clipsToBounds = YES;
    self.layer.borderColor = [BORDER_INACTIVE_COLOR CGColor];
    self.layer.borderWidth = BORDER_INACTIVE_WIDTH;
}

- (void) setDefaultImageTitle:(NSString *)defaultImageTitle {
    _defaultImageTitle = defaultImageTitle;
    resourceImageView.image = [UIImage imageNamed: self.defaultImageTitle];
}

- (void) setActive:(BOOL)active {
    _active = active;
    if (active) {
        self.layer.borderColor = [COLOR_GREEN CGColor];
        self.layer.borderWidth = BORDER_ACTIVE_WIDTH;
    }
    else {
        self.layer.borderColor = [BORDER_INACTIVE_COLOR CGColor];
        self.layer.borderWidth = BORDER_INACTIVE_WIDTH;
    }
}

- (void)setStatus:(AuthMethodStatus)status {
    _status = status;
    switch (status) {
        case AuthMethodStatusClean: {
            checkmarkImageView.hidden = YES;
            break;
        }
        case AuthMethodStatusCompleted: {
            checkmarkImageView.image = [UIImage imageNamed:@"checkmark"];
            checkmarkImageView.hidden = NO;
            break;
        }
        case AuthMethodStatusFailed: {
            checkmarkImageView.image = [UIImage imageNamed:@"failed"];
            checkmarkImageView.hidden = NO;
            break;
        }
        default:
            checkmarkImageView.hidden = YES;
            break;
    }
}

- (void) setCompleted:(BOOL)completed {
    if (completed) {
        checkmarkImageView.hidden = NO;
    }
    else {
        checkmarkImageView.hidden = YES;
    }
}

@end
