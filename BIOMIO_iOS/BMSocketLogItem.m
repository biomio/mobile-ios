//
//  BMSocketLogItem.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 10/2/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

#import "BMSocketLogItem.h"

#define LOG_ITEM_TIMESTAMP      @"LOG_ITEM_TIMESTAMP"
#define LOG_ITEM_MESSAGE        @"LOG_ITEM_MESSAGE"
#define LOG_ITEM_OID            @"LOG_ITEM_OID"
#define LOG_ITEM_TYPE           @"LOG_ITEM_TYPE"

@implementation BMSocketLogItem

+ (instancetype) logItemWithDictionary:(NSDictionary*) pDict type:(logItemType) pType {
    BMSocketLogItem* logItem = [BMSocketLogItem new];
    logItem.timestamp = [[NSDate date] timeIntervalSince1970];
    logItem.type = pType;
    if (pType == logItemTypeError)
        logItem.oid = @"error";
    else {
        if (pDict[@"msg"])
            if (pDict[@"msg"][@"oid"])
                logItem.oid = pDict[@"msg"][@"oid"];
    }
    // remove auth result (only photos)
    if ([logItem.oid isEqualToString:@"probe"])
        logItem.message = [BMSocketLogItem removeAuthSamplesFromMessage:pDict];
    else
        logItem.message = pDict;
    return logItem;
}

+ (NSDictionary*) removeAuthSamplesFromMessage:(NSDictionary*) pDict {
    NSMutableDictionary* cleanMessage = [pDict mutableCopy];
    if (pDict[@"msg"][@"probeData"][@"samples"]) {
        if ([pDict[@"msg"][@"probeData"][@"oid"] isEqualToString:@"imageSamples"]) {
            NSArray* samples = pDict[@"msg"][@"probeData"][@"samples"];
            NSMutableArray* cleanSamples = [NSMutableArray new];
            for (NSInteger i=0; i<[samples count];i++)
                [cleanSamples addObject:@"auth_result"];
            cleanMessage[@"msg"][@"probeData"][@"samples"] = [cleanSamples copy];
        }
    }
    return cleanMessage;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeDouble:self.timestamp forKey:LOG_ITEM_TIMESTAMP];
    [encoder encodeObject:self.message forKey:LOG_ITEM_MESSAGE];
    [encoder encodeObject:self.oid forKey:LOG_ITEM_OID];
    [encoder encodeInteger:self.type forKey:LOG_ITEM_TYPE];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.timestamp = [decoder decodeDoubleForKey:LOG_ITEM_TIMESTAMP];
        self.message = [decoder decodeObjectForKey:LOG_ITEM_MESSAGE];
        self.oid = [decoder decodeObjectForKey:LOG_ITEM_OID];
        self.type = (logItemType) [decoder decodeIntegerForKey:LOG_ITEM_TYPE];}
    return self;
}



@end
