//
//  BMSocketViewController.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 12/11/14.
//  Copyright (c) 2014 BIOMIO. All rights reserved.
//

#import "BMSocketViewController.h"
// handlers
#import "BMSocketLogger.h"

@interface BMSocketViewController () <BMSocketLoggerDelegate>

@end

@implementation BMSocketViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [BMSocketLogger defaultLogger].delegate = self;
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (void) viewDidAppear:(BOOL)animated {
    [self.messageTableView reloadData];
}

#pragma mark - Superclass methods

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - SocketLogger delegates

- (void)socketLoggerDidUpdateLogs {
    [self.messageTableView reloadData];
}

#pragma mark - TableView delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [[[BMSocketLogger defaultLogger] allLogs] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"TableCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:identifier];
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd.MM.yyyy HH:mm:ss";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    BMSocketLogItem* lMesLog = [[BMSocketLogger defaultLogger] allLogs][[[[BMSocketLogger defaultLogger] allLogs] count]-1-indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.text = [NSString stringWithFormat:@"%@\tOID:%@", [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:lMesLog.timestamp]],lMesLog.oid];
    cell.detailTextLabel.numberOfLines = 0;
    switch (lMesLog.type) {
        case logItemTypeMessageReceived:
            cell.backgroundColor = [UIColor colorWithRed:1.000 green:0.543 blue:0.277 alpha:1.000];
            break;
        case logItemTypeMessageSent:
            cell.backgroundColor = [UIColor colorWithRed:0.133 green:0.576 blue:0.129 alpha:1.000];
            break;
        case logItemTypeError:
            cell.backgroundColor = [UIColor redColor];
            break;
        default:
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BMSocketLogItem* lMesLog = [[BMSocketLogger defaultLogger] allLogs][[[[BMSocketLogger defaultLogger] allLogs] count]-1-indexPath.row];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd.MM.yyyy             HH:mm:ss";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    self.messageDetailsTextview.text = [NSString stringWithFormat:@"%@\n%@\n%@", [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:lMesLog.timestamp]], (lMesLog.type == logItemTypeMessageReceived) ? @"\tRECEIVED" : (lMesLog.type == logItemTypeError) ? @"ERROR" : @"\tSENT",lMesLog.message];
    
    self.messageDetailsView.hidden = NO;
}

#pragma mark - Buttons

- (IBAction)closeDetailsButtonPressed:(id)sender {
    self.messageDetailsView.hidden = YES;
}

- (IBAction)backButtonPressed:(id)sender {
    [[self navigationController]popViewControllerAnimated:YES];
}

- (IBAction)uploadLogsButtonPressed:(id)sender {
    [[BMSocketLogger defaultLogger]uploadLogsWithCompletionHandler:^(NSHTTPURLResponse *response, NSError *error) {
        self.uploadLogsButton.enabled = YES;
    }];
    self.uploadLogsButton.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(30 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.uploadLogsButton.enabled = YES;
    });
}

@end
