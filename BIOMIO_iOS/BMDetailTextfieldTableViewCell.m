//
//  BMDetailTextfieldTableViewCell.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 10/4/16.
//  Copyright © 2016 BIOMIO. All rights reserved.
//

#import "BMDetailTextfieldTableViewCell.h"


@implementation BMDetailTextfieldTableViewCell
- (void)awakeFromNib {
    [super awakeFromNib];
    self.detailTextfield.delegate = self;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([self.delegate respondsToSelector:@selector(textfieldCell:didFinishEditingTextfield:)])
        [self.delegate textfieldCell:self didFinishEditingTextfield:self.detailTextfield];
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([self.delegate respondsToSelector:@selector(textfieldCell:didFinishEditingTextfield:)])
        [self.delegate textfieldCell:self didFinishEditingTextfield:self.detailTextfield];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newTextValue = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    
    if ([self.delegate respondsToSelector:@selector(textfieldCell:textfield:didChangeTextTo:)])
        [self.delegate textfieldCell:self textfield:textField didChangeTextTo:newTextValue];
    return YES;
}

@end
