//
//  BMDeviceResourceWarningTableViewCell.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 10/4/16.
//  Copyright © 2016 BIOMIO. All rights reserved.
//

#import "BMDeviceResourceWarningTableViewCell.h"

@implementation BMDeviceResourceWarningTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.stackView.alignment = OAStackViewAlignmentCenter;
    self.stackView.distribution = OAStackViewDistributionFillProportionally;
    self.stackView.spacing = 15;
    
    self.titleLabel.textColor = COLOR_RED;
}

- (void) initWithAuthMethods:(NSArray*) methods {
    for (UIView* view in self.stackView.arrangedSubviews)
        [self.stackView removeArrangedSubview:view];
    for (NSString* methodName in methods) {
        UIImageView* imageResource = [[UIImageView alloc] initWithImage:[UIImage imageNamed:methodName]];
        [imageResource addConstraint:[NSLayoutConstraint constraintWithItem:imageResource
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:imageResource
                                                                  attribute:NSLayoutAttributeWidth
                                                                 multiplier:1.0
                                                                   constant:0.0f]];
        [self.stackView addArrangedSubview:imageResource];
    }
}

- (IBAction)openSettingsButtonPressed:(id)sender {
    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:url];
    // better move to BMSettingsViewController
    // the badge must disappear when the user taps "open Settings"
    // note that when user changes any of the permissions and the number of warnings changes, the badge will be displayed
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:WARNING_REVIEWED];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
