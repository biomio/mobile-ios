//
//  GUTabBarView.h
//
//
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol GUTabBarDelegate <NSObject>
@optional
- (void) tabBarItemPressedAtIndex:(NSInteger)pTag;
@end

@interface GUTabBarView : UIView

@property (nonatomic, strong) NSArray* images;
@property (nonatomic, strong) NSArray* imagesSelected;
@property (nonatomic, strong) NSArray* titles;
@property (nonatomic, assign) NSUInteger numberOfTabs;
@property (nonatomic, assign) NSUInteger selectedTabIndex;
@property (nonatomic, strong) NSMutableArray* tabBarItems;
@property (unsafe_unretained, nonatomic) id  <GUTabBarDelegate> delegate;

- (void) manuallySelectTabBarItemAtIndex:(NSUInteger) pIndex;
- (void) setTabBarView;

@end
