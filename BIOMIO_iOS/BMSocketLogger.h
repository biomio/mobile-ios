//
//  BMSocketLogger.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 10/1/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

#import <Foundation/Foundation.h>
// models
#import "BMSocketLogItem.h"
@class BMSocketLogger;

#define SOCKET_LOGGER       [BMSocketLogger defaultLogger]

@protocol BMSocketLoggerDelegate <NSObject>

/**
 *  Called on every modification of the temporary or persistent log storage.
 */
- (void) socketLoggerDidUpdateLogs;

@optional
/**а
 *  Called after a successfull write to persistent storage.
 */
- (void) socketLoggerDidWriteToStorage;

/**
 *  Called after a successful manual clean of the log storage.
 */
- (void) socketLoggerDidCleanLogs;

/**
 *  Called after a successful manual clean of the log buffer.
 */
- (void) socketLoggerDidCleanBuffer;

/**
 *  Called when the Logger has successfully removed a part of logs in the storage.
 */
- (void) socketLoggerDidRemoveOldLogs;

/**
 *  Called when NSURLSessionUploadTask returns the completionHandler.
 *
 *  @param response NSHTTPURLResponse: check statusCode
 *  @param error    NSError
 */
- (void) socketLoggerDidUploadLogsWithResponse: (NSHTTPURLResponse*) response error: (NSError**) error;
@end

@interface BMSocketLogger : NSObject <NSURLSessionTaskDelegate, NSURLSessionDataDelegate, NSURLSessionDelegate, NSURLSessionDelegate>

/**
 *  Returns the default singleton instance.
 */
+ (instancetype) defaultLogger;

/**
 *  Defines whether to save new logs to persistent storage. Default = YES;
 */
@property (nonatomic, assign) BOOL writeToStorage;
@property id <BMSocketLoggerDelegate> delegate;

#pragma mark - Add logs
/**
 *  Add a new log of message type. Buffer and storage management are automatically handled.
 *
 *  @param pMessage Message of NSString or NSDictionary type.
 */
- (void) logMessage: (id) pLog;

/**
 *  Add a new log of error type. Buffer and storage management are automatically handled.
 *
 *  @param pMessage Error of NSError type.
 */
- (void) logError: (NSError *)pError;

#pragma mark - Get logs
- (NSArray*) allLogs;
- (void) uploadLogsWithCompletionHandler: (void(^)(NSHTTPURLResponse* response, NSError* error)) completion;

#pragma mark - Storage management
- (BOOL) persistLogsToStorage;
- (BOOL) cleanLogs;
@end
