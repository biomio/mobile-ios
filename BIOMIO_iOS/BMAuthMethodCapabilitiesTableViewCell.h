//
//  BMAuthMethodCapabilitiesTableViewCell.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 10/4/16.
//  Copyright © 2016 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
// frameworks
#import "OAStackView.h"

@interface BMAuthMethodCapabilitiesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet OAStackView *stackView;

- (void) initWithAuthMethods:(NSArray*) methods disabled:(NSArray*) disabledMethods;

@end
