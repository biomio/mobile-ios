//
//  AppDelegate.m
//  BIOMIO_iOS
//
//  Created by Roman Andruseiko on 10/8/14.
//  Copyright (c) 2014 BIOMIO. All rights reserved.
//

#import "AppDelegate.h"

// handlers
#import "BMSocketHandler.h"
#import "BMStateMachine.h"
#import "BMSocketLogger.h"
#import <SAMKeychain/SAMKeychain.h>
#import "BMErrorHandler.h"
#import "BMDeviceResources.h"
#import "BMAnalyticsHandler.h"
// models
#import "BMUserModel.h"
// view controllers
#import "BMTabViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self setup3DTouchShortcuts];

    // analytics
    [BMAnalyticsHandler setup];
    [Answers logCustomEventWithName:@"AppLaunch" customAttributes:nil];
    //    [[NSUserDefaults standardUserDefaults] setValue:@(NO) forKey:@"_UIConstraintBasedLayoutLogUnsatisfiable"];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [SAMKeychain setAccessibilityType:kSecAttrAccessibleWhenUnlocked];
    [BMSocketHandler sharedSocket];
    
    BMTabViewController* tabBarController = [[BMTabViewController alloc] init];
    self.window.rootViewController = tabBarController;
    [self.window makeKeyAndVisible];
    
    [self.window setTintColor:[UIColor colorWithRed:0.000 green:0.488 blue:0.988 alpha:1.000]];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    // push notifications
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil]];
        [application registerForRemoteNotifications];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    }
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    if (![[BMSocketHandler sharedSocket] isBusyUploadingProbe]) {
        NSError* error = nil;
        [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].socketWillClose userInfo:nil error:&error];
    }
    [[BMSocketLogger defaultLogger] persistLogsToStorage];
    [self setup3DTouchShortcuts];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if (![BMUserModel isRegistrationRequired]) {
        if ([[BMStateMachine sharedHandler].currentState isEqual:[BMStateMachine sharedHandler].disconnectedState]) {
            NSError* error;
            [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidStart userInfo:nil error:&error];
        }
    }
    [self updateSettingsWarningCount];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"BIOMIO_registration_secret"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[BMSocketLogger defaultLogger] persistLogsToStorage];
}

#pragma mark - 3D Touch Shortcuts

- (void) setup3DTouchShortcuts {
    // if 3D Touch supported
    if(self.window.traitCollection.forceTouchCapability == UIForceTouchCapabilityAvailable){
        UIApplicationShortcutItem* registerItem = [[UIApplicationShortcutItem alloc] initWithType:@"com.biomio.app.3dtouch.register"
                                                                                   localizedTitle:@"Register"
                                                                                localizedSubtitle:nil
                                                                                             icon:[UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeAdd]
                                                                                         userInfo:nil];
        UIApplicationShortcutItem* idItem = [[UIApplicationShortcutItem alloc] initWithType:@"com.biomio.app.3dtouch.id"
                                                                             localizedTitle:@"BiomioID"
                                                                          localizedSubtitle:nil
                                                                                       icon:[UIApplicationShortcutIcon iconWithTemplateImageName:@"3dtouch_biomioId"]
                                                                                   userInfo:nil];
        NSArray* shortcuts = [BMUserModel isRegistrationRequired] ? @[registerItem] : @[idItem];
        [UIApplication sharedApplication].shortcutItems = shortcuts;
    }
}

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler {
    BMTabViewController* tabbar = (BMTabViewController*) self.window.rootViewController;
    if ([shortcutItem.type isEqualToString:@"com.biomio.app.3dtouch.id"]) {
        [tabbar.customTabBar manuallySelectTabBarItemAtIndex:1];
    }
    else if ([shortcutItem.type isEqualToString:@"com.biomio.app.3dtouch.auth"]) {
        [tabbar.customTabBar manuallySelectTabBarItemAtIndex:0];
    }
    else if ([shortcutItem.type isEqualToString:@"com.biomio.app.3dtouch.register"]) {
        [tabbar.customTabBar manuallySelectTabBarItemAtIndex:0];
    }
}

#pragma mark - URL schemes

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    NSString *urlString = [url absoluteString];
    if([urlString hasPrefix:@"biomio://"]) {
        NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:url
                                                    resolvingAgainstBaseURL:NO];
        NSArray *queryItems = urlComponents.queryItems;
        
        if ([url.host isEqualToString:@"registration"]) {
            NSString* code = [self valueForKey:@"code" fromQueryItems:queryItems];
            [[NSUserDefaults standardUserDefaults] setValue:code forKey:@"BIOMIO_registration_secret_URL"];
        } else if ([url.host isEqualToString:@"biomioid"]) {
            NSString* postRequestUrl = [self valueForKey:@"url" fromQueryItems:queryItems];
            NSLog(@"BiomioID POST URL: %@", postRequestUrl);
            if (postRequestUrl && ![postRequestUrl isEqualToString:@""]) {
                NSURL* requestUrl = [NSURL URLWithString:postRequestUrl];
                [SOCKET_HANDLER sendIdentificationRequestToURL:requestUrl
                                               completionBlock:nil];
            }
        }
        return YES;
    }
    return NO;
}

- (NSString *)valueForKey:(NSString *)key
           fromQueryItems:(NSArray *)queryItems {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name=%@", key];
    NSURLQueryItem *queryItem = [[queryItems filteredArrayUsingPredicate:predicate] firstObject];
    return queryItem.value;
}

#pragma mark - Device capabilities

- (void) updateSettingsWarningCount {
    NSArray* currentWarnings = [BMDeviceResources deviceCapabilitiesWarnings];
    NSArray* latestWarnings = [[NSUserDefaults standardUserDefaults] objectForKey:WARNING_CODES];
    
    NSMutableArray* logWarnings = [NSMutableArray new];
    // save new capabilities check
    for (NSDictionary* warning in currentWarnings) {
        NSError* error = warning[@"error"];
        [logWarnings addObject:@(error.code)];
    }
    [[NSUserDefaults standardUserDefaults] setObject:logWarnings forKey:WARNING_CODES];
    
    if (![latestWarnings isEqualToArray:logWarnings])
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:WARNING_REVIEWED];
    
    BMTabViewController* tabbar = (BMTabViewController*) self.window.rootViewController;
    [tabbar refreshTabBar];
}

#pragma mark - Notifications

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"Did Register for Remote Notifications with APNS Token (%@)", deviceToken);
    
    const unsigned *tokenBytes = (const unsigned *)[deviceToken bytes];
    NSString *stringToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                             ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                             ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                             ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
    [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:APNS_TOKEN_NSDATA];
    [[NSUserDefaults standardUserDefaults] setObject:stringToken forKey:APNS_TOKEN_NSSTRING];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Did Fail to Register for Remote Notifications");
    NSLog(@"%@, %@", error, error.localizedDescription);
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:APNS_TOKEN_NSDATA];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:APNS_TOKEN_NSSTRING];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
