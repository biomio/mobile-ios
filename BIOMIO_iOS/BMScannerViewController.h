//
//  BMScannerViewController.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 7/7/16.
//  Copyright © 2016 BIOMIO. All rights reserved.
//

#import "BMHomeViewController.h"
// frameworks
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface BMScannerViewController : BMHomeViewController <AVCaptureMetadataOutputObjectsDelegate>
@property (weak, nonatomic) IBOutlet UIView *viewPreview;
@property (nonatomic, strong) RSProgressBar* registrationProgressBar;
@end
