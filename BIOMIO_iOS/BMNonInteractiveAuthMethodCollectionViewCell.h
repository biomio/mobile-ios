//
//  BMNonInteractiveAuthMethodCollectionViewCell.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 12/9/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BMNonInteractiveAuthMethodCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *authMethodTitleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *authMethodImageView;
@property (nonatomic, assign) BOOL completed;
@property (nonatomic, strong) NSString* authMethodTitle;
@end
