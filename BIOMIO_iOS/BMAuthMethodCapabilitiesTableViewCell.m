//
//  BMAuthMethodCapabilitiesTableViewCell.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 10/4/16.
//  Copyright © 2016 BIOMIO. All rights reserved.
//

#import "BMAuthMethodCapabilitiesTableViewCell.h"

@implementation BMAuthMethodCapabilitiesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.stackView.distribution = OAStackViewDistributionFillEqually;
    self.stackView.spacing = 4;
}

- (void) initWithAuthMethods:(NSArray*) methods disabled:(NSArray*) disabledMethods {
    for (UIView* view in self.stackView.arrangedSubviews)
        [self.stackView removeArrangedSubview:view];
    
    for (NSString* methodName in methods) {
        UIImageView* imageResource = [[UIImageView alloc] initWithImage:[UIImage imageNamed:methodName]];
        imageResource.alpha =  [disabledMethods containsObject:methodName] ? 0.4 : 1.0;
        [imageResource addConstraint:[NSLayoutConstraint constraintWithItem:imageResource
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:imageResource
                                                                  attribute:NSLayoutAttributeWidth
                                                                 multiplier:1.0
                                                                   constant:0.0f]];
        [self.stackView addArrangedSubview:imageResource];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
