//
//  BMTryAuthMethod.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 5/18/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import "BMTryAuthMethod.h"

@interface BMTryAuthMethod ()
@property (nonatomic, strong, readwrite) NSString* title;
@property (nonatomic, readwrite) BOOL userInteractive;
@property (nonatomic, readwrite) NSInteger index;
@property (nonatomic, strong, readwrite) NSString* parrentTryID;

@property (nonatomic, strong, readwrite) NSString* deviceTitle;
@property (nonatomic, strong, readwrite) NSString* deviceSettings;

@property (nonatomic, readwrite) NSUInteger samplesRequested;
@end

@implementation BMTryAuthMethod
@synthesize status = _status;

- (id) init {
    if (self = [super init]) {
        self.authSamples = [NSMutableArray new];
    }
    return self;
}

+ (instancetype) authMethodWithTitle:(NSString*) title
                             samples:(NSUInteger) samplesRequested
                     userInteractive:(BOOL) userInteractive
                             atIndex:(NSInteger) idx
                              device:(NSString*) deviceTitle
                      deviceSettings:(NSString*) deviceSettings
                   childOfTryRequest:(NSString*) parrentID {
    BMTryAuthMethod* authMethod = [BMTryAuthMethod new];
    
    authMethod.title = title;
    authMethod.samplesRequested = samplesRequested;
    authMethod.userInteractive = userInteractive;
    authMethod.index = idx;
    authMethod.deviceTitle = deviceTitle;
    authMethod.deviceSettings = deviceSettings;
    
    authMethod.parrentTryID = parrentID;
    
    return authMethod;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"AuthMethod #%li %@\t%@ \tDevice: %@/%@ \tSamplesCompleted: %li/%li \tStatus: %li \tTryID: %@",
            (long)self.index,
            self.title,
            self.userInteractive ? @"UserInteractive" : @"UserNonInteractive",
            self.deviceTitle, self.deviceSettings,
            (long)self.samplesCompleted, (long)self.samplesRequested,
            (long)self.status,
            self.parrentTryID];
}


#pragma mark - Status

- (AuthMethodStatus)status {
    if (self.samplesCompleted == self.samplesRequested && _status != AuthMethodStatusCompleted)
        self.status = AuthMethodStatusSent;
    return _status;
}

// TODO: check if setting if allowed
- (void) setStatus:(AuthMethodStatus) status {
    _status = status;
}

- (void) setCompleted {
    self.status = AuthMethodStatusCompleted;
}

- (void) didFailWithError:(NSError*) error {
    self.status = AuthMethodStatusFailed;
}

- (BOOL)recoverToClean {
    if (self.status == AuthMethodStatusActive || self.status == AuthMethodStatusSkipped) {
        self.status = AuthMethodStatusClean;
        self.authSamples = [NSMutableArray new];
        return YES;
    }
    return NO;
}

#pragma mark - Samples

- (NSUInteger) samplesCompleted {
    return [self.authSamples count];
}

- (void) addAuthSample: (id) pResult {
    [self.authSamples addObject:pResult];
    [self status];
}

@end
