//
//  BMHelpViewController.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 10/27/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

#import "OnboardingViewController.h"

@protocol HelpPopoverDelegate <NSObject>

- (void) helpPopoverDidClose;

@end

@interface BMHelpViewController : OnboardingViewController
@property id <HelpPopoverDelegate> delegate;
@property (nonatomic, assign) BOOL firstLaunchType;

@end
