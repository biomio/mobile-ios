//
//  BMAnyProbeViewController.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 04.09.15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import "BMHomeViewController.h"

@class BMTryRequestRoot;
@interface BMAnyProbeViewController : BMHomeViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UINavigationItem *navBarTitle;
@property (weak, nonatomic) IBOutlet UICollectionView *resourcesCollectionView;
@property (nonatomic, strong) BMTryRequestRoot* tryRequest;
@property (strong, nonatomic) IBOutlet UIImageView *providerLogoImageView;
@property (strong, nonatomic) IBOutlet UILabel *providerTitleLabel;
@property (strong, nonatomic) IBOutlet UIButton *startChooseButton;

@end
