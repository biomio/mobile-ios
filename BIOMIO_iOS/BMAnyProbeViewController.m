
//
//  BMAnyProbeViewController.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 04.09.15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

// view controllers
#import "BMAnyProbeViewController.h"
// cells
#import "BMProbeResourcesCollectionViewCell.h"
// handlers
#import "BMStateMachine.h"
// models
#import "BMTryRequestRoot.h"
#import "BMTryAuthMethod.h"

@interface BMAnyProbeViewController ()

@end

@implementation BMAnyProbeViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.resourcesCollectionView registerNib:[UINib nibWithNibName:@"BMProbeResourcesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    
    // Provider logo/title
    if (self.tryRequest.providerLogoURL) {
        [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.tryRequest.providerLogoURL]] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            self.providerLogoImageView.image = [UIImage imageWithData:data];
        }];
    }
    else
        self.providerLogoImageView.image = [UIImage imageNamed: @"biomio_logo_new"];
    
    self.providerTitleLabel.text = [NSString stringWithFormat:@"%@\n%@",
                                    self.tryRequest.providerTitle ? self.tryRequest.providerTitle : @"BIOMIO",
                                    [self.tryRequest tryDescription] ? [self.tryRequest tryDescription] : @""];
    
    // startchooseButton (all/any)
    [self.startChooseButton setTitle:[self.tryRequest startButtonTitle] forState:UIControlStateNormal];
    self.startChooseButton.enabled = !(self.tryRequest.condition == TryConditionAny);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ResourcesCollectionView delegate methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.tryRequest.interactiveAuthMethods count];
}

- (BMProbeResourcesCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Cell";
    BMProbeResourcesCollectionViewCell* cell = [self.resourcesCollectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    BMTryAuthMethod* lResource = [self.tryRequest.interactiveAuthMethods objectAtIndex:indexPath.item];
    cell.defaultImageTitle = lResource.title;
    //    cell.titleLabel.text = NSLocalizedString(lResource.title, nil);
    cell.active = NO;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.tryRequest.condition == TryConditionAny) { // double check :)
        self.tryRequest.selectedAuthMethod = @[[self.tryRequest.interactiveAuthMethods objectAtIndex:indexPath.item]];
        [self.navigationController popViewControllerAnimated:NO];
    }
}

#pragma mark - Button Methods

- (IBAction)cancelProbeButtonPressed:(id)sender {
    NSError* error = nil;
    BMTryRequestRoot* cancelTry = [TRY_REQUEST_HANDLER nextTryRequest];
    [STATE_MACHINE fireEvent:STATE_MACHINE.biometricAuthCanceled userInfo: cancelTry ? @{@"cancelTry" : cancelTry} : nil error:&error];
}

- (IBAction)startVerification:(id)sender {
    self.tryRequest.selectedAuthMethod = self.tryRequest.interactiveAuthMethods;
    [self.navigationController popViewControllerAnimated:NO];
}

@end
