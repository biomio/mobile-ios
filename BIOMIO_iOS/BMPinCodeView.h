//
//  BMPinCodeView.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 12/16/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BMPinCodeView;
@protocol PinCodeViewDelegate <NSObject>
- (void) pinCodeConfirmed:(BMPinCodeView*) view;
- (void) pinCodeDidExpire:(BMPinCodeView*) view;
@end

@interface BMPinCodeView : UIView <UITextFieldDelegate>
@property id <PinCodeViewDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIView* mainView;
@property (strong, nonatomic) IBOutlet UILabel *pinCodeLabel;
@property (strong, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UILabel *countdownLabel;

@property (strong, nonatomic) NSString* pincodeString;
@property (assign, nonatomic) NSTimeInterval timeoutPeriod;


+ (instancetype) pinCodeViewWithFrame:(CGRect) frame
                                 code:(NSString*) code
                     expirationPeriod:(NSTimeInterval) expire
                             delegate:(id) delegateObject;

@end
