//
//  BMDeviceResources.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 8/12/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import "BMDeviceResources.h"
// frameworks
#import <AVFoundation/AVFoundation.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import <sys/utsname.h>
#import "UIKit/UIKit.h"
#import <CoreLocation/CoreLocation.h>
// handlers
#import "BMErrorHandler.h"
@implementation BMDeviceResources

+ (void) logDeviceResources {
    NSLog(@"=== DEVICE RESOURCES ===\nDEVICE MODEL: %@ \nTOUCH ID: %@ \nMIC: %@ \nCAM: %@ \nCAM RES: %@ \nFLASH: %@ \nTORCH: %@",
          [self deviceModel],
          [self isTouchIDAvailableWithError:nil] ? @"YES":@"NO",
          [self isMicrophoneAvailableWithError:nil] ? @"YES":@"NO",
          [self isCameraAvailableWithError:nil] ? @"YES":@"NO",
          [self cameraResolutions],
          [self isCameraFlashAvailable] ? @"YES":@"NO",
          [self isCameraTorchAvailable] ? @"YES":@"NO" );
}

+ (NSString*) deviceModel {
    struct utsname systemInfo;
    uname(&systemInfo);
    return [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
}

#pragma mark - Network

+ (dataNetworkType) dataNetworkType {
    NSArray *subviews = [[[[UIApplication sharedApplication] valueForKey:@"statusBar"] valueForKey:@"foregroundView"]subviews];
    NSNumber *dataNetworkItemView = nil;
    
    for (id subview in subviews) {
        if([subview isKindOfClass:[NSClassFromString(@"UIStatusBarDataNetworkItemView") class]]) {
            dataNetworkItemView = subview;
            break;
        }
    }
    
    switch ([[dataNetworkItemView valueForKey:@"dataNetworkType"]integerValue]) {
        case 0:
            return dataNetworkTypeOffline;
        case 1:
            return dataNetworkType2G;
        case 2:
            return dataNetworkType3G;
        case 3:
            return dataNetworkType4G;
        case 4:
            return dataNetworkTypeLTE;
        case 5:
            return dataNetworkTypeWiFi;
        default:
            return dataNetworkTypeOffline;
    }
}

+ (NSString*) dataNetworkTypeTitle {
    switch ([BMDeviceResources dataNetworkType]) {
        case dataNetworkTypeOffline:
            return @"Offline";
        case dataNetworkType2G:
            return @"2G";
        case dataNetworkType3G:
            return @"3G";
        case dataNetworkType4G:
            return @"4G";
        case dataNetworkTypeLTE:
            return @"LTE";
        case dataNetworkTypeWiFi:
            return @"WiFi";
        default:
            return @"";
    }
}

+ (NSString*) apnsToken {
    if ([[UIApplication sharedApplication] isRegisteredForRemoteNotifications])
        return [[NSUserDefaults standardUserDefaults]objectForKey:APNS_TOKEN_NSSTRING];
    return nil;
}

+ (NSArray*) deviceCapabilitiesWarnings {
    NSMutableArray* warningsArray = [NSMutableArray new];
    { // CAMERA
        NSError* error;
        [BMDeviceResources isCameraAvailableWithError:&error];
        if (error) {
            NSMutableDictionary* warning = [NSMutableDictionary dictionaryWithDictionary:@{@"authMethods": @[@"camera_resource"],
                                                                                           @"error" : error}];
            NSString* cameraDescription = @"Biomio authenticator uses camera for various identity verification techniques such as face recognition and document scanning.";
            switch (error.code) {
                case 41002:
                    [warning addEntriesFromDictionary:@{@"title" : @"Camera access denied",
                                                        @"description" : cameraDescription,
                                                        @"solution" : @"Please review camera permissions in Settings to enable camera access."}];
                    break;
                case 41003:
                    [warning addEntriesFromDictionary:@{@"title" : @"Camera access restricted",
                                                        @"description" : cameraDescription,
                                                        @"solution" : @"Please review camera permissions in Settings to enable camera access."}];
                    break;
                default:
                    [warning addEntriesFromDictionary:@{@"title" : @"Camera not available",
                                                        @"description" : cameraDescription,
                                                        @"solution" : @"Please review camera settings to enable camera access."}];
                    break;
            }
            [warningsArray addObject: warning];
        }
    }
    { // TOUCH ID
        NSError* error;
        [BMDeviceResources isTouchIDAvailableWithError:&error];
        if (error) {
            NSMutableDictionary* warning = [NSMutableDictionary dictionaryWithDictionary:@{@"authMethods": @[@"fp"],
                                                                                           @"error" : error}];
            NSString* fpDescription = @"Biomio uses built-in Touch ID scanner for authentication using your fingerprint. Your fingerprint data never leaves the device.";
            switch (error.code) {
                case 42002:
                    [warning addEntriesFromDictionary:@{@"title" : @"Password not set",
                                                        @"description" : fpDescription,
                                                        @"solution" : @"You will need to set system password to enable fingerprint authentication."}];
                    break;
                case 42007:
                    [warning addEntriesFromDictionary:@{@"title" : @"Touch ID not supported",
                                                        @"description" : fpDescription,
                                                        @"solution" : @"You will need a device with Touch ID sensor to enable fingerprint authentication."}];
                    break;
                case 42008:
                    [warning addEntriesFromDictionary:@{@"title" : @"Fingerprints not enrolled",
                                                        @"description" : fpDescription,
                                                        @"solution" : @"You will need to enroll at least one fingerprint to enable fingerprint authentication."}];
                    break;
                default:
                    [warning addEntriesFromDictionary:@{@"title" : @"Touch ID not available",
                                                        @"description" : fpDescription,
                                                        @"solution" : @"Please review Touch ID settings to enable fingerprint authentication."}];
                    break;
            }
            [warningsArray addObject: warning];
        }
    }
    { // LOCATION
        NSError* error;
        [BMDeviceResources isLocationServicesAvailableWithError:&error];
        if (error) {
            NSMutableDictionary* warning = [NSMutableDictionary dictionaryWithDictionary:@{@"authMethods": @[@"location_resource"],
                                                                                           @"error" : error}];
            NSString* locationDescription = @"Biomio uses location services for various identity verification techniques.";
            switch (error.code) {
                case 44002:
                    [warning addEntriesFromDictionary:@{@"title" : @"Location services access denied",
                                                        @"description" : locationDescription,
                                                        @"solution" : @"Please review location services permissions in Settings to enable location-based authentication."}];
                    break;
                case 44003:
                    [warning addEntriesFromDictionary:@{@"title" : @"Location services access restricted",
                                                        @"description" : locationDescription,
                                                        @"solution" : @"Please review location services permissions in Settings to enable location-based authentication."}];
                    break;
                default:
                    [warning addEntriesFromDictionary:@{@"title" : @"Location services not available",
                                                        @"description" : locationDescription,
                                                        @"solution" : @"Please review location services settings to enable location-based authentication."}];
                    break;
            }
            [warningsArray addObject: warning];
            
        }
    }
    { // MIC
        NSError* error;
        [BMDeviceResources isMicrophoneAvailableWithError:&error];
        if (error) {
            NSMutableDictionary* warning = [NSMutableDictionary dictionaryWithDictionary:@{@"authMethods": @[@"voice"],
                                                                                           @"error" : error}];
            NSString* micDescription = @"Biomio authenticator uses microphone for voice recognition authentication.";
            switch (error.code) {
                case 43002:
                    [warning addEntriesFromDictionary:@{@"title" : @"Microphone access denied",
                                                        @"description" : micDescription,
                                                        @"solution" : @"Please review microphone permissions in Settings to enable voice recognition."}];
                    break;
                default:
                    [warning addEntriesFromDictionary:@{@"title" : @"Microphone not found",
                                                        @"description" : micDescription,
                                                        @"solution" : @"Please review microphone settings to enable voice recognition."}];
                    break;
            }
            [warningsArray addObject: warning];
        }
    }
    return warningsArray;
}

#pragma mark - Device capabilities

+ (BOOL) isTouchIDAvailableWithError:(NSError**) error {
    LAContext *myContext = [[LAContext alloc] init];
    NSError *authError = nil;
    
    [myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError];
    
    if (authError && error) {
        switch (authError.code) {
            case LAErrorPasscodeNotSet:
                *error = [BMErrorHandler errorWithCode:42002];
                return NO;
            case LAErrorTouchIDNotAvailable:
                *error = [BMErrorHandler errorWithCode:42007];
                return NO;
            case LAErrorTouchIDNotEnrolled:
                *error = [BMErrorHandler errorWithCode:42008];
                return NO;
            case LAErrorSystemCancel:
                *error = [BMErrorHandler errorWithCode:42006];
                return NO;
                
            default:
                *error = [BMErrorHandler errorWithCode:42001];
                return NO;
        }
    } else if (authError)
        return NO;
    return YES;
}

+ (BOOL) isMicrophoneAvailableWithError:(NSError**) error {
    NSArray* devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio];
    if ([devices count] == 0) {
        if (error)
            *error = [BMErrorHandler errorWithCode:41003];
        return NO;
    }
    
    switch ([[AVAudioSession sharedInstance] recordPermission]) {
        case AVAudioSessionRecordPermissionGranted:
            return YES;
        case AVAudioSessionRecordPermissionDenied:{
            if (error)
                *error = [BMErrorHandler errorWithCode:43002];
            return NO;
        }
        case AVAudioSessionRecordPermissionUndetermined: {
            __block NSString* access;
            [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
                if(granted)
                    access = @"YES";
                else {
                    access = @"NO";
                    if (error)
                        *error = [BMErrorHandler errorWithCode:43002];
                }
            }];
            return [access isEqualToString:@"YES"];
        }
        default:
            return NO;
    }
}

+ (BOOL) isLocationServicesAvailableWithError:(NSError**) error {
    if([CLLocationManager locationServicesEnabled]) {
        switch ([CLLocationManager authorizationStatus]) {
            case kCLAuthorizationStatusDenied:
                if (error)
                    *error = [BMErrorHandler errorWithCode:44002];
                return NO;
            case kCLAuthorizationStatusRestricted:
                if (error)
                    *error = [BMErrorHandler errorWithCode:44003];
                return NO;
                
            default:
                return YES;
        }
    }
    *error = [BMErrorHandler errorWithCode:44004];
    return NO;
}

#pragma mark Camera

+ (BOOL) isCameraAvailableWithError: (NSError **) error {
    NSArray* devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    if ([devices count] == 0) {
        if (error)
            *error = [BMErrorHandler errorWithCode:41004];
        return NO;
    }
    
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if(status == AVAuthorizationStatusAuthorized) {
        return YES;
    } else if(status == AVAuthorizationStatusDenied){
        if (error)
            *error = [BMErrorHandler errorWithCode:41002];
        return NO;
    } else if(status == AVAuthorizationStatusRestricted){
        if (error)
            *error = [BMErrorHandler errorWithCode:41003];
        return NO;
    } else if (status == AVAuthorizationStatusNotDetermined){
        // not determined
        __block NSString* access;
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){
                access = @"YES";
            } else {
                access = @"NO";
                if (error)
                    *error = [BMErrorHandler errorWithCode:41002];
            }
        }];
        return [access isEqualToString:@"YES"];
    }
    return NO;
}

+ (BOOL) isCameraFlashAvailable {
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    return [device hasFlash];
}

+ (BOOL) isCameraTorchAvailable {
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    return [device hasTorch];
}

#pragma mark Camera resolutions

/**
 *  returns all posible front and rear camera resolutions based on device model.
 *
 *  @return NSDictionary with 2 key/value pairs
 */
+ (NSDictionary*) cameraResolutions {
    NSString* deviceModel = [self deviceModel];
    
    // iPhone 4S
    if ([deviceModel isEqualToString:@"iPhone4,1"]) {
        NSString* lFrontCam = @"640x480,480x360,192x144";
        NSString* lBackCam = @"3264x2448,1920x1080,1280x720,640x480,480x360,192x144";
        NSDictionary* lCamResolutions = @{@"front":lFrontCam,
                                          @"back":lBackCam};
        return lCamResolutions;
    }
    // iPhone 5/5C/5S/6/6+/iPod Touch 6
    else if ([deviceModel isEqualToString:@"iPhone5,1"]
             || [deviceModel isEqualToString:@"iPhone5,2"]
             || [deviceModel isEqualToString:@"iPhone5,3"]
             || [deviceModel isEqualToString:@"iPhone5,4"]
             || [deviceModel isEqualToString:@"iPhone6,1"]
             || [deviceModel isEqualToString:@"iPhone6,2"]
             || [deviceModel isEqualToString:@"iPhone7,1"]
             || [deviceModel isEqualToString:@"iPhone7,2"]
             || [deviceModel isEqualToString:@"iPod7,1"]) {
        NSString* lFrontCam = @"1280x960,1280x720,640x480,480x360,192x144";
        NSString* lBackCam = @"3264x2448,1920x1080,1280x720,640x480,480x360,192x144";
        NSDictionary* lCamResolutions = @{@"front":lFrontCam,
                                          @"back":lBackCam};
        return lCamResolutions;
    }
    // iPad 2
    else if ([deviceModel isEqualToString:@"iPad2,1"]
             || [deviceModel isEqualToString:@"iPad2,2"]
             || [deviceModel isEqualToString:@"iPad2,3"]
             || [deviceModel isEqualToString:@"iPad2,4"]) {
        NSString* lFrontCam = @"640x480,480x360,192x144";
        NSString* lBackCam = @"1280x720,960x720,640x480,480x360,192x144";
        NSDictionary* lCamResolutions = @{@"front":lFrontCam,
                                          @"back":lBackCam};
        return lCamResolutions;
    }
    // iPad 3
    else if ([deviceModel isEqualToString:@"iPad3,1"]
             || [deviceModel isEqualToString:@"iPad3,2"]
             || [deviceModel isEqualToString:@"iPad3,3"]) {
        NSString* lFrontCam = @"640x480,480x360,192x144";
        NSString* lBackCam = @"2592x1936,1920x1080,1280x720,640x480,480x360,192x144";
        NSDictionary* lCamResolutions = @{@"front":lFrontCam,
                                          @"back":lBackCam};
        return lCamResolutions;
    }
    // iPad 4/Air/Mini/Mini 2/Mini 3/iPod 5G
    else if ([deviceModel isEqualToString:@"iPad3,4"]
             || [deviceModel isEqualToString:@"iPad3,5"]
             || [deviceModel isEqualToString:@"iPad3,6"]
             || [deviceModel isEqualToString:@"iPad4,1"]
             || [deviceModel isEqualToString:@"iPad4,2"]
             || [deviceModel isEqualToString:@"iPad4,3"]
             || [deviceModel isEqualToString:@"iPad2,5"]
             || [deviceModel isEqualToString:@"iPad2,6"]
             || [deviceModel isEqualToString:@"iPad2,7"]
             || [deviceModel isEqualToString:@"iPad4,4"]
             || [deviceModel isEqualToString:@"iPad4,5"]
             || [deviceModel isEqualToString:@"iPad4,6"]
             || [deviceModel isEqualToString:@"iPad4,7"]
             || [deviceModel isEqualToString:@"iPad4,8"]
             || [deviceModel isEqualToString:@"iPad4,9"]
             || [deviceModel isEqualToString:@"iPod5,1"]) {
        
        NSString* lFrontCam = @"1280x960,1280x720,640x480,480x360,192x144";
        NSString* lBackCam = @"2592x1936,1920x1080,1280x720,640x480,480x360,192x144";
        NSDictionary* lCamResolutions = @{@"front":lFrontCam,
                                          @"back":lBackCam};
        return lCamResolutions;
    }
    // iPad Air 2/Mini 4/Pro
    else if ([deviceModel isEqualToString:@"iPad5,3"]
             || [deviceModel isEqualToString:@"iPad5,4"]
             || [deviceModel isEqualToString:@"iPad5,1"]
             || [deviceModel isEqualToString:@"iPad5,2"]
             || [deviceModel isEqualToString:@"iPad6,7"]
             || [deviceModel isEqualToString:@"iPad6,8"]) {
        NSString* lFrontCam = @"1280x960,1280x720,640x480,480x360,192x144";
        NSString* lBackCam = @"3264x2448,1920x1080,1280x720,640x480,480x360,192x144";
        NSDictionary* lCamResolutions = @{@"front":lFrontCam,
                                          @"back":lBackCam};
        return lCamResolutions;
    }
    // iPhone 6S/6S+/SE/ iPad Pro 9.7
    else if ([deviceModel isEqualToString:@"iPhone8,1"]
             || [deviceModel isEqualToString:@"iPhone8,2"]
             || [deviceModel isEqualToString:@"iPhone8,4"]) { // SE
        NSString* lFrontCam = @"1280x960,1280x720,640x480,480x360,192x144";
        NSString* lBackCam = @"4032x3024,1920x1080,1280x720,640x480,480x360,192x144";
        NSDictionary* lCamResolutions = @{@"front":lFrontCam,
                                          @"back":lBackCam};
        return lCamResolutions;
    }
    // iPhone 7/7+
    else if ([deviceModel isEqualToString:@"iPhone9,1"] // 7
             || [deviceModel isEqualToString:@"iPhone9,3"]
             || [deviceModel isEqualToString:@"iPhone9,2"] // 7+
             || [deviceModel isEqualToString:@"iPhone9,4"]) {
        NSString* lFrontCam = @"1280x720,640x480,480x360,192x144";
        NSString* lBackCam = @"4032x3024,1920x1080,1280x720,640x480,480x360,192x144";
        NSDictionary* lCamResolutions = @{@"front":lFrontCam,
                                          @"back":lBackCam};
        return lCamResolutions;
    }
    else {
        NSString* lFrontCam = @"640x480,480x360,192x144";
        NSString* lBackCam = @"1280x720,640x480,480x360,192x144";
        NSDictionary* lCamResolutions = @{@"front":lFrontCam,
                                          @"back":lBackCam};
        return lCamResolutions;
    }
}

+ (NSString*) captureSessionPresetByResolution: (NSString*) pResolution {
    if ([pResolution isEqualToString:@"1920x1080"]) {
        return AVCaptureSessionPreset1920x1080;
    }
    else if ([pResolution isEqualToString:@"1280x720"]) {
        return AVCaptureSessionPreset1280x720;
    }
    else if ([pResolution isEqualToString:@"640x480"]) {
        return AVCaptureSessionPreset640x480;
    }
    else if ([pResolution isEqualToString:@"3264x2448"]
             || [pResolution isEqualToString:@"2592x1936"]
             || [pResolution isEqualToString:@"1280x960"]
             || [pResolution isEqualToString:@"4032x3024"]) {
        return AVCaptureSessionPresetPhoto;
    }
    else if ([pResolution isEqualToString:@"480x360"]) {
        return AVCaptureSessionPresetMedium;
    }
    else if ([pResolution isEqualToString:@"192x144"]) {
        return AVCaptureSessionPresetLow;
    }
    else {
        return AVCaptureSessionPresetHigh;
    }
}

/*
 struct utsname systemInfo;
 uname(&systemInfo);
 [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
 
 
 @"i386":     @"iPhone Simulator",
 @"x86_64":   @"iPad Simulator",
 
 @"iPhone1,1":    @"iPhone",
 @"iPhone1,2":    @"iPhone 3G",
 @"iPhone2,1":    @"iPhone 3GS",
 @"iPhone3,1":    @"iPhone 4",
 @"iPhone3,2":    @"iPhone 4(Rev A)",
 @"iPhone3,3":    @"iPhone 4(CDMA)",
 @"iPhone4,1":    @"iPhone 4S",
 @"iPhone5,1":    @"iPhone 5(GSM)",
 @"iPhone5,2":    @"iPhone 5(GSM+CDMA)",
 @"iPhone5,3":    @"iPhone 5c(GSM)",
 @"iPhone5,4":    @"iPhone 5c(GSM+CDMA)",
 @"iPhone6,1":    @"iPhone 5s(GSM)",
 @"iPhone6,2":    @"iPhone 5s(GSM+CDMA)",
 
 @"iPhone7,1":    @"iPhone 6+ (GSM+CDMA)",
 @"iPhone7,2":    @"iPhone 6 (GSM+CDMA)",
 
 @"iPad1,1":  @"iPad",
 @"iPad2,1":  @"iPad 2(WiFi)",
 @"iPad2,2":  @"iPad 2(GSM)",
 @"iPad2,3":  @"iPad 2(CDMA)",
 @"iPad2,4":  @"iPad 2(WiFi Rev A)",
 @"iPad2,5":  @"iPad Mini 1G (WiFi)",
 @"iPad2,6":  @"iPad Mini 1G (GSM)",
 @"iPad2,7":  @"iPad Mini 1G (GSM+CDMA)",
 @"iPad3,1":  @"iPad 3(WiFi)",
 @"iPad3,2":  @"iPad 3(GSM+CDMA)",
 @"iPad3,3":  @"iPad 3(GSM)",
 @"iPad3,4":  @"iPad 4(WiFi)",
 @"iPad3,5":  @"iPad 4(GSM)",
 @"iPad3,6":  @"iPad 4(GSM+CDMA)",
 
 @"iPad4,1":  @"iPad Air(WiFi)",
 @"iPad4,2":  @"iPad Air(GSM)",
 @"iPad4,3":  @"iPad Air(GSM+CDMA)",
 
 @"iPad5,3":  @"iPad Air 2 (WiFi)",
 @"iPad5,4":  @"iPad Air 2 (GSM)",
 
 @"iPad4,4":  @"iPad Mini 2G (WiFi)",
 @"iPad4,5":  @"iPad Mini 2G (GSM)",
 @"iPad4,6":  @"iPad Mini 2G (GSM+CDMA)",
 @"iPad4,7":  @"iPad Mini 3G (WiFi)",
 @"iPad4,8":  @"iPad Mini 3G (GSM)",
 
 @"iPod1,1":  @"iPod 1st Gen",
 @"iPod2,1":  @"iPod 2nd Gen",
 @"iPod3,1":  @"iPod 3rd Gen",
 @"iPod4,1":  @"iPod 4th Gen",
 @"iPod5,1":  @"iPod 5th Gen",
 @"iPod7,1":  @"iPod 6th Gen",
 
 http://www.everyi.com/by-identifier/ipod-iphone-ipad-specs-by-model-identifier.html
 
 
 Camera Resolutions
 http://stackoverflow.com/questions/19422322/method-to-find-devices-camera-resolution-ios
 
 
 Here is the a list of tested camera sessionPreset`s on devices, that support iOS 9
 
 Updated with iPhone 6S and iPad Pro
 iPhone
 iPhone 4S
 FRONT camera
 AVCaptureSessionPresetPhoto = 640x480
 AVCaptureSessionPresetHigh = 640x480
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = not supported
 AVCaptureSessionPreset1920x1080 = not supported
 BACK camera
 AVCaptureSessionPresetPhoto = 3264x2448
 AVCaptureSessionPresetHigh = 1920x1080
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = 1280x720
 AVCaptureSessionPreset1920x1080 = 1920x1080
 iPhone 5/5C/5S/6/6+
 FRONT camera
 AVCaptureSessionPresetPhoto = 1280x960
 AVCaptureSessionPresetHigh = 1280x720
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = 1280x720
 AVCaptureSessionPreset1920x1080 = not supported
 BACK camera
 AVCaptureSessionPresetPhoto = 3264x2448
 AVCaptureSessionPresetHigh = 1920x1080
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = 1280x720
 AVCaptureSessionPreset1920x1080 = 1920x1080
 iPhone 6S/6S+
 FRONT camera
 AVCaptureSessionPresetPhoto = 1280x960
 AVCaptureSessionPresetHigh = 1280x720
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = 1280x720
 AVCaptureSessionPreset1920x1080 = not supported
 BACK camera
 AVCaptureSessionPresetPhoto = 4032x3024
 AVCaptureSessionPresetHigh = 1920x1080
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = 1280x720
 AVCaptureSessionPreset1920x1080 = 1920x1080
 iPad
 iPad 2
 FRONT camera
 AVCaptureSessionPresetPhoto = 640x480
 AVCaptureSessionPresetHigh = 640x480
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = not supported
 AVCaptureSessionPreset1920x1080 = not supported
 BACK camera
 AVCaptureSessionPresetPhoto = 960x720
 AVCaptureSessionPresetHigh = 1280x720
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = 1280x720
 AVCaptureSessionPreset1920x1080 = not supported
 iPad 3
 FRONT camera
 AVCaptureSessionPresetPhoto = 640x480
 AVCaptureSessionPresetHigh = 640x480
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = not supported
 AVCaptureSessionPreset1920x1080 = not supported
 BACK camera
 AVCaptureSessionPresetPhoto = 2592x1936
 AVCaptureSessionPresetHigh = 1920x1080
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = 1280x720
 AVCaptureSessionPreset1920x1080 = 1920x1080
 iPad 4/Air/Mini/Mini 2/Mini 3/iPod 5G
 FRONT camera
 AVCaptureSessionPresetPhoto = 1280x960
 AVCaptureSessionPresetHigh = 1280x720
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = 1280x720
 AVCaptureSessionPreset1920x1080 = not supported
 BACK camera
 AVCaptureSessionPresetPhoto = 2592x1936
 AVCaptureSessionPresetHigh = 1920x1080
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = 1280x720
 AVCaptureSessionPreset1920x1080 = 1920x1080
 iPad Air 2/Mini 4/Pro
 FRONT camera
 AVCaptureSessionPresetPhoto = 1280x960
 AVCaptureSessionPresetHigh = 1280x720
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = 1280x720
 AVCaptureSessionPreset1920x1080 = not supported
 BACK camera
 AVCaptureSessionPresetPhoto = 3264x2448
 AVCaptureSessionPresetHigh = 1920x1080
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = 1280x720
 AVCaptureSessionPreset1920x1080 = 1920x1080
 iPod
 iPod Touch 5
 FRONT camera
 AVCaptureSessionPresetPhoto = 1280x960
 AVCaptureSessionPresetHigh = 1280x720
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = 1280x720
 AVCaptureSessionPreset1920x1080 = not supported
 BACK camera
 AVCaptureSessionPresetPhoto = 2592x1936
 AVCaptureSessionPresetHigh = 1920x1080
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = 1280x720
 AVCaptureSessionPreset1920x1080 = 1920x1080
 iPod Touch 6
 FRONT camera
 AVCaptureSessionPresetPhoto = 1280x960
 AVCaptureSessionPresetHigh = 1280x720
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = 1280x720
 AVCaptureSessionPreset1920x1080 = not supported
 BACK camera
 AVCaptureSessionPresetPhoto = 3264x2448
 AVCaptureSessionPresetHigh = 1920x1080
 AVCaptureSessionPresetMedium = 480x360
 AVCaptureSessionPresetLow = 192x144
 AVCaptureSessionPreset640x480 = 640x480
 AVCaptureSessionPreset1280x720 = 1280x720
 AVCaptureSessionPreset1920x1080 = 1920x1080
 
 */

@end
