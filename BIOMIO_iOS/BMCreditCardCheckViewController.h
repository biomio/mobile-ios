//
//  BMCreditCardCheckViewController.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 1/27/16.
//  Copyright © 2016 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CreditCardCheckViewControllerDelegate <NSObject>
- (void) userDidConfirmCardNumber:(NSString*) pNumber expiryMonth: (NSString*) pMonth expiryYear: (NSString*) pYear;
- (void) userDidCancelAuthentication;
@end

@interface BMCreditCardCheckViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *cardNumberTextfield;
@property (strong, nonatomic) IBOutlet UIPickerView *datePickerView;

@property (strong, nonatomic) NSString* cardNumber;
@property (assign, nonatomic) NSUInteger expiryMonth;
@property (assign, nonatomic) NSUInteger expiryYear;
@property id <CreditCardCheckViewControllerDelegate> delegate;

@end
