//
//  MenuCollectionViewCell.h
//
//  Created by Roman Slyepko on 31.08.15.
//  Copyright (c) 2015 Roman Slyepko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *menuItemImageView;
@property (strong, nonatomic) IBOutlet UILabel *menuItemTitleLabel;

@end
