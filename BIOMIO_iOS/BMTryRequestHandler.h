//
//  BMTryRequestHandler.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 11/19/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BMTryRequestRoot.h"
// models
#import "BMLocationRequestOperation.h"

@protocol BMTryRequestHandlerDelegate <NSObject>
- (void) nonInteractiveAuthMethodsDidUpdate;
- (void) tryRequestDidExpire:(BMTryRequestRoot*) pTry;
@optional
- (void) interactiveAuthMethodsDidUpdate;
- (void) didAddTryRequest:(BMTryRequestRoot*) tryRequest;
@end

@interface BMTryRequestHandler : NSObject <LocationRequestOperationDelegate>

@property id <BMTryRequestHandlerDelegate> delegate;

@property (nonatomic,strong) NSMutableArray* tryRequests;
@property (nonatomic,strong) BMTryAuthMethod* activeInteractiveAuthMethod;


#pragma mark - TryRequest
- (void) addTryRequest: (BMTryRequestRoot*) pTry;
- (BMTryRequestRoot*) nextTryRequest;
- (BMTryRequestRoot*) activeTryRequest;
- (BMTryRequestRoot*) tryRequestWithID:(NSString*) pID;

- (NSArray*) nonInteractiveOperationQueue;
- (void) addAuthResultToActiveInteractiveAuthMethod: (id) pResult;
- (BMTryAuthMethod*) nextInteractiveAuthMethod;

- (BMTryAuthMethod*) authMethodWithTitle:(NSString*) title parrentTryID:(NSString*) tryID;

- (void) completeAuthMethod: (BMTryAuthMethod*) authMethod;

- (BOOL) isUploadingProbeData;

#pragma mark - Cancel
- (void) cancelTryRequest: (BMTryRequestRoot*) tryRequest;
- (void) cancelTryRequestWithID: (NSString*) tryID;
//TODO: rename
- (BOOL) readyToRun;

@end
