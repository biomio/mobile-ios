//
//  BMAnalyticsHandler.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 1/10/17.
//  Copyright © 2017 BIOMIO. All rights reserved.
//

#import "BMAnalyticsHandler.h"
// analytics
#import <Intercom/Intercom.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@implementation BMAnalyticsHandler

+ (void)setup {
    [Fabric with:@[[Crashlytics class]]];
    [Intercom setApiKey:INTERCOM_API_KEY forAppId:INTERCOM_APP_ID];
}

+ (void)logEvent:(NSString*) eventName properties:(NSDictionary*) customProperties {
#ifdef DEBUG
#if !DEBUG
    [Answers logCustomEventWithName:eventName customAttributes: customProperties];
    [Intercom logEventWithName:eventName metaData:customProperties];
#endif
#endif
}

@end
