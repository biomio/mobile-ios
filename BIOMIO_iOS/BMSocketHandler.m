//
//  BMSocketHandler.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 1/22/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import "BMSocketHandler.h"

// handlers
#import "BMStateMachine.h"
#import "BMSocketLogger.h"
#import "BMUserModel.h"
#import "BMDeviceResources.h"
#import "BMErrorHandler.h"

// models
#import "BMTryAuthMethod.h"
#import "BMTryRequestRoot.h"

// libs/frameworks
#import "SRWebSocket.h"
#include <openssl/pem.h>
#include <openssl/engine.h>
#include <iomanip>
#import <CommonCrypto/CommonDigest.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import <SAMKeychain/SAMKeychain.h>
#import <AVFoundation/AVFoundation.h>

#define TTL_CALIBRATION_TIME                    10.0
#define CONNECTION_TIMEOUT                      30.0
#define CONNECTION_NOT_RESPONDING_PERIOD        sessionttl+10

typedef NS_ENUM(NSInteger, SocketReceivedMessageType){
    ReceivedMessageRegistrationServerHello,
    ReceivedMessageRegularServerHello,
    ReceivedMessageBye,
    ReceivedMessageNop,
    ReceivedMessageRPCresponse,
    ReceivedMessageGetResources,
    ReceivedMessageTry
};

typedef NS_ENUM(NSInteger, SocketSendMessageType){
    SendMessageRegistrationClientHello,
    SendMessageRegularClientHello,
    SendMessageAck,
    SendMessageAuth,
    SendMessageNop,
    SendMessageNopTokenRefresh,
    SendMessageRPCRequest,
    SendMessageDeviceResources,
    SendMessageProbe,
    SendMessageBye,
    
    SendMessageNil
};

@implementation BMProbeStatusEnum
+ (NSString *)probeStatusToString:(BMProbeStatus)status {
    switch (status) {
        case BMProbeStatusSuccess:
            return @"success";
        case BMProbeStatusCanceled:
            return @"canceled";
        case BMProbeStatusFailed:
            return @"failed";
            
        default:
            break;
    }
}
@end

@interface BMSocketHandler () <SRWebSocketDelegate>
@end

@implementation BMSocketHandler {
    SRWebSocket *webSocket;
    
    int sesq;
    NSString* token;
    NSString* refreshToken;
    
    NSTimeInterval connectionttl;
    NSTimeInterval sessionttl;
    
    NSTimer* nopTimer;
    NSTimer* tokenRefreshTimer;
    NSTimer* socketRespondingTimer;
    
    UIBackgroundTaskIdentifier backgroundUploadID;
}

#pragma mark - Init

+ (BMSocketHandler*) sharedSocket {
    static BMSocketHandler *sharedSocket = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^
                  {
                      sharedSocket = [self new];
                  });
    return sharedSocket;
}

- (id)init {
    if (self = [super init]) {
        if (!self.tryRequestHandler)
            self.tryRequestHandler = [BMTryRequestHandler new];
        [self initStateMachineStates];
        NSError* error = nil;
        
        [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidStart userInfo:nil error:&error];
    }
    return self;
}

- (void) initStateMachineStates {
    [[BMStateMachine sharedHandler].disconnectedState setDidEnterStateBlock:^(TKState *state, TKTransition *transition) {
        NSLog(@"STATE CHANGED FROM %@ TO %@ BY EVENT %@", transition.sourceState.name, transition.destinationState.name, transition.event.name);
        if ([self.delegate respondsToSelector:@selector(socketDidChangeState:uploading:)])
            [self.delegate socketDidChangeState:BMSocketStateDisconnected uploading:NO];
        
        
        if ([transition.event isEqual:[BMStateMachine sharedHandler].socketWillClose]) {
            [self send:[self composeMessageOfType:SendMessageBye]];
        }
        
        webSocket.delegate = nil;
        webSocket = nil;
        [webSocket close];
        NSLog(@"WebSocket closed");
        
        self.tryRequestHandler = nil;
        
        [tokenRefreshTimer invalidate];
        tokenRefreshTimer = nil;
        [nopTimer invalidate];
        nopTimer = nil;
        [socketRespondingTimer invalidate];
        socketRespondingTimer = nil;
        
        // handle reconnection
        if ([transition.event isEqual:[BMStateMachine sharedHandler].connectionDidFail]) {
            NSInteger reconnectionTimeout = transition.userInfo [@"reconnect_timeout"] ? [transition.userInfo [@"reconnect_timeout"] integerValue] : CONNECTION_TIMEOUT;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(reconnectionTimeout * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                NSError* error;
                [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidStart userInfo:nil error:&error];
            });
            return;
        }
        
        if (transition.userInfo) {
            if (transition.userInfo [@"redirect_url"]) {
                [BMSocketHandler setServerUrl:transition.userInfo [@"redirect_url"]];
                NSError* error;
                [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidStart userInfo:nil error:&error];
            }
            if ([transition.userInfo [@"status"] isEqualToString:@"wrong code"]  ||
                [transition.userInfo [@"status"] isEqualToString:@"Not Found"]) {
                NSError* error;
                [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].registrationDidFail userInfo:transition.userInfo error:&error];
                if ([BMUserModel isUserBackupAvailable]) {
                    // TODO: ask user whether to restore login backup
                    // [BMUserModel restoreUserBackup];
                    [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidStart userInfo:nil error:&error];
                }
            }
            else if ([transition.userInfo [@"status"] isEqualToString:@"Regular handshake is inappropriate. It is required to run registration handshake first."]) {
                if ([self.delegate respondsToSelector:@selector(socketRequiresRegistration)])
                    [self.delegate socketRequiresRegistration];
            }
            else if ([transition.userInfo [@"status"] isEqualToString:@"Handhsake failed. Invalid signature."]) {
                [self restartWithUserReset:YES];
            }
            else if (![transition.userInfo [@"status"] isEqualToString:@"Connection timeout"] && ![transition.userInfo [@"status"] isEqualToString:@"Session expired"]) {
                NSError* error;
                [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidStart userInfo:nil error:&error];
            }
            else {
                [[NSUserDefaults standardUserDefaults] setObject:@"autoreconnect" forKey:@"APP_DID_RECEIVE_BYE"];
                NSError* error;
                [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidStart userInfo:nil error:&error];
            }
        }
    }];
    
    [[BMStateMachine sharedHandler].connectionState setDidEnterStateBlock:^(TKState *state, TKTransition *transition) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"APP_DID_RECEIVE_BYE"];
        NSLog(@"STATE CHANGED FROM %@ TO %@ BY EVENT %@", transition.sourceState.name, transition.destinationState.name, transition.event.name);
        
        if ([self.delegate respondsToSelector:@selector(socketDidChangeState:uploading:)])
            [self.delegate socketDidChangeState:BMSocketStateConnection uploading:YES];
        
        [self reconnectSocket];
    }];
    
    [[BMStateMachine sharedHandler].connectionOpenState setDidEnterStateBlock:^(TKState *state, TKTransition *transition) {
        NSLog(@"STATE CHANGED FROM %@ TO %@ BY EVENT %@", transition.sourceState.name, transition.destinationState.name, transition.event.name);
        
        if ([self.delegate respondsToSelector:@selector(socketDidChangeState:uploading:)])
            [self.delegate socketDidChangeState:BMSocketStateConnection uploading:NO];
        
        NSError* error;
        if ([[NSUserDefaults standardUserDefaults]valueForKey:@"BIOMIO_registration_secret_URL"]) {
            [[NSUserDefaults standardUserDefaults] setValue:[[NSUserDefaults standardUserDefaults]valueForKey:@"BIOMIO_registration_secret_URL"] forKey:@"BIOMIO_registration_secret"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"BIOMIO_registration_secret_URL"];
            if (![BMUserModel isRegistrationRequired]) {
                if ([self.delegate respondsToSelector:@selector(socketDidOpenUrlRegistrationCode)])
                    [self.delegate socketDidOpenUrlRegistrationCode];
            }
        }
        
        if (![BMUserModel isRegistrationRequired]) {
            [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidOpenWithUser userInfo:nil error:&error];
        }
        else if ([[NSUserDefaults standardUserDefaults]valueForKey:@"BIOMIO_registration_secret"]) {
            [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidOpenWithoutUser userInfo:nil error:&error];
        }
        else {
            [Intercom registerUnidentifiedUser];
            if ([self.delegate respondsToSelector:@selector(socketRequiresRegistration)]) {
                [self.delegate socketRequiresRegistration];
            }
        }
    }];
    
    [[BMStateMachine sharedHandler].registrationHelloState setDidEnterStateBlock:^(TKState *state, TKTransition *transition) {
        if ([self.delegate respondsToSelector:@selector(socketDidChangeState:uploading:)])
            [self.delegate socketDidChangeState:BMSocketStateConnection uploading:YES];
        
        NSLog(@"STATE CHANGED FROM %@ TO %@ BY EVENT %@", transition.sourceState.name, transition.destinationState.name, transition.event.name);
        NSLog(@"\n=== INITIALIZING REGISTRATION HANDSHAKE ===");
        BMUserModel* currentUser = [BMUserModel createUser];
        NSLog(@"\n=== GENERATING NEW USER ===\ndeviceID: \t%@\nappID: \t\t%@\nosID: \t\t%@\nprivKey: \t%@", currentUser.deviceID, currentUser.appID, [NSString stringWithFormat:@"iOS_%@", IOS_VERSION], currentUser.privateKey);
        
        [self send: [self composeMessageOfType:SendMessageRegistrationClientHello]];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"BIOMIO_registration_secret"];
    }];
    
    [[BMStateMachine sharedHandler].regularHelloState setDidEnterStateBlock:^(TKState *state, TKTransition *transition) {
        if ([self.delegate respondsToSelector:@selector(socketDidChangeState:uploading:)])
            [self.delegate socketDidChangeState:BMSocketStateConnection uploading:YES];
        NSLog(@"STATE CHANGED FROM %@ TO %@ BY EVENT %@", transition.sourceState.name, transition.destinationState.name, transition.event.name);
        
        NSLog(@"=== INITIALIZING REGULAR HANDSHAKE ===");
        [self send: [self composeMessageOfType:SendMessageRegularClientHello]];
    }];
    
    [[BMStateMachine sharedHandler].regularHandshakeState setDidEnterStateBlock:^(TKState *state, TKTransition *transition) {
        if ([self.delegate respondsToSelector:@selector(socketDidChangeState:uploading:)])
            [self.delegate socketDidChangeState:BMSocketStateConnection uploading:YES];
        NSLog(@"STATE CHANGED FROM %@ TO %@ BY EVENT %@", transition.sourceState.name, transition.destinationState.name, transition.event.name);
        
        // auth message
        NSError* error = nil;
        [self send: [self composeMessageOfType:SendMessageAuth]];
        [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].handshakeMessageDidSend userInfo:nil error:&error];
    }];
    
    [[BMStateMachine sharedHandler].registrationHandshakeState setDidEnterStateBlock:^(TKState *state, TKTransition *transition) {
        if ([self.delegate respondsToSelector:@selector(socketDidChangeState:uploading:)])
            [self.delegate socketDidChangeState:BMSocketStateConnection uploading:YES];
        NSLog(@"STATE CHANGED FROM %@ TO %@ BY EVENT %@", transition.sourceState.name, transition.destinationState.name, transition.event.name);
        
        NSError* error = nil;
        [self send: [self composeMessageOfType:SendMessageAck]];
        
        [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].handshakeMessageDidSend userInfo:nil error:&error];
    }];
    
    [[BMStateMachine sharedHandler].standbyeState setDidEnterStateBlock:^(TKState *state, TKTransition *transition) {
        if ([self.delegate respondsToSelector:@selector(socketDidChangeState:uploading:)])
            [self.delegate socketDidChangeState:BMSocketStateStandbye uploading:NO];
        NSLog(@"STATE CHANGED FROM %@ TO %@ BY EVENT %@", transition.sourceState.name, transition.destinationState.name, transition.event.name);
        
        [self send:[self composeMessageOfType:SendMessageNop]];
        if (![nopTimer isValid]) {
            nopTimer = [NSTimer scheduledTimerWithTimeInterval:connectionttl-TTL_CALIBRATION_TIME
                                                        target:self
                                                      selector:@selector(sendNopMessage:)
                                                      userInfo:nil
                                                       repeats:YES];
            nopTimer.tolerance = connectionttl*0.1;
        }
        if (![tokenRefreshTimer isValid]) {
            tokenRefreshTimer = [NSTimer scheduledTimerWithTimeInterval:sessionttl-TTL_CALIBRATION_TIME
                                                                 target:self
                                                               selector:@selector(sendTokenRefreshMessage:)
                                                               userInfo:nil
                                                                repeats:YES];
            tokenRefreshTimer.tolerance = sessionttl*0.1;
        }
        
        if ([transition.event isEqual: [BMStateMachine sharedHandler].biometricAuthCanceled]) {
            [SOCKET_HANDLER sendProbeMessageWithAuthMethod:[TRY_REQUEST_HANDLER activeInteractiveAuthMethod]
                                                authResult:nil
                                                    status:BMProbeStatusCanceled
                                                     error:nil];
            BMTryRequestRoot* removeTry = transition.userInfo[@"cancelTry"];
            [TRY_REQUEST_HANDLER cancelTryRequest:removeTry];
        }
        else if ([transition.event isEqual: [BMStateMachine sharedHandler].biometricAuthDidExpire]) {
            if ([self.delegate respondsToSelector:@selector(socketProbeDidTimeOut)])
                [self.delegate socketProbeDidTimeOut];
        }
        else if ([transition.event isEqual: [BMStateMachine sharedHandler].biometricAuthCanceledByServer]) {
        }
    }];
    
    [[BMStateMachine sharedHandler].biometricAuthState setDidEnterStateBlock:^(TKState *state, TKTransition *transition) {
        if ([self.delegate respondsToSelector:@selector(socketDidChangeState:uploading:)])
            [self.delegate socketDidChangeState:BMSocketStateStandbye uploading:NO];
        
        NSLog(@"STATE CHANGED FROM %@ TO %@ BY EVENT %@", transition.sourceState.name, transition.destinationState.name, transition.event.name);
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
        
        if ([self.delegate respondsToSelector:@selector(socketDidReceiveTryRequest)])
            [self.delegate socketDidReceiveTryRequest];
    }];
    
    [STATE_MACHINE.registrationServerHelloDidReceive setDidFireEventBlock:^(TKEvent *event, TKTransition *transition) {
        BMUserModel* currentUser = [BMUserModel currentUser];
        [Intercom registerUserWithUserId:currentUser.appID];
    }];
}

#pragma mark - Server URL

+ (NSString*) serverUrl {
    NSString* url = [[NSUserDefaults standardUserDefaults] valueForKey:@"SERVER_URL"];
    return url ? url : WEBSOCKET_URL;
}

+ (BOOL) restoreDefaultServerUrl {
    if (![[BMSocketHandler serverUrl] isEqualToString: WEBSOCKET_URL]) {
        [BMSocketHandler setServerUrl: WEBSOCKET_URL];
        return YES;
    }
    return NO;
}

+ (BOOL) setServerUrl:(NSString*) newServerUrl {
    if (![[BMSocketHandler serverUrl] isEqualToString: newServerUrl]) {
        [[NSUserDefaults standardUserDefaults] setValue: newServerUrl forKey:@"SERVER_URL"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return YES;
    }
    return NO;
}

#pragma mark - Connection methods

- (void) reconnectSocket {
    webSocket.delegate = nil;
    [webSocket close];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self connectSocket];
    });
}


- (void) connectSocket {
    sesq = 0;
    token = nil;
    
    NSURL *url;
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"SERVER_URL"])
        url = [NSURL URLWithString:[[NSUserDefaults standardUserDefaults] valueForKey:@"SERVER_URL"]];
    else {
        url = [NSURL URLWithString: WEBSOCKET_URL];
        [[NSUserDefaults standardUserDefaults]setValue:WEBSOCKET_URL forKey:@"SERVER_URL"];
    }
    
    NSLog(@"URL: %@", url);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    // SSL+certificate support
    NSString *cerPath;
    cerPath = [[NSBundle mainBundle] pathForResource:@"certificate-2017" ofType:@"der"];
    
    NSData *certData = [[NSData alloc] initWithContentsOfFile:cerPath];
    CFDataRef certDataRef = (__bridge CFDataRef)certData;
    SecCertificateRef certRef = SecCertificateCreateWithData(NULL, certDataRef);
    id certificate = (__bridge id)certRef;
    
    [request setSR_SSLPinnedCertificates:@[certificate]];
    webSocket = [[SRWebSocket alloc] initWithURLRequest:request];
    webSocket.delegate = self;
    [webSocket open];
    self.tryRequestHandler = [BMTryRequestHandler new];
}

- (void) restartWithUserReset: (BOOL) reset {
    if (reset) {
        [BMUserModel logOut];
        [Intercom reset];
    }
    NSError* error = nil;
    [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].socketWillClose userInfo:nil error:&error];
    error = nil;
    [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidStart userInfo:nil error:&error];
}

- (void) checkConnectionStatus: (NSTimer*) timer {
    if ([[BMStateMachine sharedHandler].currentState isEqual:[BMStateMachine sharedHandler].connectionState]) {
        NSError* error = nil;
        [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidStart userInfo:nil error:&error];
    }
}

- (void) checkSocketResponse {
    if (![[BMStateMachine sharedHandler].currentState isEqual:[BMStateMachine sharedHandler].disconnectedState]) {
        [self restartWithUserReset:NO];
    }
}

- (BOOL) isBusyUploadingProbe {
    return [TRY_REQUEST_HANDLER isUploadingProbeData];
}

- (void) forceUpdateStateVisualization {
    if ([self.delegate respondsToSelector:@selector(socketDidChangeState:uploading:)]) {
        if ([[BMStateMachine sharedHandler].currentState isEqual:[BMStateMachine sharedHandler].disconnectedState]) {
            [self.delegate socketDidChangeState:BMSocketStateDisconnected uploading:NO];
        }
        else if ([[BMStateMachine sharedHandler].currentState isEqual:[BMStateMachine sharedHandler].connectionState] ||
                 [[BMStateMachine sharedHandler].currentState isEqual:[BMStateMachine sharedHandler].regularHelloState] ||
                 [[BMStateMachine sharedHandler].currentState isEqual:[BMStateMachine sharedHandler].registrationHelloState] ||
                 [[BMStateMachine sharedHandler].currentState isEqual:[BMStateMachine sharedHandler].regularHandshakeState] ||
                 [[BMStateMachine sharedHandler].currentState isEqual:[BMStateMachine sharedHandler].registrationHandshakeState]) {
            [self.delegate socketDidChangeState:BMSocketStateConnection uploading:YES];
        }
        else if ([[BMStateMachine sharedHandler].currentState isEqual:[BMStateMachine sharedHandler].standbyeState] ||
                 [[BMStateMachine sharedHandler].currentState isEqual:[BMStateMachine sharedHandler].biometricAuthState]) {
            if ([self isBusyUploadingProbe])         // if (uploading photos)
                [self.delegate socketDidChangeState:BMSocketStateStandbye uploading:YES];
            else {
                [self endBackgroundProbeUpload];
                [self.delegate socketDidChangeState:BMSocketStateStandbye uploading:NO];
            }
        }
        else if ([[BMStateMachine sharedHandler].currentState isEqual:[BMStateMachine sharedHandler].connectionOpenState]) {
            [self.delegate socketDidChangeState:BMSocketStateConnection uploading:NO];
        }
    }
}

#pragma mark - Message Methods

/**
 *  @description Creates dictionary containing message header with current user info + token;
 *  @discussion In case some info (refresh token) needs to be overwriten - use [lHeaderDict setValue:@"new_value" forKey:@"your_key"] during composing the message body;
 *
 *  @return NSMutableDictionary with message header
 */
- (NSMutableDictionary*) composeMessageHeader {
    NSMutableDictionary* lHeaderDict = [NSMutableDictionary new];
    BMUserModel* currentUser = [BMUserModel currentUser];
    //         message header
    //          required
    [lHeaderDict setValue:@"clientHeader" forKey:@"oid"];
    [lHeaderDict setValue:[NSNumber numberWithInt:sesq] forKey:@"seq"];
    [lHeaderDict setValue:@"probe" forKey:@"appType"];
    [lHeaderDict setValue:@"1.0" forKey:@"protoVer"];
    
    [lHeaderDict setValue:[NSString stringWithFormat:@"iOS_%@", IOS_VERSION] forKey:@"osId"];
    [lHeaderDict setValue:currentUser.appID forKey:@"appId"];
    //          optional
    [lHeaderDict setValue:currentUser.deviceID forKey:@"devId"];
    
    [lHeaderDict setValue:[BMDeviceResources dataNetworkTypeTitle] forKey:@"network"];
    if (token)
        [lHeaderDict setValue:token forKey:@"token"];
    
    return lHeaderDict;
}

/**
 *  Creates message, containing header with required fields and body
 *
 *  @param pSendMessageType message-type (SocketSendMessageType enum)
 *
 *  @return JSON-formatted NSString object
 */
- (NSMutableDictionary*) composeMessageOfType: (SocketSendMessageType) pSendMessageType {
    NSMutableDictionary* lBodyDict = [NSMutableDictionary new];
    NSMutableDictionary* lHeaderDict = [[NSMutableDictionary alloc]initWithDictionary:[self composeMessageHeader]];
    NSMutableDictionary* lResultDict = [NSMutableDictionary new];
    
    if (pSendMessageType != SendMessageNil) {
        switch (pSendMessageType) {
            case SendMessageAck: {
                [lBodyDict setValue:@"ack" forKey:@"oid"];
                break;
            }
            case SendMessageNop: {
                [lBodyDict setValue:@"nop" forKey:@"oid"];
                break;
            }
            case SendMessageNopTokenRefresh: {
                [lHeaderDict setValue:refreshToken forKey:@"token"];
                [lBodyDict setValue:@"nop" forKey:@"oid"];
                break;
            }
            case SendMessageRegistrationClientHello: {
                [lHeaderDict removeObjectForKey:@"appId"];
                [lBodyDict setValue:@"clientHello" forKey:@"oid"];
                
                [lBodyDict setValue:[[NSUserDefaults standardUserDefaults]valueForKey:@"BIOMIO_registration_secret"] forKey:@"secret"];
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"BIOMIO_registration_secret"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                break;
            }
            case SendMessageRegularClientHello: {
                [lBodyDict setValue:@"clientHello" forKey:@"oid"];
                break;
            }
            case SendMessageAuth: {
                BMUserModel* currentUser = [BMUserModel currentUser];
                [lBodyDict setValue:@"auth" forKey:@"oid"];
                NSString* lHeaderString = [NSString stringWithFormat:@"{\"oid\":\"clientHeader\",\"seq\":%i,\"protoVer\":\"1.0\",\"appType\":\"probe\",\"appId\":\"%@\",\"osId\":\"%@\",\"devId\":\"%@\",\"token\":\"%@\"}",sesq, currentUser.appID,[NSString stringWithFormat:@"iOS_%@", IOS_VERSION], currentUser.deviceID, token];
                NSError* error;
                NSString* lPrivateKey = [SAMKeychain passwordForService:BIOMIO_PRIVKEY account:BIOMIO_KEYCHAIN error:&error];
                // Fix for transition from NSUserDefaults to KeyChain storage. Remove in some future, when nobody uses version 1.0
                if (!lPrivateKey)
                    lPrivateKey = [[NSUserDefaults standardUserDefaults]valueForKey:BIOMIO_PRIVKEY];
                if (!lPrivateKey) {
                    lPrivateKey = currentUser.privateKey;
                    NSError* error;
                    [SAMKeychain setPassword:lPrivateKey forService:BIOMIO_PRIVKEY account:BIOMIO_KEYCHAIN error:&error];
                }
                if (lPrivateKey) {
                    NSString* digest = [self signHeader:lHeaderString withPrivateKey:lPrivateKey];
                    [lBodyDict setValue:digest forKey:@"key"];
                }
                else {
                    if ([self.delegate respondsToSelector:@selector(socketRequiresRegistration)])
                        [self.delegate socketRequiresRegistration];
                }
                break;
            }
            case SendMessageRPCRequest: {
                NSMutableArray* keysArray = [NSMutableArray new];
                NSMutableArray* valuesArray = [NSMutableArray new];
                NSMutableDictionary* dataDict = [NSMutableDictionary new];
                [dataDict setValue:keysArray forKey:@"keys"];
                [dataDict setValue:valuesArray forKey:@"values"];
                [lBodyDict setValue:dataDict forKey:@"data"];
                
                [lBodyDict setValue:@"rpcReq" forKey:@"oid"];
                [lBodyDict setValue:@"id" forKey:@"onBehalfOf"];
                [lBodyDict setValue:@"probe_test_plugin" forKey:@"namespace"];
                [lBodyDict setValue:@"test_probe_valid" forKey:@"call"];
                
                break;
            }
            case SendMessageDeviceResources: {
                [lBodyDict setValue:@"resources" forKey:@"oid"];
                NSMutableArray* dataArray = [NSMutableArray new];
                if ([BMDeviceResources isCameraAvailableWithError:nil]) {
                    NSDictionary* lCameraRes = [BMDeviceResources cameraResolutions];
                    if (lCameraRes) {
                        [dataArray addObject:@{@"rType": @"front-cam",
                                               @"rProperties": lCameraRes [@"front"]}];
                        [dataArray addObject:@{@"rType": @"back-cam",
                                               @"rProperties": lCameraRes [@"back"]}];
                    }
                }
                if ([BMDeviceResources isMicrophoneAvailableWithError:nil]) {
                    [dataArray addObject:@{@"rType": @"mic",
                                           @"rProperties": @""}];
                }
                if ([BMDeviceResources isTouchIDAvailableWithError:nil]) {
                    [dataArray addObject:@{@"rType": @"fp-scanner",
                                           @"rProperties": @"" }];
                }
                if ([BMDeviceResources isLocationServicesAvailableWithError:nil]) {
                    [dataArray addObject:@{@"rType": @"location",
                                           @"rProperties": @"" }];
                }
                [lBodyDict setValue:dataArray forKey:@"data"];
                if ([BMDeviceResources apnsToken])
                    [lBodyDict setValue:[BMDeviceResources apnsToken] forKey:@"push_token"];
                break;
            }
            case SendMessageBye: {
                [lBodyDict setValue:@"bye" forKey:@"oid"];
                break;
            }
            default:
                break;
        }
    }
    
    // make final dictionary with header & message
    [lResultDict setValue:lHeaderDict forKey:@"header"];
    [lResultDict setValue:lBodyDict forKey:@"msg"];
    return lResultDict;
}

- (SocketReceivedMessageType) analyzeReceivedMessage: (NSDictionary*) pRootDict {
    NSString* lReceivedMessageOid;
    BOOL lRegistrationServerHello = NO;
    if (pRootDict[@"header"]){
        NSDictionary *lHeaderDict = [pRootDict objectForKey:@"header"];
        if (lHeaderDict && lHeaderDict.count) {
            if (lHeaderDict[@"token"]){
                token = [lHeaderDict objectForKey:@"token"];
            }
        }
    }
    if (pRootDict[@"msg"]) {
        NSDictionary *lMessageDict = [pRootDict objectForKey:@"msg"];
        if (lMessageDict[@"oid"]) {
            lReceivedMessageOid = [lMessageDict objectForKey:@"oid"];
            
            // TRY message
            if ([lReceivedMessageOid isEqualToString:@"try"])
                [self parseTryMessage:lMessageDict];
            else if ([lReceivedMessageOid isEqualToString:@"bye"]) {
                NSString* byeStatus = [pRootDict objectForKey:@"status"];
                NSMutableDictionary* userInfo = [NSMutableDictionary dictionaryWithDictionary:@{@"status": (byeStatus) ? byeStatus : @""}];
                
                NSString* redirectUrl = [pRootDict objectForKey:@"redirect_url"];
                if (redirectUrl)
                    [userInfo addEntriesFromDictionary:@{@"redirect_url": redirectUrl}];
                
                NSError* error = nil;
                [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].byeMessageDidReceive userInfo:userInfo  error:&error];
                NSLog(@"ANALYZE: ReceivedMessageBye STATUS: %@", byeStatus);
                return ReceivedMessageBye;
            }
            else if ([lReceivedMessageOid isEqualToString:@"probe"]) {
                [self parseProbeMessage:lMessageDict];
            }
        }
        if (lMessageDict[@"fingerprint"]) {
            [BMUserModel updateCurrentUserWithValue:[lMessageDict objectForKey:@"fingerprint" ] ForKey:BIOMIO_APP_ID];
        }
        if (lMessageDict[@"refreshToken"]) {
            refreshToken = [lMessageDict objectForKey:@"refreshToken"];
        }
        if (lMessageDict[@"sessionttl"]) {
            sessionttl = [[lMessageDict objectForKey:@"sessionttl"]doubleValue];
        }
        if (lMessageDict[@"connectionttl"]) {
            connectionttl =  [[lMessageDict objectForKey:@"connectionttl"]doubleValue];
        }
        if (lMessageDict[@"key"]) {
            NSString* privateKey = [lMessageDict objectForKey:@"key"];
            lRegistrationServerHello = YES;
            // Store the private key locally
            if (privateKey) {
                // save privatekey to keychain
                [SAMKeychain setPassword:privateKey forService:BIOMIO_PRIVKEY account:BIOMIO_KEYCHAIN];
                [BMUserModel updateCurrentUserWithValue:privateKey ForKey:BIOMIO_PRIVKEY];
            }
            [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].registrationServerHelloDidReceive userInfo:nil error:nil];
        }
    }
    if ([lReceivedMessageOid isEqualToString:@"serverHello"] && !lRegistrationServerHello) {
        NSError* error = nil;
        [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].regularServerHelloDidReceive userInfo:nil error:&error];
        return ReceivedMessageRegularServerHello;
    }
    else if ([lReceivedMessageOid isEqualToString:@"serverHello"] && lRegistrationServerHello) {
        NSError* error = nil;
        [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].registrationServerHelloDidReceive userInfo:nil error:&error];
        return ReceivedMessageRegistrationServerHello;
    }
    else if ([lReceivedMessageOid isEqualToString:@"nop"]) {
        return ReceivedMessageNop;
    }
    else if ([lReceivedMessageOid isEqualToString:@"getResources"]) {
        return ReceivedMessageGetResources;
    }
    else if ([lReceivedMessageOid isEqualToString:@"rpcResp"]) {
        return ReceivedMessageRPCresponse;
    }
    else if ([lReceivedMessageOid isEqualToString:@"try"]) {
        return ReceivedMessageTry;
    }
    return ReceivedMessageNop;
}

- (SocketReceivedMessageType)parseTryMessage:(NSDictionary*)messageBody {
    TryCondition condition = [messageBody [@"policy"][@"condition"] isEqualToString:@"any"] ? TryConditionAny : TryConditionAll;
    
    NSArray* infoMessageArray;
    if (messageBody [@"message"] && [messageBody [@"message"] isKindOfClass:[NSString class]])
        infoMessageArray = @[messageBody [@"message"]];
    else if (messageBody [@"message"] && [messageBody [@"message"] isKindOfClass:[NSArray class]])
        infoMessageArray = messageBody [@"message"];
    
    BMTryRequestRoot* tryRequest = [BMTryRequestRoot tryRequestWithID:messageBody [@"try_id"] ? messageBody [@"try_id"] : nil
                                                            condition:condition
                                                             messages:infoMessageArray ? infoMessageArray : @[]
                                                              timeout:messageBody [@"authTimeout"] ? [messageBody[@"authTimeout"]integerValue] : 300
                                                        providerTitle:messageBody [@"provider"][@"title"] ? messageBody [@"provider"][@"title"] : nil
                                                      providerLogoURL:messageBody [@"provider"][@"image_url"] ? messageBody [@"provider"][@"image_url"] : nil
                                                             delegate:nil];
    
    if (messageBody[@"resource"])
        [tryRequest addAuthMethods:[messageBody objectForKey:@"resource"]];
    else
        NSLog(@"Warning: try request #%@ doesn't contain authmethods", messageBody[@"try_id"]);
    
    [self.tryRequestHandler addTryRequest:tryRequest];
    
    NSError* error = nil;
    [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].tryMessageDidReceive userInfo:nil error:&error];
    
    return ReceivedMessageTry;
}

- (void)parseProbeMessage:(NSDictionary*)messageBody {
    if ([[messageBody objectForKey:@"probeStatus"] isEqualToString:@"received"]) {
        NSString* tryID = [messageBody objectForKey:@"try_id"];
        NSString* authMethodTitle = [messageBody objectForKey:@"tType"];
        BMTryAuthMethod* authMethod = [TRY_REQUEST_HANDLER authMethodWithTitle:authMethodTitle parrentTryID:tryID];
        [TRY_REQUEST_HANDLER completeAuthMethod:authMethod];
    }
    else if ([[messageBody objectForKey:@"probeStatus"] isEqualToString:@"canceled"]) {
        // TODO: cancel try request by tryID
        NSError* error = nil;
        [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].biometricAuthCanceledByServer userInfo:nil error:&error];
    }
}

#pragma mark - Send methods

- (void) send:(id) pMessage {
    if (webSocket.readyState == SR_OPEN) {
        if ([pMessage isKindOfClass:[NSString class]]) {
            [webSocket send: pMessage];
            sesq+=2;
            [[BMSocketLogger defaultLogger] logMessage:pMessage];
        }
        else if ([pMessage isKindOfClass:[NSMutableDictionary class]] || [pMessage isKindOfClass:[NSDictionary class]]) {
            
            NSData *lJsonData = [NSJSONSerialization dataWithJSONObject:pMessage
                                                                options:NSJSONWritingPrettyPrinted
                                                                  error:NULL];
            if (!lJsonData) {
                NSLog(@"Error composing a message");
            }
            else {
                NSString* lMessageJsonString = [[NSString alloc] initWithData:lJsonData encoding:NSUTF8StringEncoding];
                [webSocket send:lMessageJsonString];
                sesq+=2;
                [[BMSocketLogger defaultLogger] logMessage:pMessage];
            }
        }
    } else {
        NSLog(@"Sending message failed. Invalid State: Cannot call send: until connection is open");
    }
}

- (void) sendTokenRefreshMessage:(NSTimer*)sender {
    NSLog(@"SENDING NOP TOKEN REFRESH MESSAGE");
    [self send:[self composeMessageOfType:SendMessageNopTokenRefresh]];
}

- (void) sendNopMessage:(NSTimer*)sender {
    NSLog(@"SENDING NOP MESSAGE");
    [self send:[self composeMessageOfType:SendMessageNop]];
}

- (void) sendRPC {
    [self send: [self composeMessageOfType:SendMessageRPCRequest]];
}

- (void) sendDeviceResourcesMessage:(NSTimer*)sender {
    [self send:[self composeMessageOfType:SendMessageDeviceResources]];
}

/**
 *  Sends a message {oid = resources} with all the available device resourses: camera, mic, Touch ID, location service etc.
 */
- (void) sendDeviceResourcesMessage {
    [self send:[self composeMessageOfType:SendMessageDeviceResources]];
}

- (void) sendProbeMessageWithAuthMethod: (BMTryAuthMethod*) pAuthMethod
                             authResult: (NSString*) pResult
                                 status: (BMProbeStatus) pStatus
                                  error: (NSError*) pError{
    // before sending probe message checking remaining session timeout to avoid situations when sending heavy authsamples (photos) blocks sending nop-messages causing bye=session expired
    [self checkRemainingSessionTtl];
    NSMutableDictionary* lBodyDict = [NSMutableDictionary new];
    NSMutableDictionary* lHeaderDict = [NSMutableDictionary new];
    NSMutableDictionary* lResultDict = [NSMutableDictionary new];
    
    //    message header
    lHeaderDict = [self composeMessageHeader];
    
    [lBodyDict setValue:@"probe" forKey:@"oid"];
    [lBodyDict setValue:pAuthMethod.title forKey:@"tType"];
    [lBodyDict setValue:pAuthMethod.parrentTryID forKey:@"try_id"];
    
    [lBodyDict setValue:[BMProbeStatusEnum probeStatusToString:pStatus]
                 forKey:@"probeStatus"];
    // status = success
    if (pStatus == BMProbeStatusSuccess) {
        NSMutableDictionary* lProbeDataDict = [NSMutableDictionary new];
        [lProbeDataDict setValue:pResult ? @[pResult] : pAuthMethod.authSamples forKey:@"samples"];
        [lProbeDataDict setValue:[self sampleTitleForAuthmethod:pAuthMethod] forKey:@"oid"];
        [lBodyDict setValue:lProbeDataDict forKey:@"probeData"];
    }
    // status = failed
    else if (pStatus == BMProbeStatusFailed) {
        [pAuthMethod didFailWithError:pError];
        [[BMSocketLogger defaultLogger] logError:pError];
        if ([self.delegate respondsToSelector:@selector(socketDidFailWithError:)])
            [self.delegate socketDidFailWithError:pError];
    }
    //        else if (pStatus == BMProbeStatusCanceled) {
    //            Probe message with status "canceled" shouldn't contain authresults, therefore no need to add them
    //        }
    
    // make final dictionary with header & message
    [lResultDict setValue:lHeaderDict forKey:@"header"];
    [lResultDict setValue:lBodyDict forKey:@"msg"];
    if (lResultDict) {
        [self beginBackgroundProbeUpload];
        if ([[NSUserDefaults standardUserDefaults]boolForKey:@"SEND_PROBE_MESSAGES"])
            [self send:lResultDict];
        [self forceUpdateStateVisualization];
    }
    else
        NSLog(@"Error composing a message");
}

- (NSString*) sampleTitleForAuthmethod:(BMTryAuthMethod*) authMethod {
    NSDictionary* sampleTitles = @{@"fp"            : @"touchIdSamples",
                                   @"palm"          : @"imageSamples",
                                   @"face"          : @"imageSamples",
                                   @"location"      : @"locationSamples",
                                   @"pin_code"      : @"pincodeSamples",
                                   @"push_button"   : @"pushbuttonSamples",
                                   @"credit_card"   : @"creditCardSamples"};
    NSString* sampleTitle = sampleTitles[authMethod.title];
    return  sampleTitle ? sampleTitle : @"";
}

- (BOOL) checkRemainingSessionTtl {
    NSTimeInterval timeRemaining = tokenRefreshTimer.fireDate.timeIntervalSinceNow;
    if (timeRemaining < sessionttl/2) {
        [tokenRefreshTimer invalidate];
        [self sendTokenRefreshMessage:nil];
        tokenRefreshTimer = [NSTimer scheduledTimerWithTimeInterval:sessionttl-TTL_CALIBRATION_TIME
                                                             target:self
                                                           selector:@selector(sendTokenRefreshMessage:)
                                                           userInfo:nil
                                                            repeats:YES];
        tokenRefreshTimer.tolerance = sessionttl*0.1;
        return YES;
    }
    return NO;
}

/**
 BiomioID request.
 
 @param requestUrl target URL for POST request
 @param completion completionHandler
 */
- (void) sendIdentificationRequestToURL: (NSURL*) requestUrl
                        completionBlock: (void(^)(NSHTTPURLResponse* response, NSError* error)) completion {
    // Create the Request
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:requestUrl];
    [request setHTTPMethod:@"POST"];
    NSString* paramsString = [NSString stringWithFormat:@"app_id=%@",[BMUserModel currentUser].appID];
    [request setHTTPBody:[paramsString dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Configure the NSURL Session
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration]; //backgroundSessionConfigurationWithIdentifier:@"com.upload"];
    config.HTTPMaximumConnectionsPerHost = 1;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:nil delegateQueue:nil];
    
    // Define the Upload task
    NSURLSessionDataTask *uploadTask = [session uploadTaskWithRequest:request
                                                             fromData:nil
                                                    completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*) response;
                                                        if (completion)
                                                            completion(httpResponse, error);
                                                    }];
    [uploadTask resume];
}

#pragma mark - Background Upload

- (void) beginBackgroundProbeUpload {
    backgroundUploadID = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [self endBackgroundProbeUpload];
    }];
}

- (void) endBackgroundProbeUpload {
    //    if (![self isBusyUploadingProbe]) {
    //        UILocalNotification* lNotification = [[UILocalNotification alloc] init];
    //        lNotification.fireDate = [NSDate date];
    //        lNotification.alertBody = @"Background upload completed";
    //        lNotification.soundName =  UILocalNotificationDefaultSoundName;
    //        [[UIApplication sharedApplication] scheduleLocalNotification:lNotification];
    //    }
    UIApplicationState state = [UIApplication sharedApplication].applicationState;
    BOOL backgroundState = (state == UIApplicationStateBackground);
    
    if (backgroundState) {
        NSError* error = nil;
        [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].socketWillClose userInfo:nil error:&error];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] endBackgroundTask: backgroundUploadID];
        backgroundUploadID = UIBackgroundTaskInvalid;
    });
}

#pragma mark - SocketRocket delegates

- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
    NSLog(@"Websocket Connected to %@",[BMSocketHandler serverUrl]);
    NSError* error;
    [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidOpen userInfo:nil error:&error];
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    [socketRespondingTimer invalidate];
    socketRespondingTimer = [NSTimer scheduledTimerWithTimeInterval:CONNECTION_NOT_RESPONDING_PERIOD
                                                             target:self
                                                           selector:@selector(checkSocketResponse)
                                                           userInfo:nil
                                                            repeats:NO];
    
    [[BMSocketLogger defaultLogger] logMessage:message];
    
    NSError *jsonError;
    NSData *objectData = [message dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *lDict = [NSJSONSerialization JSONObjectWithData:objectData
                                                          options:NSJSONReadingMutableContainers
                                                            error:&jsonError];
    
    SocketReceivedMessageType receivedMessageType = [self analyzeReceivedMessage:lDict];
    if (receivedMessageType ==  ReceivedMessageGetResources)
        [self send:[self composeMessageOfType:SendMessageDeviceResources]];
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    // Stream end encountered code		1001
    //    NSError* error = nil;
    [self restartWithUserReset:NO];
}

- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload {
    NSLog(@"Websocket received pong");
}

- (void) webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    [[BMSocketLogger defaultLogger] logError:error];
    NSError* eventError = nil;
    switch (error.code) {
        case 2: {
            // no internet // The operation couldn't be completed
            [[BMSocketLogger defaultLogger] logError:[BMErrorHandler errorWithCode:13005]];
            [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidFail userInfo:nil error:&eventError];
            break;
        }
        case 61: {
            // server is down
            [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidFail userInfo:nil error:&eventError];
        }
        case 57: {
            // internet gone off/ wifi -> low energy wifi
            // TODO: CHECK INTERNET CONNECTION ALERT
            [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidFail userInfo:@{@"reconnect_timeout" : @10} error:&eventError];
        }
        case 2145: {
            // server is down
            [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidFail userInfo:@{@"reconnect_timeout" : @10} error:&eventError];
            //            [self restartWithUserReset:NO];
        }
        default: {
            [[BMStateMachine sharedHandler] fireEvent:[BMStateMachine sharedHandler].connectionDidFail userInfo:nil error:&eventError];
            break;
        }
    }
}

#pragma mark - Digest Signing Methods

// OPENSSL
/**
 *  @brief VALID METHOD FOR DIGEST SIGNATURE.
 *  @discussion Creates digest signature with string key. converts base64 private key to RSA key. uses OpenSSL
 *  @discussion Digest validation: http://kjur.github.io/jsrsasign/sample-rsasign.html
 *
 *  @param pTextString    String to sign
 *  @param pPrivateKey    Base64 private key
 *
 *  @return NSString* signed digest in hex format
 */
- (NSString*) signHeader:(NSString*) pTextString withPrivateKey: (NSString*) pPrivateKey {
    // partially code from http://sehermitage.web.fc2.com/program/src/rsacrypt.c
    // Roman Slyepko's (my :) answer on stackoverflow http://stackoverflow.com/questions/19182158/ios-sign-a-string-with-an-rsa-private-key/27945240#27945240
    int retEr;
    char* text = (char*) [pTextString UTF8String];
    unsigned char *data;
    unsigned int dataLen;
    
    // converting nsstring base64 private key to openssl RSA key
    
    BIO *mem = NULL;
    RSA *rsa_private = NULL;
    char *private_key = (char*)[pPrivateKey UTF8String];
    
    mem = BIO_new_mem_buf(private_key, (int)strlen(private_key));
    if (mem == NULL)
    {
        char buffer[120];
        ERR_error_string(ERR_get_error(), buffer);
        fprintf(stderr, "OpenSSL error: %s", buffer);
        exit(0);
    }
    
    rsa_private = PEM_read_bio_RSAPrivateKey(mem, NULL, NULL, NULL);
    BIO_free (mem);
    if (rsa_private == NULL)
    {
        char buffer[120];
        ERR_error_string(ERR_get_error(), buffer);
        fprintf(stderr, "OpenSSL error: %s", buffer);
        exit(0);
    }
    // end of convertion
    
    data = (unsigned char *) text;
    dataLen = (unsigned int) strlen(text);
    
    //// creating signature
    // sha1
    unsigned char hash[SHA_DIGEST_LENGTH];
    unsigned char sign[128];
    unsigned int signLen;
    
    SHA1(data, dataLen, hash);
    
    //  signing
    retEr = RSA_sign(NID_sha1, hash, SHA_DIGEST_LENGTH, sign, &signLen, rsa_private);
    
    //  printf("Signature len gth = %d\n", signLen);
    printf("%s", (retEr == 1) ? "" : "RSA_sign error");
    
    //  convert unsigned char -> std:string
    std::stringstream buffer;
    for (int i = 0; i < 128; i++)
    {
        buffer << std::hex << std::setfill('0');
        buffer << std::setw(2)  << static_cast<unsigned>(sign[i]);
    }
    std::string signature = buffer.str();
    
    //  convert std:string -> nsstring
    NSString *signedMessage = [NSString stringWithCString:signature.c_str() encoding:[NSString defaultCStringEncoding]];
    
    RSA_free(rsa_private);
    
    return signedMessage;
}

/**
 *  Method for digest signature by using .p12 certificate. Uses getPrivateKeyRef method to retrieve privateKey from a certicate
 *
 *  @param dataString string to sign
 *  @param key        SecKeyRef to a .p12 cert
 *
 *  @return signed string-digest
 */
-(NSString *)signing:(NSString *)dataString usingSecKeyRefofPrivKey:(SecKeyRef) key {
    //    SecKeyRef PrivKey = [self getPrivateKeyRef];
    //    NSString* digest = [self signing:lHeaderString usingSecKeyRefofPrivKey:PrivKey];
    
    //    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"rsaPrivate" ofType:@"pem"];
    //    NSData *data = [[NSData alloc]initWithContentsOfFile:filePath];
    
    SecKeyRef privateKey = key; //(__bridge SecKeyRef)(data);
    
    uint8_t *signedHashBytes = NULL;
    // calculate private key size
    size_t signedHashBytesSize = SecKeyGetBlockSize(privateKey);
    
    // create space to put signature
    signedHashBytes = (uint8_t *)malloc(signedHashBytesSize * sizeof(uint8_t));
    memset((void *)signedHashBytes, 0x0, signedHashBytesSize);
    
    OSStatus status = NULL;
    
    const uint8_t  *dataToSign = (const uint8_t*) [[self CC_SHA1:[dataString dataUsingEncoding:NSUTF8StringEncoding]] bytes];
    
    // sign data
    status = SecKeyRawSign(privateKey,
                           kSecPaddingPKCS1SHA1,
                           dataToSign,
                           CC_SHA1_DIGEST_LENGTH,
                           signedHashBytes,
                           &signedHashBytesSize);
    
    if (privateKey) {
        CFRelease(privateKey);
    }
    
    // get signature hash
    NSData *signedHash = [NSData dataWithBytes:(const void *)signedHashBytes length:(NSUInteger)signedHashBytesSize];
    
    // release created space
    if (signedHashBytes) {
        free(signedHashBytes);
    }
    
    if (status != errSecSuccess) {
        return @"Digest signing error";
    }
    
    // return hex encoded signature string
    return [signedHash hexadecimalString];
}

/**
 *  Extracts a SecKeyRef with a private key from .p12 certificate. Used in signing:usingSecKeyRefofPrivKey:
 *
 *  @return SecKeyRef with a privateKey
 */
- (SecKeyRef) getPrivateKeyRef {
    NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"rsaPrivate" ofType:@"p12"];
    NSData *p12Data = [NSData dataWithContentsOfFile:resourcePath];
    
    NSMutableDictionary * options = [[NSMutableDictionary alloc] init];
    
    SecKeyRef privateKeyRef = NULL;
    
    // password for private key certificate
    [options setObject:@"1234" forKey:(__bridge id)kSecImportExportPassphrase];
    
    CFArrayRef items = CFArrayCreate(NULL, 0, 0, NULL);
    
    OSStatus securityError = SecPKCS12Import((__bridge CFDataRef) p12Data,
                                             (__bridge CFDictionaryRef)options, &items);
    
    if (securityError == noErr && CFArrayGetCount(items) > 0) {
        CFDictionaryRef identityDict = (CFDictionaryRef) CFArrayGetValueAtIndex(items, 0);
        SecIdentityRef identityApp = (SecIdentityRef)CFDictionaryGetValue(identityDict, kSecImportItemIdentity);
        
        securityError = SecIdentityCopyPrivateKey(identityApp, &privateKeyRef);
        if (securityError != noErr) {
            privateKeyRef = NULL;
        }
    }
    CFRelease(items);
    return privateKeyRef;
}

/**
 *  Extracts a SecKeyRef with a public key from .der certificate. Used in signing:usingSecKeyRefofPrivKey:
 *
 *  @return SecKeyRef with a public key.
 */
- (SecKeyRef) getPublicKeyRef {
    NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"rsaCert" ofType:@"der"];
    NSData *certData = [NSData dataWithContentsOfFile:resourcePath];
    SecCertificateRef cert = SecCertificateCreateWithData(NULL, (__bridge CFDataRef)certData);
    SecKeyRef key = NULL;
    SecTrustRef trust = NULL;
    SecPolicyRef policy = NULL;
    
    if (cert != NULL) {
        policy = SecPolicyCreateBasicX509();
        if (policy) {
            if (SecTrustCreateWithCertificates((CFTypeRef)cert, policy, &trust) == noErr) {
                SecTrustResultType result;
                if (SecTrustEvaluate(trust, &result) == noErr) {
                    key = SecTrustCopyPublicKey(trust);
                }
            }
        }
    }
    if (policy) CFRelease(policy);
    if (trust) CFRelease(trust);
    if (cert) CFRelease(cert);
    return key;
}

/**
 *  @brief Cryptographic hash function.
 *  @discussion SHA-1 produces a 160-bit (20-byte) hash value known as a message digest. A SHA-1 hash value is typically rendered as a hexadecimal number, 40 digits long
 *
 *  @param plainText Text data
 *
 *  @return Message digest
 */
- (NSData *) CC_SHA1:(NSData *)plainText {
    CC_SHA1_CTX ctx;
    uint8_t * hashBytes = NULL;
    NSData * hash = nil;
    hashBytes = (uint8_t *) malloc( CC_SHA1_DIGEST_LENGTH * sizeof(uint8_t) );
    memset((void *)hashBytes, 0x0, CC_SHA1_DIGEST_LENGTH);
    CC_SHA1_Init(&ctx);
    CC_SHA1_Update(&ctx, (void *)[plainText bytes], (CC_LONG) [plainText length]);
    CC_SHA1_Final(hashBytes, &ctx);
    hash = [NSData dataWithBytes:(const void *)hashBytes length:(NSUInteger)CC_SHA1_DIGEST_LENGTH];
    if (hashBytes)
        free(hashBytes);
    return hash;
}

- (NSData*) encryptString:(NSString *)plainText withX509Certificate:(NSData *)certificate {
    
    NSString *privKeyCertPath = [[NSBundle mainBundle] pathForResource:@"privateKey" ofType:@"cer"];
    NSData *privKeyCertData =  [NSData dataWithContentsOfFile:privKeyCertPath];
    
    SecCertificateRef cert = SecCertificateCreateWithData(kCFAllocatorDefault, (__bridge CFDataRef)privKeyCertData);
    SecPolicyRef policy = SecPolicyCreateBasicX509();
    SecTrustRef trust;
    OSStatus status = SecTrustCreateWithCertificates(cert, policy, &trust);
    
    SecTrustResultType trustResult;
    if (status == noErr) {
        status = SecTrustEvaluate(trust, &trustResult);
    }
    
    SecKeyRef publicKey = SecTrustCopyPublicKey(trust);
    
    const char *plain_text = [plainText UTF8String];
    size_t blockSize = SecKeyGetBlockSize(publicKey);
    NSMutableData *collectedCipherData = [NSMutableData data];
    
    BOOL success = YES;
    size_t cipherBufferSize = blockSize;
    uint8_t *cipherBuffer = (uint8_t*) malloc(blockSize);
    
    int i;
    for (i = 0; i < strlen(plain_text); i += blockSize-11) {
        int j;
        for (j = 0; j < blockSize-11 && plain_text[i+j] != '\0'; ++j) {
            cipherBuffer[j] = plain_text[i+j];
        }
        
        int result;
        if ((result = SecKeyEncrypt(publicKey, kSecPaddingPKCS1, cipherBuffer, j, cipherBuffer, &cipherBufferSize)) == errSecSuccess) {
            [collectedCipherData appendBytes:cipherBuffer length:cipherBufferSize];
        } else {
            success = NO;
            break;
        }
    }
    
    /* Free the Security Framework Five! */
    CFRelease(cert);
    CFRelease(policy);
    CFRelease(trust);
    CFRelease(publicKey);
    free(cipherBuffer);
    
    if (!success)
        return nil;
    
    return [NSData dataWithData:collectedCipherData];
}

/*
 @discussion
 Updating PKCS12 Certificates using 'openssl' commands
 http://www.wohmart.com/ircd/pub/hybrid/2-Patchsets/LBNLSecureMessaging/certfmt.txt
 
 */

@end
