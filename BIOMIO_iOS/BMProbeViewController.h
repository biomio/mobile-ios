//
//  BMProbeViewController.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 29.08.15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
// view controllers
#import "BMHomeViewController.h"

@interface BMProbeViewController : BMHomeViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UINavigationItem *navBarTitle;
@property (weak, nonatomic) IBOutlet UICollectionView *probeResourcesCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *nonInteractiveAuthMethodsCollectionView;

@property (weak, nonatomic) IBOutlet UIView *probeMainView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *navBarBackButton;

@property (nonatomic, assign) NSUInteger anyAuthMethodIndex;
@end
