//
//  GUTabBarController.h
//
//
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GUTabBarView.h"

@interface GUTabBarController : UITabBarController <GUTabBarDelegate>

@end
