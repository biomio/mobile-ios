//
//  BMHomeViewController.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 8/27/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
// handlers
#import "BMSocketHandler.h"

@class RSProgressBar;
@interface BMHomeViewController : UIViewController <BMSocketDelegate>

@property (nonatomic, strong) RSProgressBar* socketStateBar;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@end
