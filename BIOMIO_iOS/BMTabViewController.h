//
//  BMTabViewController.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 9/17/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
// view controllers
#import "GUTabBarController.h"
@interface BMTabViewController : UITabBarController <GUTabBarDelegate, UITabBarControllerDelegate>
@property (nonatomic, strong) GUTabBarView* customTabBar;

- (void) refreshTabBar;

@end