//
//  BMHomeViewController.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 8/27/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

// view controllers
#import "BMHomeViewController.h"
#import "BMProbeViewController.h"
#import "BMRegistrationViewController.h"
#import "BMTabViewController.h"
// views
#import "RSProgressBar.h"
// handlers
#import "BMStateMachine.h"
#import "BMUserModel.h"

@interface BMHomeViewController ()
@end

@implementation BMHomeViewController


#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    // status bar background color
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, 20)];
    view.backgroundColor=[UIColor colorWithRed:0.396 green:0.416 blue:0.498 alpha:1.000];
    [self.view addSubview:view];
    
    // SocketState progressBar
    CGFloat lSocketBarPosY = self.navigationController.navigationBar.bounds.size.height + [UIApplication sharedApplication].statusBarFrame.size.height;
    self.socketStateBar = [[RSProgressBar alloc] initWithFrame:CGRectMake(0, lSocketBarPosY, [UIScreen mainScreen].bounds.size.width, 5)];
    [self.view addSubview:self.socketStateBar];
    
    [self initStateMachineStates];
}

- (void)viewWillLayoutSubviews {
    // Your adjustments accd to
    // viewController.bounds
    
    [super viewWillLayoutSubviews];
    CGFloat lSocketBarPosY = self.navigationController.navigationBar.bounds.size.height + [UIApplication sharedApplication].statusBarFrame.size.height;
    if (lSocketBarPosY == 84)
        lSocketBarPosY = 64;
    self.socketStateBar.frame = CGRectMake(0, lSocketBarPosY, [UIScreen mainScreen].bounds.size.width, 5);
    BMTabViewController* tabbar = (BMTabViewController*) self.tabBarController;
    [tabbar.customTabBar setNeedsLayout];
}

- (void) viewWillAppear:(BOOL)animated {
    [BMSocketHandler sharedSocket].delegate = self;
    [SOCKET_HANDLER forceUpdateStateVisualization];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initStateMachineStates {
    // TODO: remove direct access to BMStateMachine
    // biometricAuthState      DidExit
    __weak typeof(self) weakSelf = self.navigationController.viewControllers.firstObject;
    [[BMStateMachine sharedHandler].biometricAuthState setDidExitStateBlock:^(TKState *state, TKTransition *transition) {
        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.navigationController popToRootViewControllerAnimated:NO];
        });
    }];
}

#pragma mark - UI

- (void) setSocketStateBarState: (RSProgressBarState) pState
                          color: (UIColor*) pColor {
    [self.socketStateBar setBarColor:pColor];
    [self.socketStateBar setProgressBarState:pState];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Socket Delegates Methods

- (void)socketDidReceiveTryRequest {
    BMProbeViewController *lProbeViewController = [[BMProbeViewController alloc]initWithNibName:@"BMProbeViewController" bundle:nil];
    [self.navigationController pushViewController:lProbeViewController animated:NO];
}

- (void)socketRequiresRegistration {
    if (![BMUserModel isRegistrationRequired]) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:BIOMIO_USER];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"registrationSuccess"];
    }
    [self.navigationController pushViewController:[[BMRegistrationViewController alloc]initWithNibName:@"BMRegistrationViewController" bundle:nil] animated:NO];
}

- (void)socketDidOpenUrlRegistrationCode {
    UIAlertController* lAlertCtrl = [UIAlertController alertControllerWithTitle:@"Register device"
                                                                        message:@"Log out and register this device again?"
                                                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesLogOutAction = [UIAlertAction actionWithTitle:@"Yes, log out"
                                                              style:UIAlertActionStyleDestructive
                                                            handler:^(UIAlertAction * _Nonnull action) {
                                                                [SOCKET_HANDLER restartWithUserReset:YES];
                                                            }];
    UIAlertAction* noCancelAction = [UIAlertAction actionWithTitle:@"No"
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               // continue with regular handshake
                                                               [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"BIOMIO_registration_secret_URL"];
                                                           }];
    [lAlertCtrl addAction:yesLogOutAction];
    [lAlertCtrl addAction:noCancelAction];
    [self presentViewController:lAlertCtrl animated:YES completion:nil];
}

- (void)socketDidChangeState:(BMSocketState)state
                   uploading:(BOOL)busy {
    RSProgressBarState progressBarState = busy ? RSProgressBarStateRefreshing : RSProgressBarStateStatic;
    switch (state) {
        case BMSocketStateDisconnected:
            [self setSocketStateBarState:progressBarState color:COLOR_RED];
            break;
        case BMSocketStateConnection:
            [self setSocketStateBarState:progressBarState color:COLOR_ORANGE];
            break;
        case BMSocketStateStandbye:
            [self setSocketStateBarState:progressBarState color:COLOR_GREEN];
            break;
        default:
            [self setSocketStateBarState:progressBarState color:nil];
            break;
    }
}

@end
