//
//  BMTryRequestRoot.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 5/18/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import "BMTryRequestRoot.h"
#import "BMTryAuthMethod.h"

@interface BMTryRequestRoot ()
@property (nonatomic, strong, readwrite) NSString* identifier;
@property (nonatomic, readwrite) TryCondition condition;
@property (nonatomic, readwrite) NSInteger timeout;

@property (nonatomic, strong, readwrite) NSString* providerTitle;
@property (nonatomic, strong, readwrite) NSString* providerLogoURL;

@property (nonatomic, strong, readwrite) NSTimer* authTimeoutTimer;

@property (nonatomic, strong, readwrite) NSArray* interactiveAuthMethods;
@property (nonatomic, strong, readwrite) NSArray* nonInteractiveAuthMethods;

@end

@implementation BMTryRequestRoot
@synthesize status = _status;

+ (instancetype) tryRequestWithID:(NSString*) identifier
                        condition:(TryCondition) condition
                         messages:(NSArray*) messages
                          timeout:(NSInteger) timeout
                    providerTitle:(NSString*) providerTitle
                  providerLogoURL:(NSString*) providerLogoUrl
                         delegate:(id<TryRequestDelegate>) delegateObj {
    BMTryRequestRoot* tryRequest = [BMTryRequestRoot new];
    
    tryRequest.identifier = identifier;
    tryRequest.condition = condition;
    tryRequest.messages = messages;
    tryRequest.timeout = timeout;
    tryRequest.authTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:timeout
                                                                   target:tryRequest
                                                                 selector:@selector(tryRequestDidTimeout:)
                                                                 userInfo:nil
                                                                  repeats:NO];
    tryRequest.authTimeoutTimer.tolerance = 0.05;
    tryRequest.delegate = delegateObj;
    
    return tryRequest;
}

- (void) addAuthMethods:(NSArray*) authMethods {
    if (authMethods && authMethods.count) {
        NSMutableArray* methods = [NSMutableArray new];
        NSMutableArray* interactiveMethods = [NSMutableArray new];
        NSMutableArray* nonInteractiveMethods = [NSMutableArray new];
        
        [authMethods enumerateObjectsUsingBlock:^(NSDictionary* resDict, NSUInteger idx, BOOL *stop) {
            // FIXME: Hardcoded userInteractive check (gate not supporting)
            BMTryAuthMethod* authMethod = [BMTryAuthMethod authMethodWithTitle:resDict [@"tType"]
                                                                       samples:[resDict [@"samples"] unsignedIntegerValue]
                                                               userInteractive:![resDict [@"tType"] isEqualToString:@"location"] // [resDict [@"interactive"] isEqualToNumber: [NSNumber numberWithBool:NO]] ? NO : YES
                                                                       atIndex:idx
                                                                        device:resDict [@"resource"][@"rType"]
                                                                deviceSettings:resDict [@"resource"][@"rProperties"]
                                                             childOfTryRequest:self.identifier];
            
            [methods setObject:authMethod atIndexedSubscript:idx];
            if (authMethod.userInteractive)
            [interactiveMethods addObject:authMethod];
            else
            [nonInteractiveMethods addObject:authMethod];
        }];
        self.authMethods = methods;
        self.interactiveAuthMethods = interactiveMethods;
        self.nonInteractiveAuthMethods = nonInteractiveMethods;
    } else {
        // TODO: log error
        self.authMethods = @[];
        self.interactiveAuthMethods = @[];
        self.nonInteractiveAuthMethods = @[];
    }
}

- (NSString *)description {
    NSMutableString* authmethods = [NSMutableString stringWithString:@""];
    for (BMTryAuthMethod* authMethod in self.authMethods) {
        [authmethods appendString:[NSString stringWithFormat:@"\n%@", [authMethod description]]];
    }
    return [NSString stringWithFormat:@"TryRequest ID: %@\t type: %@ \tCondition: %ld\tTimeoutRemaining: %f/%li \nStatus: %li \tAuthMethodsCompleted: %li/%li \tCompleted: %@ \nDelegate: %@ \nAuthMethods:%@ ",
            self.identifier,
            self.messages,
            (long)self.condition,
            self.authTimeoutTimer.fireDate.timeIntervalSinceNow,(long)self.timeout,
            (long)self.status,
            (long)self.authMethodsCompleted, (long)self.authMethodsRequested,
            self.completed ? @"YES" : @"NO",
            self.delegate,
            authmethods
            ];
}

#pragma mark - Messages

- (NSString *)tryRequestType {
    return [self.messages count] ? self.messages[0] : nil;
}

- (NSString *)tryDescription {
    return [self.messages count] >= 2 ? self.messages[1] : nil;
}

- (NSString *)startButtonTitle {
    return [self.messages count] >= 3 ? self.messages[2] : @"Start";
}

#pragma mark - Parrent status

- (BOOL)recoverToClean {
    for (BMTryAuthMethod* authMethod in self.interactiveAuthMethods) {
        authMethod.status = AuthMethodStatusClean;
    }
    self.selectedAuthMethod = @[];
    return YES;
}

- (void) cancel {
    [self setStatusForEveryAuthMethod:AuthMethodStatusSkipped];
    self.status = TryRequestStatusCanceled;
    [self status]; // do the recursive getter for canceled
}

- (void) setStatusForEveryAuthMethod:(AuthMethodStatus) status {
    for (BMTryAuthMethod* authMethod in self.authMethods)
    authMethod.status = status;
}

#pragma mark - Timeout

- (void) tryRequestDidTimeout: (NSTimer*) sender {
    if ([self.delegate respondsToSelector:@selector(tryRequestDidExpire:)])
    [self.delegate tryRequestDidExpire:self];
    
    for (BMTryAuthMethod* authMethod in self.authMethods) {
        authMethod.status = AuthMethodStatusExpired;
    }
}

#pragma mark Status

- (TryRequestStatus)status {
    if (_status != TryRequestStatusCanceled ||
        _status != TryRequestStatusExpired) {
        NSInteger authMethodQnty = [self.interactiveAuthMethods count];
        NSInteger clean = 0;            //AuthMethodStatusClean
        NSInteger sent = 0;             //AuthMethodStatusSent
        NSInteger completed = 0;        //AuthMethodStatusCompleted
        NSInteger skipped = 0;          //AuthMethodStatusSkipped
        NSInteger failed = 0;           //AuthMethodStatusFailed
        
        // non interactive
        NSInteger nonIntAuthMethodQnty = [self.nonInteractiveAuthMethods count];
        NSInteger nonIntClean = 0;            //AuthMethodStatusClean
        NSInteger nonIntSent = 0;             //AuthMethodStatusSent
        NSInteger nonIntCompleted = 0;        //AuthMethodStatusCompleted
        NSInteger nonIntSkipped = 0;          //AuthMethodStatusSkipped
        NSInteger nonIntFailed = 0;           //AuthMethodStatusFailed
        
        
        for (BMTryAuthMethod* authMethod in self.authMethods) {
            switch (authMethod.status) {
                case AuthMethodStatusActive:
                    self.status =  TryRequestStatusActive;
                    return TryRequestStatusActive;
                case AuthMethodStatusCompleted:
                    authMethod.userInteractive ? completed++ : nonIntCompleted++;
                    break;
                case AuthMethodStatusFailed:
                    authMethod.userInteractive ? failed++ : nonIntFailed++;
                    break;
                case AuthMethodStatusSkipped:
                    authMethod.userInteractive ? skipped++ : nonIntSkipped++;
                    break;
                case AuthMethodStatusSent:
                    authMethod.userInteractive ? sent++ : nonIntSent++;
                    break;
                case AuthMethodStatusClean:
                    authMethod.userInteractive ? clean++ : nonIntClean++;
                    break;
                    
                default:
                    break;
            }
        }
        
        if (clean == authMethodQnty+nonIntAuthMethodQnty) {
            self.status =  TryRequestStatusClean;
            return TryRequestStatusClean;
        }
        else if (skipped == authMethodQnty) {
            self.status =  TryRequestStatusCanceled;
        return TryRequestStatusCanceled; // smth wrong
        }
        else if (self.condition == TryConditionAll) {
            if (sent == authMethodQnty && nonIntSent == nonIntAuthMethodQnty) {
                self.status =  TryRequestStatusSent;
                return TryRequestStatusSent;
            } else if (completed == authMethodQnty && nonIntCompleted == nonIntAuthMethodQnty) {
                self.status =  TryRequestStatusCompleted;
                return TryRequestStatusCompleted;
            }
        } else if (self.condition == TryConditionAny) {
            if (sent == 1 &&
                skipped == authMethodQnty-1 &&
                nonIntSent == nonIntAuthMethodQnty) {
                self.status =  TryRequestStatusSent;
                return TryRequestStatusSent;
            } else if (completed+failed == 1 &&
                       skipped == authMethodQnty-1 &&
                       nonIntCompleted+nonIntFailed == nonIntAuthMethodQnty) {
                self.status =  TryRequestStatusCompleted;
                return TryRequestStatusCompleted;
            } else if (clean == 1 &&
                       skipped == authMethodQnty-1) {
                self.status =  TryRequestStatusActive;
                return TryRequestStatusActive;
            }
        }
        
        
        
        
        
        //        else if (self.condition == TryConditionAll &&
        //                 sent+completedFailed >= 1 &&
        //                 clean == authMethodQnty-sent-completedFailed)
        //        return TryRequestStatusActive;
        //        else if (self.condition == TryConditionAny &&
        //                 skipped == [self.interactiveAuthMethods count]-1) {
        //            if ([self.nonInteractiveAuthMethods count] && completedFailed == [self.nonInteractiveAuthMethods count] + ([self.interactiveAuthMethods count] ? 1 : 0)) {
        //                return TryRequestStatusCompleted;
        //            }
        //            else if (sent == 1)
        //            return TryRequestStatusSent;
        //            else if (completedFailed == 1)
        //            return TryRequestStatusCompleted;
        //            else if (clean == 1)
        //            return TryRequestStatusActive;
        //        } else if (self.condition == TryConditionAll) {
        //            if (sent >= 1 && completedFailed == authMethodQnty-sent)
        //            return TryRequestStatusSent;
        //        }
        //        else if ([self.nonInteractiveAuthMethods count] && completedFailed >0 && [self.selectedAuthMethod count] == 0) {
        //            return TryRequestStatusActive;
        //        }
        else {
            NSLog(@"WRONG STATUS");
            return _status;
        }
    }
    else {
        NSAssert(_status == TryRequestStatusCanceled || _status == TryRequestStatusExpired, @"TryRequestRoot: status");
        return _status;
    }
    return _status;
}

- (void)setStatus:(TryRequestStatus)status {
    _status = status;
}

- (BOOL) completed {
    TryRequestStatus status = self.status;
    if (status == TryRequestStatusClean ||
        status == TryRequestStatusActive)
    return NO;
    return YES;
}

#pragma mark - AuthMethods

- (NSInteger) authMethodsRequested {
    return [self.authMethods count];
}

- (NSInteger) authMethodsCompleted {
    __block NSInteger lCompleted = 0;
    [self.authMethods enumerateObjectsUsingBlock:^(BMTryAuthMethod *obj, NSUInteger idx, BOOL *stop) {
        if (obj.status != AuthMethodStatusClean && obj.status != AuthMethodStatusActive)
        lCompleted++;
    }];
    return lCompleted;
}

- (BMTryAuthMethod*) activeInteractiveAuthMethod {
    for (BMTryAuthMethod* authMethod in self.interactiveAuthMethods) {
        if (authMethod.status == AuthMethodStatusActive)
        return authMethod;
    }
    return nil;
}

- (void)setSelectedAuthMethod:(NSArray *)selectedAuthMethod {
    _selectedAuthMethod = selectedAuthMethod;
    if ([selectedAuthMethod count]) {
        for (BMTryAuthMethod* authMethod in self.interactiveAuthMethods) {
            if (![selectedAuthMethod containsObject:authMethod])
            authMethod.status = AuthMethodStatusSkipped;
        }
    }
}

- (BMTryAuthMethod*) authMethodWithTitle:(NSString*) title {
    for (BMTryAuthMethod* authMethod in self.authMethods) {
        if ([authMethod.title isEqualToString:title])
        return authMethod;
    }
    return nil;
}

#pragma mark - AuthSamples

- (void) addAuthResultToActiveInteractiveAuthMethod: (id) pResult {
    [[self activeInteractiveAuthMethod] addAuthSample: pResult];
}

- (void) addAuthResult:(NSString*) pResult toAuthMethod:(BMTryAuthMethod*) pAuthMethod {
    [pAuthMethod addAuthSample:pResult];
}

@end
