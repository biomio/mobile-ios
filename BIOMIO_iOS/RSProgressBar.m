//
//  RSProgressBar.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 8/28/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import "RSProgressBar.h"

@interface RSProgressBar ()

@property (nonatomic, assign) BOOL isRefreshing;
@property (nonatomic, strong) UIView *progressBarView;
@property (nonatomic, strong) NSMutableArray *refreshIndicators;
@end

@implementation RSProgressBar

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
        self.progressBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, frame.size.height)];
        self.barColor = [UIColor colorWithRed:22.f / 255.f green:126.f / 255.f blue:251.f / 255.f alpha:1.0];
        self.refreshIndicatorColor = [UIColor whiteColor];
        self.incompleteBarColor = [UIColor clearColor];
        [self addSubview:self.progressBarView];
        self.progressBarState = RSProgressBarStateStatic;
    }
    return self;
}

- (void) setFrame:(CGRect)frame {
    [super setFrame:frame];
    self.progressBarView.frame = CGRectMake(0, 0, self.frame.size.width, frame.size.height);
}

- (void)setProgressBarState:(RSProgressBarState)progressBarState {
    dispatch_async(dispatch_get_main_queue(), ^{
        _progressBarState = progressBarState;
        switch (progressBarState) {
            case RSProgressBarStateStatic: {
                [self setProgressBarProgress:1.0 animated:NO];
                if (self.isRefreshing)
                    [self finishRefresh];
                break;
            }
            case RSProgressBarStateRefreshing: {
                [self setProgressBarProgress:1.0 animated:NO];
                if (!self.isRefreshing)
                    [self startRefresh];
                break;
            }
            case RSProgressBarStateLoading: {
                if (self.isRefreshing)
                    [self finishRefresh];
                [self setProgressBarProgress:0.0 animated:NO];
                break;
            }
            default:
                break;
        }
    });
}

#pragma mark Colors

- (void) setBarColor:(UIColor *)barColor {
    if (barColor) {
        _barColor = barColor;
        self.progressBarView.backgroundColor = self.barColor;
    }
}

- (void)setIncompleteBarColor:(UIColor *)incompleteBarColor {
    _incompleteBarColor = incompleteBarColor;
    self.backgroundColor = self.incompleteBarColor;
}

#pragma mark Progress

- (CGFloat) getCurrentProgress {
    return self.progressBarView.frame.size.width/self.frame.size.width;
}

- (void)setProgressBarProgress:(CGFloat)progress animated:(BOOL)pAnimated {
    [self setProgressBarProgress:progress animated:pAnimated completed:nil];
}

- (void)setProgressBarProgress:(CGFloat)progress animated: (BOOL) pAnimated completed:(void(^)(BOOL completed)) completion {
    __block CGRect frame = self.progressBarView.frame;
    if (progress < 0.0)
        progress = 0.0;
    else if (progress > 1.0)
        progress = 1.0;
    frame.size.width = self.frame.size.width*progress;
    if (pAnimated) {
        NSTimeInterval animationDuration = 1*ABS([self getCurrentProgress] - progress);
        [UIView animateWithDuration:animationDuration animations:^{
            self.progressBarView.frame = frame;
        } completion:^(BOOL finished) {
            if (completion)
                completion(YES);
        }];
    }
    else {
        self.progressBarView.frame = frame;
    if (completion)
        completion(YES);
    }
}

#pragma mark Refreshing

- (void)startRefresh {
    self.isRefreshing = YES;
    self.refreshIndicators = [NSMutableArray new];
    
    for (NSInteger i = 0; i < 3; i++) {
        UIView *indicator = [[UIView alloc] initWithFrame:CGRectMake(-self.progressBarView.frame.size.height, 0, self.progressBarView.frame.size.height, self.progressBarView.frame.size.height)];
        indicator.backgroundColor = self.refreshIndicatorColor;
        
        float delay = 0.4 * i;
        
        [UIView animateWithDuration:1.2 delay:delay options:UIViewAnimationOptionCurveEaseIn|UIViewAnimationOptionRepeat animations:^{
            CGRect frame = indicator.frame;
            frame.origin.x = self.progressBarView.frame.size.width;
            indicator.frame = frame;
        } completion:^(BOOL finished) {
        }];
        [self.progressBarView addSubview:indicator];
        [self.refreshIndicators addObject:indicator];
    }
}

- (void)finishRefresh {
    self.isRefreshing = NO;
    [self.refreshIndicators enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [(UIView *)obj removeFromSuperview];
    }];
    [self.refreshIndicators removeAllObjects];
}

@end
