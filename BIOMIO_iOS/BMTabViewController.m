//
//  BMTabViewController.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 9/17/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

// view controllers
#import "BMTabViewController.h"
#import "BMHomeViewController.h"
#import "BMSettingsViewController.h"
#import "BMRegistrationViewController.h"
#import "BMHelpViewController.h"
#import "BMHelpLauncherViewController.h"
#import "BMScannerViewController.h"
// handlers
#import "BMUserModel.h"

@interface BMTabViewController ()

@end

@implementation BMTabViewController
@synthesize customTabBar;

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.customTabBar.delegate = self;

    for(UIView *view in self.view.subviews){
        if([view isKindOfClass:[UITabBar class]]){
            [view setFrame:CGRectMake(view.frame.origin.x, self.view.frame.size.height, view.frame.size.width, view.frame.size.height)];
        }else{
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, self.view.frame.size.height)];
        }
    }
    customTabBar = [[GUTabBarView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - self.tabBar.frame.size.height, self.view.frame.size.width, self.tabBar.frame.size.height)];
    customTabBar.delegate = self;
    [self.view addSubview:customTabBar];
    
    [self setupTabBarViewControllers];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];

    self.tabBar.translucent  = YES;
    self.tabBar.barStyle = UIBarStyleDefault;
    self.selectedIndex = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout

- (void) setupTabBarViewControllers {
    BMHomeViewController* homeVC = [[BMHomeViewController alloc] initWithNibName:@"BMHomeViewController" bundle:nil];
    UINavigationController* authNavController = [[UINavigationController alloc]initWithRootViewController:homeVC];
    
    BMScannerViewController* scannerVC = [[BMScannerViewController alloc] initWithNibName:@"BMScannerViewController" bundle:nil];
    UINavigationController* scannerNavController = [[UINavigationController alloc]initWithRootViewController:scannerVC];
    
    BMSettingsViewController* settingsVC = [[BMSettingsViewController alloc] initWithNibName:@"BMSettingsViewController" bundle:nil];
    UINavigationController* settNavController = [[UINavigationController alloc]initWithRootViewController:settingsVC];
    
    BMHelpLauncherViewController* helpLauncherVC = [[BMHelpLauncherViewController alloc]init];
    UINavigationController* helpNavController = [[UINavigationController alloc]initWithRootViewController:helpLauncherVC];
    
    if ([BMUserModel isRegistrationRequired])
        self.viewControllers = @[authNavController, settNavController, helpNavController];
    else
        self.viewControllers = @[authNavController, scannerNavController, settNavController, helpNavController];
    
    if ([BMUserModel isRegistrationRequired])
        [authNavController pushViewController:[[BMRegistrationViewController alloc]initWithNibName:@"BMRegistrationViewController" bundle:nil] animated:NO];
}

- (void) refreshTabBar {
    [self.customTabBar setTabBarView];
    [self setupTabBarViewControllers];
}

- (void)viewWillLayoutSubviews {
    CGRect tabbarFrame = self.tabBar.frame;
    // FIXME: fixes tabbar in ios 11.
    if (SYSTEM_VERSION_LESS_THAN(@"11.0")) {
      tabbarFrame.origin.y -= customTabBar.frame.size.height;
    }
    [UIView animateWithDuration:0.3 animations:^{
        customTabBar.frame = tabbarFrame;
    }];
}

#pragma mark - Buttons

- (void)tabBarItemPressedAtIndex:(NSInteger)pTag {
    self.selectedIndex = pTag;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
