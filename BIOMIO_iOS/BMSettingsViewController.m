//
//  BMSettingsViewController.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 6/15/15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

// view controllers
#import "BMSettingsViewController.h"
#import "BMSocketViewController.h"
#import "BMTabViewController.h"
// cells
#import "BMDetailTextfieldTableViewCell.h"
#import "BMSwitchTableViewCell.h"
#import "BMDeviceResourceWarningTableViewCell.h"
#import "BMAuthMethodCapabilitiesTableViewCell.h"
// handlers
#import "BMSocketHandler.h"
#import "BMUserModel.h"
#import "BMDeviceResources.h"

@interface BMSettingsViewController () <DetailTextfieldCellDelegate, SwitchCellDelegate>

@end

@implementation BMSettingsViewController {
    NSArray* warnings;
}

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    // observe app returning from background to recheck capabilities and reload tableview
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidReturnFromBackground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"BMDetailTextfieldTableViewCell" bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"DetailTextfieldCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BMSwitchTableViewCell" bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"SwitchCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BMDeviceResourceWarningTableViewCell" bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"DeviceWarningCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BMAuthMethodCapabilitiesTableViewCell" bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"CapabilitiesCell"];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
}

- (void) applicationDidReturnFromBackground {
    warnings = [BMDeviceResources deviceCapabilitiesWarnings];
    [self.tableView reloadData];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
    warnings = [BMDeviceResources deviceCapabilitiesWarnings];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Socket Delegates Methods

- (void)socketRequiresRegistration {
    
}

- (void)socketDidReceiveTryRequest {
    BMTabViewController* tabbar = (BMTabViewController*) self.tabBarController;
    [tabbar.customTabBar manuallySelectTabBarItemAtIndex:0];
    
    if  ([SOCKET_HANDLER.delegate respondsToSelector:@selector(socketDidReceiveTryRequest)])
        [SOCKET_HANDLER.delegate socketDidReceiveTryRequest];
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger numberOfSections = 2;
    if ([warnings count])
        numberOfSections++;
    if (![BMUserModel isRegistrationRequired])
        numberOfSections++;
    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return [[[NSBundle mainBundle] bundleIdentifier] hasSuffix:@".dev"] ? 4 : 3;
        case 1:
            return 1;
            
        default:{
            if (tableView.numberOfSections == 3) {
                if (![BMUserModel isRegistrationRequired])
                    return 1;
                else if ([warnings count])
                    return [warnings count];
            }
            else if (tableView.numberOfSections == 4) {
                if (section == 2)
                    return [warnings count];
                else
                    return 1;
            }
            return 0;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0: {
                static NSString *identifier = @"DetailTextfieldCell";
                BMDetailTextfieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: identifier];
                if (!cell) {
                    cell = [[BMDetailTextfieldTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                                                 reuseIdentifier:identifier];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.titleLabel.text = @"URL";
                cell.detailTextfield.text = [BMSocketHandler serverUrl];
                cell.detailTextfield.autocapitalizationType = UITextAutocapitalizationTypeNone;
                cell.detailTextfield.autocorrectionType = UITextAutocorrectionTypeNo;
                cell.detailTextfield.spellCheckingType = UITextSpellCheckingTypeNo;
                cell.delegate = self;
                return cell;
            }
            case 1: {
                static NSString *identifier = @"TableCellIdentifierButton";
                
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
                if (!cell) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                  reuseIdentifier:identifier];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.textLabel.textColor = COLOR_GREEN;
                cell.textLabel.text = @"Restore default URL";
                cell.textLabel.enabled = ![[BMSocketHandler serverUrl] isEqualToString:WEBSOCKET_URL];
                return cell;
            }
            case 2: {
                static NSString *identifier = @"TableCellIdentifierButton";
                
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
                if (!cell) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                  reuseIdentifier:identifier];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.textLabel.text = @"Socket logs";
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                return cell;
            }
            case 3: {
                static NSString *identifier = @"SwitchCell";
                BMSwitchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: identifier];
                if (!cell) {
                    cell = [[BMSwitchTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                                        reuseIdentifier:identifier];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.titleLabel.text = @"Send probe messages";
                cell.switchControl.on = [[NSUserDefaults standardUserDefaults]boolForKey:@"SEND_PROBE_MESSAGES"];
                cell.switchControl.onTintColor = COLOR_GREEN;
                cell.delegate = self;
                return cell;
            }
            default: {
                NSAssert(false, @"Settings: Number of rows is invalid");
            }
        }
    }
    else if (indexPath.section == 1) {
        switch (indexPath.row) {
            case 0: {
                static NSString *identifier = @"CapabilitiesCell";
                BMAuthMethodCapabilitiesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: identifier];
                if (!cell) {
                    cell = [[BMAuthMethodCapabilitiesTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                                                        reuseIdentifier:identifier];
                }
                
                NSArray* authMethods = @[@"camera_resource",
                                         @"fp",
                                         @"location_resource",
                                         @"voice"];
                
                NSMutableArray* methods = [NSMutableArray new];
                for (NSDictionary* warning in warnings)
                    [methods addObjectsFromArray:warning[@"authMethods"]];
                [cell initWithAuthMethods:authMethods
                                 disabled:methods];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            default: {
                NSAssert(false, @"Settings: Number of rows is invalid");
            }
        }
    }
    else if ([warnings count] && indexPath.section == 2) {
        static NSString *identifier = @"DeviceWarningCell";
        BMDeviceResourceWarningTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: identifier];
        if (!cell) {
            cell = [[BMDeviceResourceWarningTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                                               reuseIdentifier:identifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSDictionary* warningInfo = warnings[indexPath.row];
        [cell initWithAuthMethods:warningInfo[@"authMethods"]];
        cell.titleLabel.text = [warningInfo[@"title"] uppercaseString];
        cell.descriptionLabel.text = [NSString stringWithFormat:@"%@\n\n%@", warningInfo[@"description"],warningInfo[@"solution"]];
        
        return cell;
    }
    else if (![BMUserModel isRegistrationRequired] && indexPath.section == tableView.numberOfSections-1) {
        switch (indexPath.row) {
            case 0: {
                static NSString *identifier = @"TableCellIdentifier";
                
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
                if (!cell) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                  reuseIdentifier:identifier];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.textLabel.text = @"Log out";
                cell.textLabel.textAlignment = NSTextAlignmentCenter;
                cell.textLabel.font = [UIFont boldSystemFontOfSize:19];
                cell.textLabel.textColor = COLOR_RED;
                return cell;
            }
            default: {
                NSAssert(false, @"Settings: Number of rows is invalid");
            }
        }
    }
    return nil;
}

#pragma mark TableView Header/Footer

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"Server configuration";
        case 1:
            return @"Device capabilities";
        default:
            return nil;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    // adding footer with app version only to the last tableview section
    if (section == tableView.numberOfSections-1) { // if last section
        // if (bundleID == dev app) version = v1.0(2) else version = v1.0
        NSString* versionString = [[[NSBundle mainBundle] bundleIdentifier] hasSuffix:@".dev"] ?
        [NSString stringWithFormat:@"v%@(%@)", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"], [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]] :
        [NSString stringWithFormat:@"v%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
        
        return versionString;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section {
    NSUInteger sectionQnty = tableView.numberOfSections;
    if (section == sectionQnty-1) {
        UITableViewHeaderFooterView *footer = (UITableViewHeaderFooterView *)view;
        footer.textLabel.textAlignment = NSTextAlignmentCenter;
    }
}

#pragma mark TableView Selection

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == 1) { // // restore default url
        BMDetailTextfieldTableViewCell* urlCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        if (![urlCell.detailTextfield.text isEqualToString:WEBSOCKET_URL]){ // if textfield.text != defaultURL -> textfield.text=defaultURL
            urlCell.detailTextfield.text = WEBSOCKET_URL;
            [urlCell.detailTextfield resignFirstResponder];
            
            UITableViewCell* restoreCell = [tableView cellForRowAtIndexPath:indexPath];
            restoreCell.textLabel.enabled = NO;
            if (![[BMSocketHandler serverUrl] isEqualToString:WEBSOCKET_URL]) { // additionally: if socketURL != defaultURL -> socketURL=defaultURL
                [BMSocketHandler restoreDefaultServerUrl];
                [[BMSocketHandler sharedSocket] restartWithUserReset:YES];}
        }
    }
    // socket logs
    else if (indexPath.section == 0 && indexPath.row == 2) { // socket logs
        BMSocketViewController *lSocketViewController = [[BMSocketViewController alloc]initWithNibName:@"BMSocketViewController" bundle:nil];
        [self.navigationController pushViewController:lSocketViewController animated:YES];
    }
    // log out
    else if (![BMUserModel isRegistrationRequired] && // already logged-in -> last section = "log out"
             indexPath.section == tableView.numberOfSections-1 && // last section
             indexPath.row == 0){ // log out button row
        BMTabViewController* tabbar = (BMTabViewController*) self.tabBarController;
        [tabbar.customTabBar manuallySelectTabBarItemAtIndex:0];
        [[BMSocketHandler sharedSocket] restartWithUserReset:YES];
        [tabbar refreshTabBar];
    }
}

#pragma mark - Cell delegate methods

- (void)textfieldCell:(BMDetailTextfieldTableViewCell *)cell didFinishEditingTextfield:(UITextField *)textfield {
    // TODO: check text for URL-format
    [self.view endEditing:YES];
    if (![textfield.text isEqualToString: [BMSocketHandler serverUrl]]) {
        [BMSocketHandler setServerUrl: textfield.text];
        [[BMSocketHandler sharedSocket] restartWithUserReset:YES];
        
        BMTabViewController* tabbar = (BMTabViewController*) self.tabBarController;
        [tabbar.customTabBar manuallySelectTabBarItemAtIndex:1];
        [tabbar refreshTabBar];
        [self.tableView reloadData];
    }
}

- (void)textfieldCell:(BMDetailTextfieldTableViewCell *)cell textfield:(UITextField *)textfield didChangeTextTo:(NSString *)newText {
    UITableViewCell* restoreCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    restoreCell.textLabel.enabled = ![newText isEqualToString: [BMSocketHandler serverUrl]];
}

- (void)switchCell:(BMSwitchTableViewCell *)cell didChangeSwitchState:(UISwitch *)switchCtrl {
    [[NSUserDefaults standardUserDefaults] setBool:switchCtrl.on forKey:@"SEND_PROBE_MESSAGES"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
