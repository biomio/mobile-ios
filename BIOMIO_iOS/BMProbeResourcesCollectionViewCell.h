//
//  BMProbeResourcesCollectionViewCell.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 03.09.15.
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BMTryAuthMethod.h"

@interface BMProbeResourcesCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *resourceImageView;
@property (weak, nonatomic) IBOutlet UIImageView *checkmarkImageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, assign) AuthMethodStatus status;
@property (nonatomic, assign) BOOL active;
@property (nonatomic, strong) NSString* defaultImageTitle;
@end
