//
//  BMProbeViewController.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 29.08.15.ах
//  Copyright (c) 2015 BIOMIO. All rights reserved.
//

#define PROBE_PROGRESS_BAR_HEIGHT           15

// view controllers
#import "BMProbeViewController.h"
#import "BMLiveProcCaptureViewController.h"
#import "BMAnyProbeViewController.h"
// cells
#import "BMProbeResourcesCollectionViewCell.h"
#import "BMNonInteractiveAuthMethodCollectionViewCell.h"
#import "BMCreditCardCheckViewController.h"
// views
#import "RSProgressBar.h"
#import <GBSlideOutToUnlockView/GBSlideOutToUnlockView.h>
#import "BMPinCodeView.h"
// handlers
#import "BMSocketHandler.h"
#import "BMStateMachine.h"
#import "BMDeviceResources.h"
#import "BMTryRequestHandler.h"
#import "BMErrorHandler.h"
// models
#import "BMTryAuthMethod.h"
// libs/frameworks
#import <LocalAuthentication/LocalAuthentication.h>
#import <CoreGraphics/CGGeometry.h>

#if !CARD_IO_FRAMEWORK
@interface BMProbeViewController () <BMSocketDelegate, BMLiveCaptureDelegate, BMTryRequestHandlerDelegate, GBSlideOutToUnlockViewDelegate, PinCodeViewDelegate>
#else
#import <CardIO/CardIO.h>
@interface BMProbeViewController () <BMSocketDelegate, BMLiveCaptureDelegate, BMTryRequestHandlerDelegate, GBSlideOutToUnlockViewDelegate, PinCodeViewDelegate, CardIOViewDelegate,CardIOPaymentViewControllerDelegate, CreditCardCheckViewControllerDelegate>
#endif
@end

@implementation BMProbeViewController {
    BMLiveProcCaptureViewController *liveCameraViewController;
    CGFloat scrnBrightness;
    RSProgressBar* probeProgressBar;
    UILabel* probeProgressLabel;
    
    NSDictionary* authMethodsDatasource;
}

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.probeResourcesCollectionView registerNib:[UINib nibWithNibName:@"BMProbeResourcesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    [self.nonInteractiveAuthMethodsCollectionView registerNib:[UINib nibWithNibName:@"BMNonInteractiveAuthMethodCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"NonInteractiveCell"];
    self.probeMainView.clipsToBounds = YES;
    TRY_REQUEST_HANDLER.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    authMethodsDatasource = [self notCompletedAuthMethods];
    
    [self runAuthentication];
    
    [self.probeResourcesCollectionView reloadData];
    [self.nonInteractiveAuthMethodsCollectionView reloadData];
}

- (void) pushToAnyProbeViewControllerWithTryRequest:(BMTryRequestRoot*) tryRequest {
    BMAnyProbeViewController *lAnyProbeViewController = [[BMAnyProbeViewController alloc]initWithNibName:@"BMAnyProbeViewController" bundle:nil];
    lAnyProbeViewController.tryRequest = tryRequest;
    [self.navigationController pushViewController:lAnyProbeViewController animated:NO];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}

- (void)dealloc {
    self.probeResourcesCollectionView.delegate = nil;
    self.probeResourcesCollectionView.dataSource = nil;
    
    self.nonInteractiveAuthMethodsCollectionView.delegate = nil;
    self.nonInteractiveAuthMethodsCollectionView.dataSource = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI -

#pragma mark UICollectionView delegate methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([collectionView isEqual:self.probeResourcesCollectionView])
        return [authMethodsDatasource[@"interactive"] count]; //[TRY_REQUEST_HANDLER.nextTryRequest.selectedAuthMethod count];
    else if ([collectionView isEqual:self.nonInteractiveAuthMethodsCollectionView])
        return authMethodsDatasource[@"nonInteractive"] ? [authMethodsDatasource[@"nonInteractive"] count] : 0;
    return 0;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([collectionView isEqual:self.probeResourcesCollectionView]) {
        NSString *identifier = @"Cell";
        BMProbeResourcesCollectionViewCell* cell = [self.probeResourcesCollectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        BMTryAuthMethod* lResource = [authMethodsDatasource[@"interactive"] objectAtIndex:indexPath.row];
        cell.defaultImageTitle = lResource.title;
        //        cell.titleLabel.text = NSLocalizedString(lResource.title, nil);
        cell.status = lResource.status;
        cell.active = ([TRY_REQUEST_HANDLER.activeInteractiveAuthMethod isEqual:lResource]) ? YES : NO;
        return cell;
    }
    else if ([collectionView isEqual:self.nonInteractiveAuthMethodsCollectionView]) {
        NSString *identifier = @"NonInteractiveCell";
        BMNonInteractiveAuthMethodCollectionViewCell* cell = [self.nonInteractiveAuthMethodsCollectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        BMTryAuthMethod* lResource = [authMethodsDatasource[@"nonInteractive"] objectAtIndex:indexPath.row];
        cell.authMethodTitleLabel.text = NSLocalizedString(lResource.title, nil);
        cell.authMethodTitle = lResource.title;
        cell.completed = lResource.status == AuthMethodStatusCompleted;
        return cell;
    }
    return nil;
}

- (void) updateProbeCollectionView {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.probeResourcesCollectionView reloadData];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        authMethodsDatasource = [self notCompletedAuthMethods];
        [self.probeResourcesCollectionView performBatchUpdates:^{
            [self.probeResourcesCollectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
        } completion:nil];
    });
}

- (void) updateNonInteractiveCollectionView {
    //    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.nonInteractiveAuthMethodsCollectionView reloadData];
        //        authMethodsDatasource = [weakSelf notCompletedAuthMethods];
        //        [self.nonInteractiveAuthMethodsCollectionView performBatchUpdates:^{
        //            [self.nonInteractiveAuthMethodsCollectionView deleteItemsAtIndexPaths:authMethodsDatasource[@"nonIntRemoveIndex"]];
        //        } completion:nil];
    });
}

#pragma mark Button Methods

- (IBAction)cancelProbeButtonPressed:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([TRY_REQUEST_HANDLER.activeInteractiveAuthMethod.title isEqualToString:@"face"]  || [TRY_REQUEST_HANDLER.activeInteractiveAuthMethod.title isEqualToString:@"palm"])
            [self removeCameraPreviewWithCompletion:nil];
    });
    NSError* error = nil;
    BMTryRequestRoot* cancelTry = [TRY_REQUEST_HANDLER activeTryRequest];
    [STATE_MACHINE fireEvent:STATE_MACHINE.biometricAuthCanceled userInfo: cancelTry ? @{@"cancelTry" : cancelTry} : nil error:&error];
}

- (void)showNavBarBackButton:(BOOL) show {
    if (show) {
        [self.navBarBackButton setEnabled:YES];
        [self.navBarBackButton setTintColor:[UIColor whiteColor]];
    } else {
        [self.navBarBackButton setEnabled:NO];
        [self.navBarBackButton setTintColor: [UIColor clearColor]];
    }
}

- (IBAction)backToAnyMenuButtonPressed:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([TRY_REQUEST_HANDLER activeTryRequest].condition == TryConditionAny) {
            for (UIView* lView in self.probeMainView.subviews)
                [lView removeFromSuperview];
            [self hideProbeProgressBar];
            
            TRY_REQUEST_HANDLER.activeInteractiveAuthMethod = nil;
            [TRY_REQUEST_HANDLER.activeTryRequest recoverToClean]; // TODO: check this return value to check whether returning back to AnyVC is possible
            [self runAuthentication];
        }
    });
}

#pragma mark ProbeProgressBar

- (void) showProbeProgressBar {
    CGFloat lProbeProgressPosY = self.nonInteractiveAuthMethodsCollectionView.frame.origin.y - PROBE_PROGRESS_BAR_HEIGHT;
    probeProgressBar = [[RSProgressBar alloc] initWithFrame:CGRectMake(0, lProbeProgressPosY, [UIScreen mainScreen].bounds.size.width, PROBE_PROGRESS_BAR_HEIGHT)];
    BMTryAuthMethod* activeAuthMethod = TRY_REQUEST_HANDLER.activeInteractiveAuthMethod;
    
    if ([activeAuthMethod.title isEqualToString:@"face"] ||
        [activeAuthMethod.title isEqualToString:@"palm"]) {
        [probeProgressBar setProgressBarState:RSProgressBarStateLoading];
        [probeProgressBar setBarColor: COLOR_GREEN];
        [probeProgressBar setIncompleteBarColor:[UIColor colorWithRed:0.396 green:0.416 blue:0.498 alpha:1.000]];
        
        probeProgressLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, probeProgressBar.frame.size.width, probeProgressBar.frame.size.height)];
        [probeProgressLabel setTextAlignment:NSTextAlignmentCenter];
        probeProgressLabel.textColor = [UIColor whiteColor];
        [probeProgressBar addSubview:probeProgressLabel];
        
        [self updateProbeProgressBarWithCompletion:^(BOOL finished) {
            [probeProgressBar setProgressBarProgress:0.05 animated:YES];
        }];
    }
    else if ([activeAuthMethod.title isEqualToString:@"push_button"]) {
        [probeProgressBar setProgressBarState:RSProgressBarStateLoading];
        [probeProgressBar setBarColor:COLOR_GREEN];
        [probeProgressBar setIncompleteBarColor:[UIColor colorWithRed:0.396 green:0.416 blue:0.498 alpha:1.000]];
        [probeProgressBar setProgressBarProgress:0.0 animated:YES];
    }
    [self.view addSubview:probeProgressBar];
}

- (void) updateProbeProgressBarWithCompletion:(void(^)(BOOL finished)) completion {
    BMTryAuthMethod* activeAuthMethod = TRY_REQUEST_HANDLER.activeInteractiveAuthMethod;
    NSString* lProgressString = [NSString stringWithFormat:@"%li/%li",
                                 (long) [activeAuthMethod samplesCompleted],
                                 (long) activeAuthMethod.samplesRequested];
    probeProgressLabel.text = lProgressString;
    CGFloat progress = (CGFloat) [activeAuthMethod samplesCompleted]/activeAuthMethod.samplesRequested;
    [probeProgressBar setProgressBarProgress:progress animated:YES completed:^(BOOL completed) {
        if (completion)
            completion(completed);
    }];
}

- (void) hideProbeProgressBar {
    [probeProgressBar removeFromSuperview];
    [probeProgressLabel removeFromSuperview];
    probeProgressBar = nil;
    probeProgressLabel = nil;
}

#pragma mark - Auth Methods -
#pragma mark Load/Complete

- (void) runAuthentication {
    if ([TRY_REQUEST_HANDLER readyToRun]) {
        BMTryRequestRoot* nextTryRequest = [TRY_REQUEST_HANDLER nextTryRequest];
        [self showNavBarBackButton:nextTryRequest.condition == TryConditionAny];
        if ([nextTryRequest.interactiveAuthMethods count] && [nextTryRequest.selectedAuthMethod count]) {
            BMTryAuthMethod* nextInteractive = [TRY_REQUEST_HANDLER nextInteractiveAuthMethod];
            if (nextInteractive)
                [self loadAuthMethod:nextInteractive];
        } else if ([nextTryRequest.interactiveAuthMethods count]) {
            [self pushToAnyProbeViewControllerWithTryRequest:nextTryRequest];
        }
        else {
            NSLog(@"ELSE BLOCK");
        }
    } else {
        // delay reason: update the UI (add checkmark to the latest completed authmethod)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            NSError* error = nil;
            [STATE_MACHINE fireEvent:STATE_MACHINE.biometricAuthDidSucceed userInfo:nil error:&error];
        });
    }
}

- (void) loadAuthMethod: (BMTryAuthMethod*) pAuthMethod {
    NSAssert(pAuthMethod, @"loadAuthMethod: authMethod is nil");
    if (TRY_REQUEST_HANDLER.activeInteractiveAuthMethod == pAuthMethod)
        return;
    // TODO: add isEqual for BMTryAuthMethod
    TRY_REQUEST_HANDLER.activeInteractiveAuthMethod = pAuthMethod;
    
    __weak typeof(self) weakSelf = self;
    self.navBarTitle.title = [[[TRY_REQUEST_HANDLER activeTryRequest] tryRequestType] ?: @"" uppercaseString];
    if ([pAuthMethod.title isEqualToString:@"fp"]) {
        NSError* error;
        [BMDeviceResources isTouchIDAvailableWithError:&error];
        if (!error) {
            [self touchIdAuthentication:^(BOOL success, NSError *authError) {
                if (authError) {
                    if (authError.code== 42004) {
                        if ([TRY_REQUEST_HANDLER activeTryRequest].condition == TryConditionAll) {
                            [self cancelProbeButtonPressed:nil];
                        } else if ([TRY_REQUEST_HANDLER activeTryRequest].condition == TryConditionAny) {
                            [self backToAnyMenuButtonPressed:nil];
                        }
                    }
                    else
                        [[BMSocketHandler sharedSocket] sendProbeMessageWithAuthMethod:pAuthMethod authResult:nil status:BMProbeStatusFailed error:authError];
                }
                else {
                    [TRY_REQUEST_HANDLER addAuthResultToActiveInteractiveAuthMethod:success ? @"true" : @"false"];
                    [[BMSocketHandler sharedSocket] sendProbeMessageWithAuthMethod:pAuthMethod
                                                                        authResult:success ? @"true" : @"false"
                                                                            status:BMProbeStatusSuccess
                                                                             error:nil];
                    [self authMethodProbeDidComplete];
                    return;
                }
            }];
        }
        else {
            [pAuthMethod didFailWithError:error];
            [[BMSocketHandler sharedSocket] sendProbeMessageWithAuthMethod:pAuthMethod
                                                                authResult:nil
                                                                    status:BMProbeStatusFailed
                                                                     error:error];
        }
    }
    else if ([pAuthMethod.title isEqualToString:@"face"] || [pAuthMethod.title isEqualToString:@"palm"]) {
        NSError* error = nil;
        [BMDeviceResources isCameraAvailableWithError:&error];
        if (!error) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf createCameraPreviewforAuthMethod:pAuthMethod];
            });
        }
        else {
            [SOCKET_HANDLER sendProbeMessageWithAuthMethod:TRY_REQUEST_HANDLER.activeInteractiveAuthMethod authResult:nil status:BMProbeStatusFailed error:error];
            [weakSelf authMethodProbeDidComplete];
        }
    }
    else if ([pAuthMethod.title isEqualToString:@"pin_code"]){
        [self addPinCodeView];
    }
    else if ([pAuthMethod.title isEqualToString:@"push_button"]){
        [self addSlideToVerifyView];
    }
    else if ([pAuthMethod.title isEqualToString:@"credit_card"]) {
        [self addCreditCardIdView];
    }
    else if ([pAuthMethod.title isEqualToString:@"voice"]) {
        NSAssert(NO, @"Voice handling isn't implemented");
    }
}

- (void) authMethodProbeDidComplete {
    TRY_REQUEST_HANDLER.activeInteractiveAuthMethod = nil;
    [self updateProbeCollectionView];
    [self runAuthentication];
}

- (NSDictionary*) notCompletedAuthMethods {
    NSMutableArray* intAuthmethods = [NSMutableArray new];
    NSMutableArray* nonIntAuthmethods = [NSMutableArray new];
    BMTryRequestRoot* nextTryRequest = [TRY_REQUEST_HANDLER nextTryRequest];
    
    NSMutableArray* removeIndexedInteractive = [NSMutableArray new];
    NSMutableArray* removeIndexedNonInteractive = [NSMutableArray new];
    
    for (NSInteger i=0; i<nextTryRequest.interactiveAuthMethods.count; i++) {
        BMTryAuthMethod* authmethod = nextTryRequest.interactiveAuthMethods[i];
        if ([authMethodsDatasource [@"interactive"] containsObject:authmethod] && authmethod.status != AuthMethodStatusClean && authmethod.status != AuthMethodStatusActive) {
            [removeIndexedInteractive addObject:[NSIndexPath indexPathForItem:i inSection:0]];
        }
        if (authmethod.status == AuthMethodStatusClean || authmethod.status == AuthMethodStatusActive)
            [intAuthmethods addObject:authmethod];
    }
    for (NSInteger i=0; i<nextTryRequest.nonInteractiveAuthMethods.count; i++) {
        BMTryAuthMethod* authmethod = nextTryRequest.nonInteractiveAuthMethods[i];
        if ([authMethodsDatasource [@"nonInteractive"] containsObject:authmethod] && authmethod.status != AuthMethodStatusClean && authmethod.status != AuthMethodStatusActive) {
            [removeIndexedNonInteractive addObject:[NSIndexPath indexPathForItem:i inSection:0]];
        }
        if (authmethod.status == AuthMethodStatusClean || authmethod.status == AuthMethodStatusActive)
            [nonIntAuthmethods addObject:authmethod];
    }
    return @{@"interactive" : intAuthmethods,
             @"nonInteractive" : nonIntAuthmethods,
             @"intRemoveIndex" : removeIndexedInteractive,
             @"nonIntRemoveIndex" : removeIndexedNonInteractive};
}

#pragma mark - Camera methods
- (void) createCameraPreviewforAuthMethod: (BMTryAuthMethod*) pAuthMethod {
    // loading camera view controller as child view controller
    liveCameraViewController = [[BMLiveProcCaptureViewController alloc]initWithNibName:@"BMLiveProcCaptureViewController" bundle:nil];
    liveCameraViewController.qualityPreset = [BMDeviceResources captureSessionPresetByResolution:pAuthMethod.deviceSettings];
    liveCameraViewController.camera = ([pAuthMethod.deviceTitle isEqualToString:@"front-cam"]) ? FrontCamera : BackCamera;
    
    liveCameraViewController.samplesRequested = pAuthMethod.samplesRequested;
    liveCameraViewController.detectionCascadeFilename = ([pAuthMethod.title isEqualToString:@"palm"]) ? @"haar_palm" : @"haar_frontFace";
    
    liveCameraViewController.frameRect = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, self.probeMainView.frame.size.height);
    liveCameraViewController.cameraDelegate = self;
    
    [self addChildViewController:liveCameraViewController];
    [self.probeMainView addSubview:liveCameraViewController.view];
    [self.probeMainView bringSubviewToFront:liveCameraViewController.view];
    [liveCameraViewController didMoveToParentViewController:self];
    [self viewDidLayoutSubviews];
    
    // loading ADDITIONAL UI elements
    // probe progress bar
    [self showProbeProgressBar];
    
    // screen brightness for face photo
    if ([pAuthMethod.deviceTitle isEqualToString:@"front-cam"]) {
        scrnBrightness = [[UIScreen mainScreen]brightness];
        [[UIScreen mainScreen] setBrightness:1.0];
    }
}

- (void) removeCameraPreviewWithCompletion:(void(^__nullable)(BOOL removed)) completion {
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:1.0 animations:^{
            liveCameraViewController.view.alpha = 0;
            [self hideProbeProgressBar];
        } completion:^(BOOL finished) {
            [liveCameraViewController.view removeFromSuperview];
            [[UIScreen mainScreen] setBrightness:scrnBrightness];
            [liveCameraViewController removeFromParentViewController];
            [liveCameraViewController willMoveToParentViewController:nil];
            liveCameraViewController = nil;
            if (completion)
                completion(finished);
        }];
    });
}

#pragma mark TouchID

- (void)touchIdAuthentication:(void(^)(BOOL success, NSError *error))reply {
    LAContext *context = [[LAContext alloc] init];
    [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
            localizedReason:@"Use Touch ID to identify yourself"
                      reply:^(BOOL authSuccess, NSError * _Nullable authError) {
                          if (authError) {
                              switch (authError.code) {
                                  case LAErrorUserCancel:
                                      reply(NO,[BMErrorHandler errorWithCode:42004]);
                                      break;
                                  case LAErrorUserFallback:
                                      reply(NO,[BMErrorHandler errorWithCode:42005]);
                                      break;
                                  case LAErrorTouchIDLockout:
                                      reply(NO,[BMErrorHandler errorWithCode:42009]);
                                      break;
                                  case LAErrorAuthenticationFailed:
                                      reply(NO,[BMErrorHandler errorWithCode:42003]);
                                      break;
                                  case LAErrorPasscodeNotSet:
                                      reply(NO,[BMErrorHandler errorWithCode:42002]);
                                      break;
                                  case LAErrorTouchIDNotAvailable:
                                      reply(NO,[BMErrorHandler errorWithCode:42007]);
                                      break;
                                  case LAErrorTouchIDNotEnrolled:
                                      reply(NO,[BMErrorHandler errorWithCode:42008]);
                                      break;
                                  case LAErrorSystemCancel:
                                      reply(NO,[BMErrorHandler errorWithCode:42006]);
                                      break;
                                  case LAErrorAppCancel:
                                      reply(NO,[BMErrorHandler errorWithCode:42010]);
                                      break;
                                  case LAErrorInvalidContext:
                                      reply(NO,[BMErrorHandler errorWithCode:42006]);
                                      break;
                                  default:
                                      reply(NO,[BMErrorHandler errorWithCode:42001]);
                                      break;
                              }
                          } else
                              reply(YES, nil);
                      }];
}

#pragma mark Slide to Verify (Push button)

- (void) addSlideToVerifyView {
    dispatch_async(dispatch_get_main_queue(), ^{
        CGFloat heightRatio = 0.8; // the ratio of slideOutView.height to probeView.height
        GBSlideOutToUnlockView* slideView = [[GBSlideOutToUnlockView alloc]initWithFrame:CGRectMake(0, (self.probeMainView.frame.size.height*(1-heightRatio))/2, self.probeMainView.frame.size.width, self.probeMainView.frame.size.height*heightRatio)];
        slideView.outerCircleRadius = slideView.frame.size.height*0.35;
        slideView.draggableImage = [UIImage imageNamed:@"lock.png"];
        slideView.outerCircleColor = COLOR_GREEN;
        slideView.innerCircleColor = COLOR_ORANGE;
        slideView.draggableButtonBackgroundColor = COLOR_ORANGE;
        slideView.clipsToBounds = NO;
        slideView.delegate = self;
        
        [self.probeMainView addSubview:slideView];
        // setting autolayout constraints to center the slideoutView inside the probeView
        UILayoutGuide* margins = self.probeMainView.layoutMarginsGuide;
        
        [slideView.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor constant:0].active = YES;
        [slideView.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor constant:0].active = YES;
        [slideView.topAnchor constraintEqualToAnchor:margins.topAnchor constant:0].active = YES;
        [slideView.bottomAnchor constraintEqualToAnchor:margins.bottomAnchor constant:0].active = YES;
        
        [self showProbeProgressBar];
    });
}

#pragma mark Pin Code

- (void) addPinCodeView {
    dispatch_async(dispatch_get_main_queue(), ^{
        // TODO: pincode expiration: change 3000 -> expiration received from server
        BMPinCodeView* pinCodeView = [BMPinCodeView pinCodeViewWithFrame:CGRectMake(0, 0, self.probeMainView.frame.size.width, self.probeMainView.frame.size.height-8)
                                                                    code:TRY_REQUEST_HANDLER.activeInteractiveAuthMethod.deviceSettings
                                                        expirationPeriod:MIN(TRY_REQUEST_HANDLER.activeTryRequest.authTimeoutTimer.fireDate.timeIntervalSinceNow, 3000)
                                                                delegate:self];
        
        [self.probeMainView addSubview:pinCodeView];
        UILayoutGuide* margins = self.probeMainView.layoutMarginsGuide;
        
        [pinCodeView.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor constant:0].active = YES;
        [pinCodeView.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor constant:0].active = YES;
        [pinCodeView.topAnchor constraintEqualToAnchor:margins.topAnchor constant:0].active = YES;
        [pinCodeView.bottomAnchor constraintEqualToAnchor:margins.bottomAnchor constant:0].active = YES;
    });
}

#pragma mark Credit Card

- (void) addCreditCardIdView {
#if CARD_IO_FRAMEWORK
    if ([CardIOUtilities canReadCardWithCamera]) {
        CGFloat height = CGRectGetMinY(self.nonInteractiveAuthMethodsCollectionView.frame)-self.navigationController.navigationBar.bounds.size.height + [UIApplication sharedApplication].statusBarFrame.size.height;
        
        CardIOView *cardIOView = [[CardIOView alloc] initWithFrame: CGRectMake( 0.0,0.0, [[UIScreen mainScreen] bounds].size.width, height)];
        cardIOView.allowFreelyRotatingCardGuide = NO;
        cardIOView.delegate = self;
        [self.probeMainView addSubview:cardIOView];
    }
#else
    NSError* error = [BMErrorHandler errorWithCode:45002];
    [[BMSocketHandler sharedSocket] sendProbeMessageWithAuthMethod:[TRY_REQUEST_HANDLER activeInteractiveAuthMethod]
                                                        authResult:nil
                                                            status:BMProbeStatusFailed
                                                             error:error];
    
    NSLog(@"CARD.IO FRAMEWORK MISSING");
    UIAlertController* lAlertCtrl = [UIAlertController alertControllerWithTitle:@"CARD.IO"
                                                                        message:@"FRAMEWORK MISSING"
                                                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* lOkAction = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                                                            [self authMethodProbeDidComplete];
                                                        }];
    [lAlertCtrl addAction:lOkAction];
    [self presentViewController:lAlertCtrl animated:YES completion:nil];
#endif
}

#pragma mark - SocketHandler delegate methods

- (void)socketDidReceiveTryRequest {
    
}

-(void) socketDidFailWithError:(NSError *)pError {
    if (pError) {
        UIAlertController* lAlertCtrl = [UIAlertController alertControllerWithTitle:pError.userInfo[@"subcategory"]
                                                                            message:pError.localizedDescription
                                                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* lOkAction = [UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction * _Nonnull action) {
                                                              [self authMethodProbeDidComplete];
                                                          }];
        [lAlertCtrl addAction:lOkAction];
        [self presentViewController:lAlertCtrl animated:YES completion:nil];
    } else {
        UIAlertController* lAlertCtrl = [UIAlertController alertControllerWithTitle:@"Authentication failed"
                                                                            message:@"Unknown error"
                                                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* lOkAction = [UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                                                                [self authMethodProbeDidComplete];
                                                            }];
        
        [lAlertCtrl addAction:lOkAction];
        [self presentViewController:lAlertCtrl animated:YES completion:nil];
    }
}

#pragma mark BMTryRequestHandlerDelegate

- (void)nonInteractiveAuthMethodsDidUpdate {
    [self updateNonInteractiveCollectionView];
    [self runAuthentication];
}

- (void)tryRequestDidExpire:(BMTryRequestRoot *)pTry {
    NSError* error = nil;
    [STATE_MACHINE fireEvent:STATE_MACHINE.biometricAuthDidExpire userInfo:nil error:&error];
}

#pragma mark - Delegate Methods -

#pragma mark Camera Delegate Methods

- (BOOL)BMLiveCapturedPhoto:(UIImage *)image encodedToBase64:(NSString *)encodedImage {
    BMTryAuthMethod* cameraAuthMethod = TRY_REQUEST_HANDLER.activeInteractiveAuthMethod;
    if ([cameraAuthMethod.title isEqualToString:@"face"] ||
        [cameraAuthMethod.title isEqualToString:@"palm"])
    {
        if (cameraAuthMethod.status == AuthMethodStatusActive) {
            [TRY_REQUEST_HANDLER addAuthResultToActiveInteractiveAuthMethod:encodedImage];
            [SOCKET_HANDLER sendProbeMessageWithAuthMethod:cameraAuthMethod
                                                authResult:encodedImage
                                                    status:BMProbeStatusSuccess
                                                     error:nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self updateProbeProgressBarWithCompletion:nil];
            });
            
            if (cameraAuthMethod.status == AuthMethodStatusSent ||
                cameraAuthMethod.status == AuthMethodStatusCompleted) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self removeCameraPreviewWithCompletion:^(BOOL removed) {
                        if (removed)
                            [self authMethodProbeDidComplete];
                    }];
                });
                return YES;
            } else
                return NO;
        }
    }
    NSAssert(NO, @"BMLiveCapturedPhoto:encodedToBase64 called when not camera-authmethod was active");
    return YES;
}

#pragma mark GBSlideOutToUnlockViewDelegate (Push button)

- (void)slideOutToUnlockViewDidUnlock:(GBSlideOutToUnlockView *)slideOutToUnlockView {
    [TRY_REQUEST_HANDLER addAuthResultToActiveInteractiveAuthMethod:@"true"];
    [[BMSocketHandler sharedSocket] sendProbeMessageWithAuthMethod:[TRY_REQUEST_HANDLER activeInteractiveAuthMethod]
                                                        authResult:@"true"
                                                            status:BMProbeStatusSuccess
                                                             error:nil];
    for (UIView* view in [self.probeMainView subviews])
        if ([view isKindOfClass:[GBSlideOutToUnlockView class]])
            [view removeFromSuperview];
    [self hideProbeProgressBar];
    [self authMethodProbeDidComplete];
}

- (void)slideOutToUnlockViewDidNotUnlock:(GBSlideOutToUnlockView *)slideOutToUnlockView {
    [probeProgressBar setProgressBarProgress:0.0 animated:YES];
    NSLog(@"GBSlideOutToUnlockView Did not unlock");
}

- (void)slideOutToUnlockView:(GBSlideOutToUnlockView *)slideOutView didDragDistance:(CGFloat)distance {
    CGFloat progress = distance/slideOutView.outerCircleRadius;
    progress = progress > 1.0 ? 1.0 : (progress < 0.0) ? 0.0 : progress;
    [probeProgressBar setProgressBarProgress:progress animated:YES];
}

#pragma mark PinCodeView

- (void) pinCodeDidExpire:(BMPinCodeView *)pinCodeView {
    [pinCodeView removeFromSuperview];
    
    NSError* error = [BMErrorHandler errorWithCode:21002];
    [[BMSocketHandler sharedSocket] sendProbeMessageWithAuthMethod:[TRY_REQUEST_HANDLER activeInteractiveAuthMethod]
                                                        authResult:nil
                                                            status:BMProbeStatusFailed
                                                             error:error];
    [self authMethodProbeDidComplete];
}

- (void) pinCodeConfirmed:(BMPinCodeView *)pinCodeView {
    [pinCodeView removeFromSuperview];
    [TRY_REQUEST_HANDLER addAuthResultToActiveInteractiveAuthMethod:@"true"];
    [[BMSocketHandler sharedSocket] sendProbeMessageWithAuthMethod:[TRY_REQUEST_HANDLER activeInteractiveAuthMethod]
                                                        authResult:@"true"
                                                            status:BMProbeStatusSuccess
                                                             error: nil];
    [self authMethodProbeDidComplete];
}

#pragma mark Card.IO Delegate

#if CARD_IO_FRAMEWORK
- (void)cardIOView:(CardIOView *)cardIOView didScanCard:(CardIOCreditCardInfo *)info {
    if (info) {
        // The full card number is available as info.cardNumber, but don't log that!
        NSLog(@"Received card info. Number: %@, expiry: %02lu/%lu, cvv: %@.", info.redactedCardNumber, (unsigned long)info.expiryMonth, (unsigned long)info.expiryYear, info.cvv);
        // Use the card info...
        [cardIOView removeFromSuperview];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            BMCreditCardCheckViewController* creditCardVC = [[BMCreditCardCheckViewController alloc] initWithNibName:@"BMCreditCardCheckViewController" bundle:nil];
            creditCardVC.modalPresentationStyle = UIModalPresentationFormSheet;
            creditCardVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            
            creditCardVC.cardNumber = info.cardNumber;
            creditCardVC.expiryMonth = info.expiryMonth;
            creditCardVC.expiryYear = info.expiryYear;
            
            creditCardVC.delegate = self;
            [self presentViewController:creditCardVC animated:YES completion:nil];
        });
    }
    else {
        NSLog(@"CardIOView failed");
        [cardIOView removeFromSuperview];
        // TODO: card.io add failure error description
        NSError* error = [BMErrorHandler errorWithCode:45001];
        [[BMSocketHandler sharedSocket] sendProbeMessageWithAuthMethod:[TRY_REQUEST_HANDLER activeInteractiveAuthMethod]
                                                            authResult:nil
                                                                status:BMProbeStatusFailed
                                                                 error:error];
        [self authMethodProbeDidComplete];
    }
}

#pragma mark CreditCardCheckViewControllerDelegate

- (void)userDidConfirmCardNumber:(NSString *)pNumber expiryMonth:(NSString *)pMonth expiryYear:(NSString *)pYear {
    NSString* result = [NSString stringWithFormat:@"%@;%@/%@", pNumber,pMonth,pYear];
    [TRY_REQUEST_HANDLER addAuthResultToActiveInteractiveAuthMethod:result];
    [[BMSocketHandler sharedSocket] sendProbeMessageWithAuthMethod:[TRY_REQUEST_HANDLER activeInteractiveAuthMethod]
                                                        authResult:result
                                                            status:BMProbeStatusSuccess
                                                             error:nil];
    [self authMethodProbeDidComplete];
}

- (void)userDidCancelAuthentication {
    [self performSelector:@selector(cancelProbeButtonPressed:) withObject:nil];
}

#endif

@end
