//
//  BMLiveProcCaptureViewController.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 11/26/14.
//  Copyright (c) 2014 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

typedef NS_ENUM(NSInteger, DeviceCamera) {
    FrontCamera = 1,
    BackCamera = 0
};

@protocol BMLiveCaptureDelegate <NSObject>
- (BOOL) BMLiveCapturedPhoto:(UIImage*) image encodedToBase64:(NSString*) encodedImage;
@optional
- (void) BMLiveCapturedPhoto;
- (void) BMLiveCapturePhoto: (NSString*) pBioTitle capturedToBase64: (NSString*) pImageBase64;
- (void) BMLiveCapturedLastSampleWithResult: (UIImage*) pImage;
- (void) BMLiveCaptureFaceDetectedInFrame;
@end

@interface BMLiveProcCaptureViewController : UIViewController <AVCaptureVideoDataOutputSampleBufferDelegate>

@property (nonatomic, strong) IBOutlet UIImageView *detectionCenterImageview;
@property (nonatomic, strong) IBOutlet UIView *screenFlashView;

@property (nonatomic, weak) id <BMLiveCaptureDelegate> cameraDelegate;

@property (nonatomic, assign) NSString * const detectionCascadeFilename;
@property (nonatomic, retain) NSString * const qualityPreset;
@property (nonatomic, assign) CGRect frameRect;
@property (nonatomic, assign) NSInteger samplesRequested;
@property (nonatomic, assign) NSInteger camera;
@property (nonatomic, assign) BOOL torchOn;
@property (weak, nonatomic) IBOutlet UIImageView *userDistanceWarningImageView;

- (IBAction)toggleTorch:(id)sender;
- (IBAction)toggleCamera:(id)sender;

@end
