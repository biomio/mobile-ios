//
//  MenuCollectionViewCell.m
//
//  Created by Roman Slyepko on 31.08.15.
//  Copyright (c) 2015 Roman Slyepko. All rights reserved.
//

#import "MenuCollectionViewCell.h"

@implementation MenuCollectionViewCell
@synthesize menuItemImageView,menuItemTitleLabel;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

@end
