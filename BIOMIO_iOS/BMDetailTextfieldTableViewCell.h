//
//  BMDetailTextfieldTableViewCell.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 10/4/16.
//  Copyright © 2016 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BMDetailTextfieldTableViewCell;

@protocol DetailTextfieldCellDelegate <NSObject>
- (void) textfieldCell:(BMDetailTextfieldTableViewCell*) cell didFinishEditingTextfield:(UITextField*) textfield;
- (void) textfieldCell:(BMDetailTextfieldTableViewCell*) cell textfield: (UITextField*) textfield didChangeTextTo:(NSString*) newText;
@end

@interface BMDetailTextfieldTableViewCell : UITableViewCell <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *detailTextfield;

@property id <DetailTextfieldCellDelegate> delegate;

@end
