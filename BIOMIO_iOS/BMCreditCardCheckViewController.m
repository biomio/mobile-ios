//
//  BMCreditCardCheckViewController.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 1/27/16.
//  Copyright © 2016 BIOMIO. All rights reserved.
//

#import "BMCreditCardCheckViewController.h"

@interface BMCreditCardCheckViewController ()

@end

@implementation BMCreditCardCheckViewController  {
    NSArray* months;
    NSArray* years;
}

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // expiration date components
    months = @[@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12"];
    years = @[@"2000",@"2001",@"2002",@"2003",@"2004",@"2005",@"2006",@"2007",@"2008",@"2009",
              @"2010",@"2011",@"2012",@"2013",@"2014",@"2015",@"2016",@"2017",@"2018",@"2019",
              @"2020",@"2021",@"2022",@"2023",@"2024",@"2025",@"2026",@"2027",@"2028",@"2029",
              @"2030",@"2031",@"2032",@"2033",@"2034",@"2035",@"2036",@"2037",@"2038",@"2039"];
    // status bar background color
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, 20)];
    view.backgroundColor=[UIColor colorWithRed:0.396 green:0.416 blue:0.498 alpha:1.000];
    [self.view addSubview:view];
}

- (void)viewWillAppear:(BOOL)animated {
    self.cardNumberTextfield.text = self.cardNumber;
    [self.cardNumberTextfield becomeFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated {
    [self.datePickerView selectRow:[self indexOfMonthElementforInteger:self.expiryMonth] inComponent:0 animated:YES];
    [self.datePickerView selectRow:[self indexOfYearElementforInteger:self.expiryYear] inComponent:1 animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIPickerView delegate methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    // return the number of components required
    return 2;
}

// Return row count for each of the components
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return [months count];
    }
    else {
        return [years count];
    }
}

// Populate the rows of the Picker
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    // Component 0 should load the array1 values, Component 1 will have the array2 values
    if (component == 0) {
        return [months objectAtIndex:row];
    }
    else if (component == 1) {
        return [years objectAtIndex:row];
    }
    return nil;
}

#pragma mark - Date components

- (NSInteger) indexOfMonthElementforInteger:(NSInteger) pMonthInt {
    
    NSString* monthString = [NSString stringWithFormat:@"%zu", (long)pMonthInt];
    
    if ([monthString isEqualToString:@"0"]) {
        // today's date components
        NSDateComponents *components = [[NSCalendar currentCalendar] components: NSCalendarUnitMonth fromDate:[NSDate date]];
        NSInteger month = [components month];
        
        monthString = [NSString stringWithFormat:@"%ld", (long)month];
    }
    if ([monthString length] == 1)
        monthString = [NSString stringWithFormat:@"0%@", monthString];
    
    NSInteger elemIndex = [months indexOfObject:monthString];
    return elemIndex;
}

- (NSInteger) indexOfYearElementforInteger:(NSInteger) pYearInt {
    
    NSString* yearString = [NSString stringWithFormat:@"%zu", (long)pYearInt];
    
    if ([yearString isEqualToString:@"0"]) {
        // today's date components
        NSDateComponents *components = [[NSCalendar currentCalendar] components: NSCalendarUnitYear fromDate:[NSDate date]];
        NSInteger year = [components year];
        
        yearString = [NSString stringWithFormat:@"%ld", (long)year];
        
    }
    
    NSInteger elemIndex = [years indexOfObject:yearString];
    return elemIndex;
}

#pragma mark - Buttons

- (IBAction)doneButtonPressed:(id)sender {
    [self.cardNumberTextfield resignFirstResponder];
    if ([self.delegate respondsToSelector:@selector(userDidConfirmCardNumber:expiryMonth:expiryYear:)])
        [self.delegate userDidConfirmCardNumber:self.cardNumberTextfield.text
                                    expiryMonth:[months objectAtIndex:[self.datePickerView selectedRowInComponent:0]]
                                     expiryYear:[years objectAtIndex:[self.datePickerView selectedRowInComponent:1]]];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self.cardNumberTextfield resignFirstResponder];
    if ([self.delegate respondsToSelector:@selector(userDidCancelAuthentication)])
        [self.delegate userDidCancelAuthentication];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)backButtonPressed:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
