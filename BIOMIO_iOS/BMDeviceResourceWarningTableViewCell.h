//
//  BMDeviceResourceWarningTableViewCell.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 10/4/16.
//  Copyright © 2016 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>
// frameworks
#import "OAStackView.h"


@interface BMDeviceResourceWarningTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet OAStackView *stackView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

- (void) initWithAuthMethods:(NSArray*) methods;

@end
