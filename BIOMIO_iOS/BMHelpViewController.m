//
//  BMHelpViewController.m
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 10/27/15.
//  Copyright © 2015 BIOMIO. All rights reserved.
//

// view controllers
#import "BMHelpViewController.h"
#import "BMHelpLauncherViewController.h"
// libs/frameworks
#import <AVFoundation/AVFoundation.h>
#import "INTULocationManager.h"

@interface BMHelpViewController ()

@end

@implementation BMHelpViewController

#pragma mark - Life cycle

- (instancetype)init {
    if (!self.viewControllers)
        self.viewControllers = [self generateHelpTabs];
    self = [super initWithBackgroundImage:[UIImage imageNamed:@"help_background"] contents:[self generateHelpTabs]];
    if (self) {
        [self setHelpView];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Help tabs

- (void) setHelpView {
    self.navigationController.navigationBarHidden = YES;
    self.shouldMaskBackground = NO;
    self.shouldBlurBackground = NO;
    self.shouldFadeTransitions = YES;
    self.fadePageControlOnLastPage = YES;
    self.fadeSkipButtonOnLastPage = YES;
    self.bodyFontSize = 25;
    if ([[UIScreen mainScreen]bounds].size.height == 480)
        self.bodyFontSize = 18;
    // If you want to allow skipping the onboarding process, enable skipping and set a block to be executed
    // when the user hits the skip button.
    self.allowSkipping = YES;
    __weak typeof(self) weakSelf = self;
    self.skipHandler = ^{
        if (!weakSelf.firstLaunchType) {
            [weakSelf dismissBySkippingAll];
        }
        else {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"APP_LAUNCHED_BEFORE"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        }
    };
    UIButton* skipButton = [[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width-70, 30, 50, 30)];
    [skipButton setTitle:@"Skip" forState:UIControlStateNormal];
    [skipButton addTarget:self action:@selector(skipButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:skipButton];
    
}

- (NSArray*) generateHelpTabs {
    // WELCOME
    OnboardingContentViewController* welcomePage = [OnboardingContentViewController contentWithTitle:@"Welcome!"
                                                                                                body:nil
                                                                                               image:[UIImage imageNamed:@"help_logo"]
                                                                                          buttonText:nil
                                                                                              action:nil];
    welcomePage.topPadding = 250;
    welcomePage.underIconPadding = -10;
    
    // INTRO
    OnboardingContentViewController* introPage = [OnboardingContentViewController contentWithTitle:nil body:@"BIOMIO application makes it easy to authenticate yourself to various places - web sites, applications, secure communication and file transfers, or physical locations" image:[UIImage imageNamed:@"help_intro"] buttonText:nil action:nil];
    introPage.topPadding = 60;
    introPage.underIconPadding = -20;
    
    OnboardingContentViewController* authOptionsPage = [OnboardingContentViewController contentWithTitle:nil
                                                                                                    body:@"Simply pick from the available authentication options"
                                                                                                   image:[UIImage imageNamed:@"help_authoptions"]
                                                                                              buttonText:nil
                                                                                                  action:nil];
    authOptionsPage.topPadding = 130;
    authOptionsPage.underIconPadding = 30;
    
    // FOLLOW INSTRUCTION
    OnboardingContentViewController* followInstructionPage = [OnboardingContentViewController contentWithTitle:nil
                                                                                                          body:@"...and follow the instructions"
                                                                                                         image:[UIImage imageNamed:@"help_follow_instr"]
                                                                                                    buttonText:nil
                                                                                                        action:nil];
    followInstructionPage.topPadding = 100;
    followInstructionPage.underIconPadding = -10;
    
    // CAMERA
    AVAuthorizationStatus cameraAccessStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    __block NSString* cameraButtonTitle;
    switch (cameraAccessStatus) {
        case AVAuthorizationStatusAuthorized: {
            cameraButtonTitle = @"Great! Access granted!";
            break;
        }
        case AVAuthorizationStatusDenied: {
            cameraButtonTitle = @"Ooh! Access denied!";
            break;
        }
        case AVAuthorizationStatusRestricted: {
            cameraButtonTitle = @"Ooh! Access restricted!";
            break;
        }
        case AVAuthorizationStatusNotDetermined: {
            cameraButtonTitle = @"Allow camera access";
            break;
        }
    }
    OnboardingContentViewController* cameraPage = [OnboardingContentViewController contentWithTitle:nil
                                                                                               body:@"We need access to the camera to complete photo authentications"
                                                                                              image:[UIImage imageNamed:@"help_camera"]
                                                                                         buttonText:cameraButtonTitle
                                                                                             action:^{
                                                                                                 if (cameraAccessStatus == AVAuthorizationStatusNotDetermined) {
                                                                                                     [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                                                                                                         if(granted){
                                                                                                             cameraPage.buttonText = @"Great! Access granted";
                                                                                                         } else {
                                                                                                             cameraPage.buttonText = @"Ooh! Access denied";
                                                                                                         }
                                                                                                     }];
                                                                                                 }
                                                                                                 else if  (cameraAccessStatus != AVAuthorizationStatusAuthorized){
                                                                                                     NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                                                     [[UIApplication sharedApplication] openURL:url];
                                                                                                 }
                                                                                             }];
    cameraPage.topPadding = 130;
    cameraPage.underIconPadding = 30;
    
    // LOCATION
    CLAuthorizationStatus locationAccessStatus = [CLLocationManager authorizationStatus];
    NSString* locationButtonTitle;
    
    switch (locationAccessStatus) {
        case kCLAuthorizationStatusAuthorizedAlways:
        case kCLAuthorizationStatusAuthorizedWhenInUse: {
            locationButtonTitle = @"Great! Access granted!";
            break;
        }
        case kCLAuthorizationStatusDenied: {
            locationButtonTitle = @"Ooh! Access denied!";
            break;
        }
        case kCLAuthorizationStatusRestricted: {
            locationButtonTitle = @"Ooh! Access restricted!";
            break;
        }
        case kCLAuthorizationStatusNotDetermined: {
            locationButtonTitle = @"Enable location services";
            break;
        }
    }
    
    
    OnboardingContentViewController* locationPage = [OnboardingContentViewController contentWithTitle:nil
                                                                                                 body:@"We need access to device location services to complete location authentications"
                                                                                                image:[UIImage imageNamed:@"help_location"]
                                                                                           buttonText:locationButtonTitle
                                                                                               action:^{
                                                                                                   if (locationAccessStatus == kCLAuthorizationStatusNotDetermined) {
                                                                                                       [[INTULocationManager sharedInstance] requestLocationWithDesiredAccuracy:INTULocationAccuracyCity timeout:30 delayUntilAuthorized:YES block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                                                                                           
                                                                                                       }];
                                                                                                   }
                                                                                                   else if  (locationAccessStatus != kCLAuthorizationStatusAuthorizedAlways && locationAccessStatus != kCLAuthorizationStatusAuthorizedWhenInUse){
                                                                                                       NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                                                       [[UIApplication sharedApplication] openURL:url];
                                                                                                   }
                                                                                               }];
    locationPage.topPadding = 130;
    locationPage.underIconPadding = 20;
    locationPage.viewWillAppearBlock = ^{
        locationPage.buttonText = locationButtonTitle;
    };
    
    // TOUCH ID
    OnboardingContentViewController* touchIDPage = [OnboardingContentViewController contentWithTitle:nil body:@"In order to use fingerprint authentication the device must be password locked and have at least one finger enrolled in Touch ID settings" image:[UIImage imageNamed:@"help_touch"] buttonText:nil action:nil];
    touchIDPage.topPadding = 0;
    touchIDPage.underIconPadding = 0;
    
    // GET STARTED
    OnboardingContentViewController* getStartedPage = [OnboardingContentViewController contentWithTitle:nil body:@"BIOMIO's mission is \nto rid the world of passwords, keys, PINs and user id's\n\nWe're ecstatic to have you along for the ride!" image:[UIImage imageNamed:@"help_mission"] buttonText:@"Get started" action:^{
        if (!self.firstLaunchType)
            [self dismissBySkippingAll];
        else {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"APP_LAUNCHED_BEFORE"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
    getStartedPage.topPadding = 20;
    getStartedPage.underIconPadding = 0;
    if (!self.firstLaunchType)
        return @[introPage,authOptionsPage,followInstructionPage, cameraPage,locationPage,touchIDPage,getStartedPage];
    return @[welcomePage, introPage,authOptionsPage,followInstructionPage, cameraPage,locationPage,touchIDPage,getStartedPage];
}

#pragma mark - Buttons

- (void) skipButtonPressed {
    if (!self.firstLaunchType)
        [self dismissBySkippingAll];
    else {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"APP_LAUNCHED_BEFORE"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void) dismissBySkippingAll {    
    BMHelpLauncherViewController* helpLauncher = [[self.presentingViewController performSelector:@selector(selectedViewController) withObject:nil] performSelector:@selector(topViewController) withObject:nil];
    if (helpLauncher)
        helpLauncher.helpClose = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
