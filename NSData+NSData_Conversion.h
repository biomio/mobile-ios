//
//  NSData+NSData_Conversion.h
//  BIOMIO_iOS
//
//  Created by Roman Slyepko on 12/25/14.
//  Copyright (c) 2014 BIOMIO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (NSData_Conversion)

#pragma mark - String Conversion
- (NSString *)hexadecimalString;

@end
