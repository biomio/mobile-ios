//
//  UIImage+ColorAtPosition.h
//  BIOMIO_iOS
//
//  Created by Roman Andruseiko on 10/22/14.
//  Copyright (c) 2014 BIOMIO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ColorAtPosition)

- (NSInteger)colorValueAtPosition:(CGPoint)position;

@end
