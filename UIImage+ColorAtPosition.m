//
//  UIImage+ColorAtPosition.m
//  BIOMIO_iOS
//
//  Created by Roman Andruseiko on 10/22/14.
//  Copyright (c) 2014 BIOMIO. All rights reserved.
//

#import "UIImage+ColorAtPosition.h"

@implementation UIImage (ColorAtPosition)

- (NSInteger)colorValueAtPosition:(CGPoint)position {
	
	CGRect sourceRect = CGRectMake(position.x, position.y, 1.f, 1.f);
	CGImageRef imageRef = CGImageCreateWithImageInRect(self.CGImage, sourceRect);
	
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *buffer = (unsigned char*)malloc(4);
	CGBitmapInfo bitmapInfo = kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big;
	CGContextRef context = CGBitmapContextCreate(buffer, 1, 1, 8, 4, colorSpace, bitmapInfo);
	CGColorSpaceRelease(colorSpace);
	CGContextDrawImage(context, CGRectMake(0.f, 0.f, 1.f, 1.f), imageRef);
	CGImageRelease(imageRef);
	CGContextRelease(context);
	
	int r = buffer[0] / 255.f;
	int g = buffer[1] / 255.f;
	int b = buffer[2] / 255.f;
	int a = buffer[3] / 255.f;
	
	int value = ((r << 16) & 0xFF) + ((g << 8) & 0xFF) + (b & 0xFF) + ((a << 24) & 0xFF);
	
	free(buffer);
	
	return value;
}


@end
